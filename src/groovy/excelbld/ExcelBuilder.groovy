package excelbld

import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.util.CellUtil
import org.apache.poi.xssf.streaming.SXSSFWorkbook
import org.apache.poi.xssf.usermodel.XSSFColor
import org.apache.poi.xssf.usermodel.XSSFWorkbook

class ExcelBuilder {

    Workbook workbook
    CellStyle defaultStyle
    boolean useDefaultStyle = false
    Map<String, CellStyle> customStyles = [:]
    String defaultDateFormat = "dd-MM-yyyy HH:mm:ss"
    String defaultIntegerFormat = "#"
    String defaultDecimalFormat = "#,##0.00"
    boolean autosizeColumns = true

    private ExcelBuilder(Workbook workbook) {
        this.workbook = workbook
        this.defaultStyle = workbook.createCellStyle()
        defaultStyle {
            border style: "thin", color: "black"
            align vertical: "center", horizontal: "left"
        }
    }

    private static create(Workbook workbook, Closure builder) {
        def excelBuilder = new ExcelBuilder(workbook)
        builder.delegate = excelBuilder
        builder.resolveStrategy = Closure.DELEGATE_FIRST
        builder.call()
        return excelBuilder.workbook
    }

    static XSSFWorkbook createXlsx(Closure builder) { return create(new XSSFWorkbook(), builder) }

    static XSSFWorkbook createXlsxReader(byte[] content) {
        return createXlsxReader(new ByteArrayInputStream(content))
    }

    static XSSFWorkbook createXlsxReader(InputStream content) {
        new XSSFWorkbook(content)
    }

    static byte[] createXlsxContent(Closure builder) {
        XSSFWorkbook workbook = createXlsx(builder)
        def stream = new ByteArrayOutputStream()
        workbook.write(stream)
        stream.close()
        return stream.toByteArray()
    }

    //static SXSSFWorkbook createXlsx(Closure builder) { return create(new SXSSFWorkbook(), builder) }

    static byte[] createStreamingXlsx(Closure builder) {
        def sxssfWorkbook = new SXSSFWorkbook(100)
        sxssfWorkbook.setCompressTempFiles(true)
        SXSSFWorkbook workbook = create(sxssfWorkbook, builder)
        def stream = new ByteArrayOutputStream()
        workbook.write(stream)
        stream.close()
        workbook.dispose()
        return stream.toByteArray()
    }

    static void streamXlsx(OutputStream output, Closure builder) {
        def sxssfWorkbook = new SXSSFWorkbook(100)
        sxssfWorkbook.setCompressTempFiles(true)
        SXSSFWorkbook workbook = create(sxssfWorkbook, builder)
        workbook.write(output)
        workbook.dispose()
    }

    static HSSFWorkbook createXls(Closure builder) { return create(new HSSFWorkbook(), builder) }

    static byte[] createXlsContent(Closure builder) {
        HSSFWorkbook workbook = createXls(builder)
        def stream = new ByteArrayOutputStream()
        workbook.write(stream)
        stream.close()
        return stream.toByteArray()
    }

    CellStyle defaultStyle(Closure builder = null) {
        if(builder) {
            def styleBuilder = new StyleBuilder(this, defaultStyle)
            builder.delegate = styleBuilder
            builder.resolveStrategy = Closure.DELEGATE_FIRST
            builder.call()
            defaultStyle = styleBuilder.style
        }
        return defaultStyle
    }

    void defaultStyle(boolean useDefaultStyle) { this.useDefaultStyle = useDefaultStyle }

    void autoSizeColumns(boolean autoSizeColumns) { this.autosizeColumns = autoSizeColumns }

    CellStyle style(String name, Closure builder = null) {
        def style = null
        if(customStyles.containsKey(name)) style = customStyles[name]
        else {
            style = workbook.createCellStyle()
            if(useDefaultStyle) style.cloneStyleFrom(defaultStyle)
        }
        if(builder) {
            def styleBuilder = new StyleBuilder(this, style)
            builder.delegate = styleBuilder
            builder.resolveStrategy = Closure.DELEGATE_FIRST
            builder.call()
            customStyles[name] = style
        }
        return style
    }

    Sheet sheet(String name, Closure builder = null) {
        if(builder) {
            def sheetBuilder = new SheetBuilder(this, name)
            builder.delegate = sheetBuilder
            builder.resolveStrategy = Closure.DELEGATE_FIRST
            builder.call()
            if(autosizeColumns) sheetBuilder.sheet.first().each { cell ->
                sheetBuilder.sheet.autoSizeColumn(cell.columnIndex)
                if(sheetBuilder.sheet.getColumnWidth(cell.columnIndex) == 0) sheetBuilder.sheet.setColumnWidth(cell.columnIndex, 200)   //dovrebbe impedire il collasso involontario di alcune colonne
            }
            return sheetBuilder.sheet
        } else return workbook.getSheet(name)
    }

    Sheet sheet(Closure builder) { return sheet(null, builder) }

    private static class StyleBuilder {

        ExcelBuilder excelBuilder
        CellStyle style

        StyleBuilder(ExcelBuilder excelBuilder, CellStyle style = null) {
            this.excelBuilder = excelBuilder
            if(style) this.style = style
            else this.style = excelBuilder.workbook.createCellStyle()
        }

        void border(Map attrs) {
            def borderBuilder = new BorderBuilder(styleBuilder: this)
            borderBuilder.top(attrs)
            borderBuilder.bottom(attrs)
            borderBuilder.left(attrs)
            borderBuilder.right(attrs)
        }

        void border(Closure builder) {
            def borderBuilder = new BorderBuilder(styleBuilder: this)
            builder.delegate = borderBuilder
            builder.resolveStrategy = Closure.DELEGATE_FIRST
            builder.call()
        }

        void align(alignment) {
            align horizontal: alignment
        }

        void align(Map attrs) {
            def vertical = ExcelBuilderHelper.parseVerticalAlignment(attrs.vertical)
            def horizontal = ExcelBuilderHelper.parseHorizontalAlignment(attrs.horizontal)
            if(vertical) style.setVerticalAlignment(vertical)
            if(horizontal) style.setAlignment(horizontal)
        }

        void background(Map attrs) {
            def color = ExcelBuilderHelper.parseColor(attrs.color)
            def fillPattern = ExcelBuilderHelper.parseFillPattern(attrs.pattern)
            if(color) {
                style.setFillForegroundColor(color)
                style.setFillPattern(fillPattern ?: CellStyle.SOLID_FOREGROUND)
            }
        }

        Font font(Map attrs) {
            def font = excelBuilder.workbook.createFont()
            if(attrs.bold) font.setBold(Boolean.valueOf(attrs.bold))
            if(attrs.italic) font.setItalic(Boolean.valueOf(attrs.italic))
            if(attrs.underline) font.setUnderline(ExcelBuilderHelper.parseFontUnderline(attrs.underline))
            if(attrs.strikeout) font.setStrikeout(Boolean.valueOf(attrs.strikeout))
            if(attrs.color) font.setColor(ExcelBuilderHelper.parseColor(attrs.color))
            style.setFont(font)
            return font
        }

        void format(String format) {
            if(format) {
                def dataFormat = excelBuilder.workbook.createDataFormat().getFormat(format)
                style.setDataFormat(dataFormat)
            }
        }

    }

    private static class BorderBuilder {

        StyleBuilder styleBuilder

        void top(Map attrs) {
            def style = ExcelBuilderHelper.parseBorderStyle(attrs.style)
            def color = ExcelBuilderHelper.parseColor(attrs.color)
            if(style) styleBuilder.style.setBorderTop(style)
            if(color) styleBuilder.style.setTopBorderColor(color)
        }

        void bottom(Map attrs) {
            def style = ExcelBuilderHelper.parseBorderStyle(attrs.style)
            def color = ExcelBuilderHelper.parseColor(attrs.color)
            if(style) styleBuilder.style.setBorderBottom(style)
            if(color) styleBuilder.style.setBottomBorderColor(color)
        }

        void left(Map attrs) {
            def style = ExcelBuilderHelper.parseBorderStyle(attrs.style)
            def color = ExcelBuilderHelper.parseColor(attrs.color)
            if(style) styleBuilder.style.setBorderLeft(style)
            if(color) styleBuilder.style.setLeftBorderColor(color)
        }

        void right(Map attrs) {
            def style = ExcelBuilderHelper.parseBorderStyle(attrs.style)
            def color = ExcelBuilderHelper.parseColor(attrs.color)
            if(style) styleBuilder.style.setBorderRight(style)
            if(color) styleBuilder.style.setRightBorderColor(color)
        }
    }

    private static class SheetBuilder {

        ExcelBuilder excelBuilder
        Sheet sheet
        private int rowIndex = 0

        SheetBuilder(ExcelBuilder excelBuilder, String name) {
            this.excelBuilder = excelBuilder
            this.sheet = name ? excelBuilder.workbook.createSheet(name) : excelBuilder.workbook.createSheet()
        }

        Row row(Map attrs, Closure builder) {
            def rowBuilder = new RowBuilder(this, rowIndex, attrs)
            builder.delegate = rowBuilder
            builder.resolveStrategy = Closure.DELEGATE_FIRST
            builder.call()
            rowIndex += 1
            return rowBuilder.row
        }

        Row row(Closure builder) { return row(null, builder) }

        Row row(List values) { return row { values.each { value -> cell(value) } } }

        void lock(rows) {
            if(rows instanceof Number) sheet.createFreezePane(0, rows)
            else if(rows instanceof Range) sheet.createFreezePane(rows.from, rows.to)
        }

        void filter(filter) {
            if(filter instanceof CellRangeAddress) sheet.setAutoFilter(filter)
            else if(filter instanceof Number) sheet.setAutoFilter(new CellRangeAddress(0, 1, filter, filter))
            //else if(filter instanceof Boolean && filter) sheet.setAutoFilter(new CellRangeAddress(0, 1, 0, sheet.first()?.lastCellNum))
            else if(filter instanceof Range) sheet.setAutoFilter(new CellRangeAddress(0, 1, filter.from, filter.to))
            else if(filter instanceof String) sheet.setAutoFilter(CellRangeAddress.valueOf(filter))
        }

        void merge(...regions) {
            if(regions.size() == 1) {
                if(regions.first() instanceof CellRangeAddress) sheet.addMergedRegion(regions.first())
                else if(regions.first() instanceof String) sheet.addMergedRegion(CellRangeAddress.valueOf(regions.first()))
            } else if(regions.size() == 2 && regions.any { el -> el instanceof Range }) {
                Range rows = regions.first(), columns = regions.last()
                sheet.addMergedRegion(new CellRangeAddress(rows.from, rows.to, columns.from, columns.to))
            } else if(regions.size() == 4 && regions.any { el -> el instanceof Number }) sheet.addMergedRegion(new CellRangeAddress(regions[0], regions[1], regions[2], regions[3]))
            else regions.each { region -> merge(region) }
        }

    }

    private static class RowBuilder {

        SheetBuilder sheetBuilder
        Row row
        private Map rowAttrs
        private int cellIndex = 0

        RowBuilder(SheetBuilder sheetBuilder, int rowIndex, Map rowAttrs = [:]) {
            this.sheetBuilder = sheetBuilder
            this.row = sheetBuilder.sheet.createRow(rowIndex)
            this.rowAttrs = rowAttrs
            if(rowAttrs?.height) this.row.setHeightInPoints(Float.valueOf(rowAttrs.height))
        }

        Cell cell(value) {
            def cell = row.createCell(cellIndex)
            setCellValue(cell, value)
            setCellStyle(cell)
            cellIndex += 1
            return cell
        }

        Cell cell(Map attrs) {
            def cell = row.createCell(cellIndex)
            setCellValue(cell, attrs.value)
            setCellStyle(cell, attrs.style)
            if(attrs.format) setCellFormat(cell, attrs.format)
            if(attrs.width) sheetBuilder.sheet.setColumnWidth(cellIndex, Integer.valueOf(attrs.width))
            cellIndex += 1
            return cell
        }

        private void setCellValue(Cell cell, value) {
            if(value != null && cell != null) {
                if(value instanceof Number) {
                    double doubleValue = ((Number)value).doubleValue()
                    cell.setCellValue(doubleValue)
                    if(value instanceof Integer || value instanceof BigInteger) setCellFormat(cell, sheetBuilder.excelBuilder.defaultIntegerFormat)
                    else setCellFormat(cell, sheetBuilder.excelBuilder.defaultDecimalFormat)
                } else if(value instanceof Date) {
                    cell.setCellValue((Date)value)
                    setCellFormat(cell, sheetBuilder.excelBuilder.defaultDateFormat)
                } else if(value instanceof Calendar) {
                    cell.setCellValue((Calendar)value)
                } else if(isFormulaDefinition(value)) cell.setCellFormula(getFormula(value))
                else cell.setCellValue(value.toString())
            }
        }

        private boolean isFormulaDefinition(value) { return value instanceof String && value.length() > 2 && value.startsWith("=") }

        private String getFormula(value) { return value[1..-1] }

        private void setCellStyle(Cell cell, String name = null) {
            def cellStyle = getStyle(name)
            if(cellStyle) cell.setCellStyle(cellStyle)
        }

        private CellStyle getStyle(String name = null) {
            if(name && sheetBuilder.excelBuilder.customStyles.containsKey(name)) return sheetBuilder.excelBuilder.customStyles[name]
            if(rowAttrs?.style && sheetBuilder.excelBuilder.customStyles.containsKey(rowAttrs.style)) return sheetBuilder.excelBuilder.customStyles[rowAttrs.style]
            if(sheetBuilder.excelBuilder.useDefaultStyle) return sheetBuilder.excelBuilder.defaultStyle
            return null
        }

        private void setCellFormat(Cell cell, String format) {
            if(format) {
                def dataFormat = sheetBuilder.excelBuilder.workbook.createDataFormat()
                CellUtil.setCellStyleProperty(cell, sheetBuilder.excelBuilder.workbook, CellUtil.DATA_FORMAT, dataFormat.getFormat(format))
            }
        }

    }

    private static class ExcelBuilderHelper {
        static def parseHorizontalAlignment(alignment) {
            switch(alignment) {
                case short:
                case HorizontalAlignment: return alignment
                case { it instanceof String && it.toUpperCase().startsWith("ALIGN")}: return CellStyle."${alignment.toUpperCase()}"
                case String: return CellStyle."ALIGN_${alignment.toUpperCase()}"
                default: return null
            }
        }
        static def parseVerticalAlignment(alignment) {
            switch(alignment) {
                case short:
                case VerticalAlignment: return alignment
                case { it instanceof String && it.toUpperCase().startsWith("VERTICAL")}: return CellStyle."${alignment.toUpperCase()}"
                case String: return CellStyle."VERTICAL_${alignment.toUpperCase()}"
                default: return null
            }
        }

        static def parseBorderStyle(borderStyle) {
            switch(borderStyle) {
                case short:
                case BorderStyle: return borderStyle
                case { it instanceof String && it.startsWith("BORDER")}: return CellStyle."${borderStyle.toUpperCase()}"
                case String: return CellStyle."BORDER_${borderStyle.toUpperCase()}"
                default: return null
            }
        }

        static def parseColor(color) {
            switch(color) {
                case short: return color
                case IndexedColors: return color.index
                case { it instanceof String && it.startsWith("#")}: return new XSSFColor(hex2rgb(color))
                case String: return IndexedColors.valueOf(color.toUpperCase()).index
                case byte[]: return new XSSFColor(color)
                default: return null
            }
        }
        private static byte[] hex2rgb(String hex) {
            def r = Integer.valueOf(hex[1..<3], 16)
            def g = Integer.valueOf(hex[3..<5], 16)
            def b = Integer.valueOf(hex[5..<7], 16)
            return [r, g, b] as byte[]
        }

        static def parseFontUnderline(underline) {
            switch(underline) {
                case { it instanceof String && it.toUpperCase().startsWith("U_") }: return Font."${underline.toUpperCase()}"
                case String: return Font."U_${underline.toUpperCase()}"
                case byte: return underline
                default: return Font.U_NONE
            }
        }

        static def parseFillPattern(fillPattern) {
            switch(fillPattern) {
                case short: return fillPattern
                case String: return CellStyle."${fillPattern.toUpperCase()}"
                default: return null
            }
        }
    }
}
