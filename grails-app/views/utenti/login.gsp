<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Esmobility - Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
    <link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <r:external file="/less/layout.less"/>
    <r:external file="/css/jquery-ui/overcast/jquery-ui.css"/>
    <r:external file="/css/bootstrap/css/bootstrap.css"/>
    <r:external file="/css/animate.css"/>
    <r:external file="/less/main.less"/>
    <r:external file="/less/login.less"/>
    <r:require module="jquery-ui"/>
    <r:layoutResources/>
    <r:script>
        $(document).ready(function() {
            //alert(window.location.pathname);
            $('#wrapper').addClass('animated fadeInDown');
        } );
    </r:script>
    </head>
    <body>
        <div id="wrapper">
            <div id="main" name="main">
                    <div class="col-md-12 col-md-push-5" style="margin-top: 5px; margin-bottom: 15px;">
                        <r:img dir="images/mach1" file="es_mobility.png" id="logo-esmobility" height="20%" width="20%"/>
                    </div>
                    <div class="col-md-6 col-md-push-3">
                        <flash:error/>
                    </div>
                    <form method="post" autocomplete="off">

                        <div class="div1 col-md-10 col-md-push-2" style="font-size: 18px;">
                            <h3 class="div4 div5">Benvenuto</h3>
                            <div class="col-md-10" >
                                <f:field type="text" name="username" autofocus="true"/>
                            </div>
                            <div class=" col-md-10">
                                <f:field type="password" name="password"/>
                            </div>
                            <div class="col-md-10 col-md-push-6">
                                <input type="submit" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right loginBtn" id="login" name="login" value="Login"/>
                                %{--<f:submit value="Login" id="login"/>--}%
                            </div>
                        </div>
                    </form>
            </div>
            <div id="footer" class="col-lg-12">
                <div style="margin-left: 120px;"><r:img dir="images/mach1" file="logo_mach1.png" id="logo-mach1"/></div>
                <div style="margin-left: 120px;">
                    Direzione e Sede Legale Via Vittor Pisani, 13/B | 20124 Milano - TEL. 02 00638 057  FAX 02 62087266 -<br>
                    CCIAA Milano - REA MI 1908726 - C.F.,  P. IVA e Reg. Imprese Milano 06680830962 - Capitale sociale 100.000 EURO - Registro Unico Intermediari  A000317603
                </div>

            </div>
        </div>

        <r:layoutResources/>
    </body>
</html>