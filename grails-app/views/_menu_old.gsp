<%@ page import="utenti.*" %>
<g:set var="utente" value="${session.utente}"/>

    <div class="col-md-12" style="margin-top: 20px; margin-left: 10px;">
        <div class="col-md-6">
            <a id="ricerca" name="ricerca" href="<g:createLink controller="sinistri" action="ricerca"/>" class=""><r:img dir="images/mach1" file="es_mobility_sm.png" id="logo-esmobility"/></a>
        </div>
        <div class="col-md-6">
            <ul class="menu">
                <li></li>
                <li id="logout"><g:link controller="utenti" action="logout">Logout</g:link></li>
            </ul>
        </div>
    </div>


<r:script>
    $(".menu").buttonset();
    $(".ui-button", $("#logout").prev()).addClass("ui-corner-right");
    $(".ui-button", "#logout").addClass("ui-corner-left");
</r:script>