<%@ page import="utenti.*" %>
<g:set var="utente" value="${session.utente}"/>
    <style>
    .botoncito {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 19px !important;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
    </style>
    <div class="col-md-12 col-md-pull-0" style="margin-top: 20px; margin-left: 10px;">
        <div class="row">
                <ul class="menu">
                    <li></li>
                   %{-- <li id="logout"><g:link controller="utenti" action="logout">Logout</g:link></li>--}%
                    <li id="logout" style="color: #C2B49C;"> <a href="${createLink(controller: "utenti", action: "logout")}" style="font-size: 25px; color: #C2B49C; border: 0px solid #fff; background: #fff "><p style="font-size: 25px"> <i class="fa fa-sign-out"></i>Logout</p> </a></li>
                </ul>

        </div>

        %{--<div class="row">
            <div id="wrapper">
                <nav class="navbar-default navbar-static-side" role="navigation">
                    <ul class="nav metismenu" id="side-menu" style="margin-top: 60px; margin-left: 15px;">
                        <li class="active">
                            <a href="${createLink(controller: "sinistri", action: "ricerca")}"><i class="fa fa-car"></i> <span class="nav-label">Gestione sinistri</span></a>
                        </li>
                        <li >
                            <a href="${createLink(controller: "sinistri", action: "estrazioni")}"><i class="fa fa-file-excel-o"></i> <span class="nav-label">Estrazione contabile</span></a>
                        </li>
                        <li >
                            <a href="${createLink(controller: "sinistri", action: "estrazioni")}"><i class="fa fa-table"></i> <span class="nav-label">Estrazione sinistri</span></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>--}%

    </div>


<r:script>
    $(".menu").buttonset();
    $(".ui-button", $("#logout").prev()).addClass("ui-corner-right");
    $(".ui-button", "#logout").addClass("ui-corner-left");
</r:script>