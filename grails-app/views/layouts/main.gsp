<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>ES mobility</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
	<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
	<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">

	<r:external file="/less/layout.less"/>
	<r:external file="/less/main.less"/>
	<r:external file="/less/sinistri.less"/>
	<r:external file="/css/font-awesome.css"/>
	<r:external file="/css/bootstrap/css/bootstrap.css"/>
	<r:external file="/css/toastr/toastr.min.css"/>
	<r:external file="/css/font-awesome.css"/>
	<r:external file="/css/animate.css"/>
	<r:external file="/css/style.css"/>
	<r:external file="/css/ui.jqgrid.css"/>
	<r:require module="jquery-ui"/>
	<g:layoutHead/>
	<r:layoutResources />
	<r:script>
		$("[title]").tooltip();
		$("body").mousemove(function(event) { $("#spinner").css({top: event.pageY, left: event.pageX + 16}); });
		$(document).ajaxStart(function() { $("#spinner").show(); }).ajaxStop(function() { $("#spinner").hide(); });
		$(document).ready(function() {
			//alert(window.location.pathname);
			$('[data-toggle=offcanvas]').click(function() {
				$('.row-offcanvas').toggleClass('active');
			});
		} );
	</r:script>
	<style>
	body,html,.row-offcanvas {
		height:100%;
	}

	body {
		padding-top: 50px;
	}

	#sidebar {
		width: inherit;
		min-width: 220px;
		max-width: 220px;
		//background-color:#f5f5f5;
		float: left;
		height:100%;
		position:relative;
		overflow-y:auto;
		overflow-x:hidden;
	}
	#main {
		height:100%;
		overflow:auto;
	}

	/*
     * off Canvas sidebar
     * --------------------------------------------------
     */
	@media screen and (max-width: 768px) {
		.row-offcanvas {
			position: relative;
			-webkit-transition: all 0.25s ease-out;
			-moz-transition: all 0.25s ease-out;
			transition: all 0.25s ease-out;
			width:calc(100% + 220px);
		}

		.row-offcanvas-left
		{
			left: -220px;
		}

		.row-offcanvas-left.active {
			left: 0;
		}

		.sidebar-offcanvas {
			position: absolute;
			top: 0;
		}
	}
	.nav {
		background-color: #C2B49C;
		/*padding: 0.4em 0.65em;*/
		-moz-box-shadow: 0 0 3px 1px #aaaaaa;
		-webkit-box-shadow: 0 0 3px 1px #aaaaaa;
		box-shadow: 0 0 3px 1px #aaaaaa;
		zoom: 1;
		overflow: auto;
		height: 520px;
		border-bottom-right-radius: 6px;
		border-bottom-left-radius: 6px;
		border-top-left-radius: 6px;
		border-top-right-radius: 6px;
	}
	.nav > li.active {
		border-radius: 0.3em;
		background: #666666;
		/*width:280px;
		/*height:27px;*/

	}
	li a {
		display: block;
		color: #fff;
		padding: 8px 16px;
		text-decoration: none;
	}

	li a.active {
		background-color: #000;
		color: #fff;
	}

	li a:hover:not(.active) {
		background-color: #666666;
		color: #fff;
		overflow: visible;
	}
	</style>
</head>
<body>
<div class="navbar">
	<div id="header" class="navbar navbar-default ">
		<div class="navbar-header">
			<a id="ricerca" name="ricerca" href="<g:createLink controller="sinistri" action="menu_es"/>" class=""><r:img dir="images/mach1" file="es_mobility_sm.png" id="logo-esmobility"/></a>
		</div>
		<nav class="collapse navbar-collapse">
			<ui:renderTemplate template="/menu"/>
		</nav>
	</div>
</div><!--/.navbar -->

<div class="row-offcanvas row-offcanvas-left">
	<div id="sidebar" class="sidebar-offcanvas">
		<div class="col-md-12">
			<ul class="nav list-group">
				<li <g:if test="${actionName=="ricerca"}">class="active"</g:if>>
					<a style="font-size: 25px;" href="${createLink(controller: "sinistri", action: "ricerca")}" > <i class="fa fa-area-chart"></i> <span class="">Gestione sinistri</span></a>
				</li>
				<li <g:if test="${actionName=="estrazioni"  || actionName=="estrazioniContabili" || actionName=="estrazioniContabiliTable" }">class="active"</g:if> >
					<a style="font-size: 25px;" href="${createLink(controller: "sinistri", action: "estrazioni")}"><i class="fa fa-cogs"></i> <span class="nav-label">Estrazione contabile</span></a>
				</li>
				<li <g:if test="${actionName=="estrazionisx"  || actionName=="estrazioniSinistri" || actionName=="estrazioniSinistriTable"}">class="active"</g:if> >
					<a style="font-size: 25px;" href="${createLink(controller: "sinistri", action: "estrazionisx")}"><i class="fa fa-wrench"></i> <span class="nav-label">Estrazione sinistri</span></a>
				</li>
			</ul>
		</div>
	</div>
	<div id="main">
		<r:layoutResources />

		<div class="col-md-12">
		<g:layoutBody/>

		</div>
		<div class="col-md-12" style="margin-top: 280px;">
			<div class="col-md-3">&nbsp&nbsp&nbsp&nbsp&nbsp</div>
			<div class="col-md-7 col-md-push-1 pull-left"><r:img dir="images/mach1" file="logo_mach1.png" id="logo-mach1"/></div>
			<div class="col-md-12 pull-right" style="word-wrap:break-word; text-align: center;" >
				Direzione e Sede Legale Via Vittor Pisani, 13/B | 20124 Milano  - TEL. 02 00638 057  FAX 02 62087266 -
				CCIAA Milano - REA MI 1908726 -  C.F.,  P. IVA e Reg. Imprese Milano 06680830962 - Capitale sociale 100.000 EURO - Registro Unico Intermediari  A000317603
			</div>
		</div>
	</div>

</div><!--/row-offcanvas -->

</body>
</html>
