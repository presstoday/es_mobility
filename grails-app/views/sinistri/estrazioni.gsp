<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title></title>
        <r:external file="/less/sinistri.less"/>
        <r:external file="/css/jquery.dataTables.css"/>
        <r:external file="/css/buttons.dataTables.min.css"/>
        <r:external file="/css/bootstrap/css/bootstrap.css"/>
        <r:external file="/css/ladda.min.css"/>
        <r:external file="/css/ladda-themeless.min.css"/>
        <r:script>
            Ladda.bind( '.ladda-button' );

         $(document).ready(function() {
             $('.datepicker').datepicker(
                     {
                         autoclose: true,
                         multidate:true,
                         multidateSeparator:true,
                         daysOfWeekDisabled: [0,6],
                         changeMonth: true,
                         changeYear: true,
                         minDate: new Date(2017, 0, 01),
                         dateFormat: 'dd-mm-yy',
                         onSelect: function(dateText) {
                             $("#dataaperturada").val($("#datada").val());
                             $("#dataaperturaal").val($("#dataal").val());
                         }

                     }
             );
                //$("#example").hide();
                 $("#dataaperturada").val($("#datada").val());
                 $("#dataaperturaal").val($("#dataal").val());
                  var compagnia=$("input[type='checkbox']").val();
                    var allVals = [];
                 $('#company :checked').each(function() {
                   allVals.push($(this).val());
                 });
                 $("#compagnia").val(allVals);

                /*  ****/
             $("#fixTable").tableHeadFixer();

         } );
            $('[data-toggle=offcanvas]').click(function() {
                $('.row-offcanvas').toggleClass('active');
                $('.collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');

            });
         $("#datada").change(function() {
             var dat1 = this.value;
             $("#dataaperturada").val( dat1);
             //params.data1=dat1;
         });
         $("#dataal").change(function() {
             var dat2 = this.value;
             $("#dataaperturaal").val( dat2);
             //params.data2=dat2;

         });
       // $("input[type='checkbox']").change(function() {
        $( function() {
            $( "#form-table" ).on( "change", ":checkbox", function () {
                var compagnia=$("input[type='checkbox']").val();
                var allVals = [];
                     $('#company :checked').each(function() {
                       allVals.push($(this).val());
                     });
                     $("#compagnia").val(allVals);
                if(this.checked) {
                }
            });
        });
        $('#mostra').click(function(){
            $('.circo').show();
        });

        </r:script>
        <style>
        #parent {
            height: 300px;
            font-family: verdana, arial, helvetica, sans-serif;
            font-size: 10.5px;
            border: 1px dotted #aaa;
            background-color: #E7E1D8 !important;
            margin-top:60px ;
            border: 1px dotted #aaa;
        }
        th{
            background-color: #C2B49C !important;
            background-image: none !important;
            color: #000;
            font-size: 15px;
            /*text-align: center;*/
        }
        td{
            text-align: right;
            font-size: 12px;
        }
        .table > thead > tr > th {
            vertical-align: top;
        }
        th,td{
            padding-right: 10px;
            padding-bottom: 6px;
            /*text-align: right;*/
            border: 1px double #aaa;
        }
        legend{
            border-bottom: 0px solid #e5e5e5;
        }
        .bottoni{
            padding-left: 0.3em !important;
            padding-right: 0.5em !important;
            padding-top: 0.7em !important;
            padding-bottom: 0.53em !important;
        }
        .comp{
            color: darkgray;
        }

    </style>
    </head>
    <body>
    <div class="row">
            <div class="col-lg-12 pull-right">
                <div class="col-lg-12 col-sm-6 col-lg-pull-1" style="margin-bottom: 20px; font-size: 16px;">
                    <flash:output/>
                </div>
                <div class="col-lg-12" style="border-top: 2px solid #C2B49C; border-bottom: 2px solid #C2B49C;" >
                    <div class="col-lg-8">
                        <legend><br><h1 style="float: left; margin-bottom: 40px;"> ESTRAZIONE CONTABILE</h1></legend>
                    </div>
                    <div class="col-lg-4 col-lg-pull-2" style="margin-top: 25px;">
                        <div class="col-lg-7"><a id="contTot" name="contTot" href="<g:createLink action="downloadContabileTot"/>" class="totContLink"><h4 style="color: #000; font-weight: bold"><i class="fa fa-file-excel-o fa-2x" style="color: #C2B49C;"></i><br><br>sinistri chiusi senza data pagamento integrato</h4></a></div>
                    </div>
                </div>
                <div class=" form-group col-lg-12 col-sm-6" style="margin-top: 30px;">
                    <form id="form-table" name="form-table" method="post" action="${createLink(controller: "sinistri", action: 'estrazioniContabiliTable')}"  style="font-size: 20px;">
                        <g:set var="today" value="${new Date()}"/>
                        <div class="form-group col-lg-12">
                            <div class="col-md-6">
                                <label style="margin-top: 5px; color: darkgray" class="col-md-1 control-label">Data:</label>

                            </div>
                            <div class="col-md-6 col-md-pull-4">
                                <div class="input-group date col-md-4" data-provide="datepicker">
                                    <span class="input-group-addon">Da:</span>
                                    <input class="datepicker" data-date-format="dd-MM-yyyy" value="${datada? datada : today.format()}" name="datada" id="datada" data-multidateSeparator="true">
                                    <span class="input-group-addon">A:</span>
                                    <input class="datepicker" data-date-format="dd-MM-yyyy"  name="dataal" id="dataal" value="${dataal? dataal : today.format()}" data-date-autoclose="true">
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-12">
                            <div class="col-md-2 col-sm-2">
                                <label style="margin-top: 5px; color: darkgray" class="col-sm-1 control-label">Compagnia:</label>
                            </div>
                            <div id="company" name="company" class="col-md-10 col-sm-4 ">
                                <label class="comp"> <input type="checkbox" name="compagnia1"  value="AXA LLD" <g:if test="${company}"><g:if test="${company.toString().contains("AXA LLD")}"> checked </g:if></g:if><g:else>checked</g:else>  class="compagnia-check"> AXA LLD &nbsp&nbsp&nbsp&nbsp </label>
                                <label class="comp"> <input type="checkbox" name="compagnia1"  value="HELVETIA RCI DEMO" <g:if test="${company}"><g:if test="${company.toString().contains("HELVETIA RCI DEMO")}"> checked </g:if></g:if><g:else>checked</g:else>  class="compagnia-check"> HELVETIA RCI DEMO </label><br>
                                <label class="comp"><input type="checkbox"  name="compagnia1" value="HELVETIA VIP RCI NOLEGGIO" <g:if test="${company}"><g:if test="${company.toString().contains("HELVETIA VIP RCI NOLEGGIO")}"> checked </g:if></g:if><g:else>checked</g:else> class="compagnia-check"> HELVETIA VIP RCI NOLEGGIO</label><br>
                                <label class="comp"><input type="checkbox" name="compagnia1"  value="HELVETIA RCI NOLEGGIO" <g:if test="${company}"><g:if test="${company.toString().contains("HELVETIA RCI NOLEGGIO")}"> checked </g:if></g:if><g:else>checked</g:else> class="compagnia-check"> HELVETIA RCI NOLEGGIO </label>
                            </div>
                        </div>

                </div>
                <div class="form-group col-lg-12 col-sm-6">
                        <div class="col-md-6">
                            <label style="margin-top: 5px; color: darkgray; font-size: 20px;" class="control-label">&nbsp&nbsp&nbsp&nbsp&nbspMostra dati:</label>
                        </div>
                        <div class="col-md-6 col-md-pull-4">
                            <button type="submit" style=" font-size:20px; color: white; float: left; " data-toggle="offcanvas" class=" bottoni ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right btn-default" name="mostra" id="mostra" >Filtra per data e compagnia <i class="fa fa-cog fa-spin fa-1x fa-fw circo" style="display: none;"></i></button>
                    </form>
                    <form id="form-filtri" method="post" action="${createLink(controller: "sinistri", action: 'estrazioniContabili')}"  style="font-size: 20px;">
                        <input type="hidden" id="compagnia" name="compagnia" value="" >
                        <input type="hidden" id="dataaperturada" name="dataaperturada"  value="">
                        <input type="hidden" id="dataaperturaal" name="dataaperturaal"  value="">
                            <button type="submit" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right btn-default bottoni" style="float: none; color: white; font-size: 19px;"  id="excel" name="excel" value="EXCEL"><i class="fa fa-file-excel-o"></i> Excel</button>

                        %{--<input type="submit" class="ui-button ui-widget ui-state-mostra ui-button-text-only ui-corner-left ui-corner-right mostra" id="mostra" name="mostra" value="MOSTRA"/>--}%
                        </div>

                    </form>
                </div>
            </div>
            <g:if test="${datiEstrazione}" >
                <div id="parent" class="col-lg-12 pull-left col-md-6 col-sm-6" >
                    <table id="fixTable" class="table">
                        <thead>
                        <tr>
                            <th>NO. SINISTRO</th>
                            <th>NO. FATTURA</th>
                            <th>INTESTATARIO FATTURA</th>
                            <th>INTESTATARIO POLIZZA</th>
                            <th>INTESTATARIO CONTO</th>
                            <th>DATA PAGAMENTO</th>
                            <th>IMPORTO FATTURA</th>
                            <th>IMPORTO LIQUIDATO</th>
                            <th>DIFFERENZA DA<br>INTEGRARE</th>
                            <th>DATA PAGAMENTO<br>INTEGRATO</th>
                            <th>&nbspFATTURA ALLEGATA &nbsp&nbsp</th>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each var="sinistro" in="${datiEstrazione}">
                            <tr>
                                <td>${sinistro.noSinistro}</td>
                                <td>${sinistro.noFattura}</td>
                                <td>${sinistro.intestatarioFattura}</td>
                                <td>${sinistro.intestatarioPolizza}</td>
                                <td>${sinistro.intestatarioConto}</td>
                                <td>${sinistro.dataPagamento?.format("dd-MM-yyyy")}</td>
                                <td>${sinistro.importoFattura?.format()}</td>
                                <td>${sinistro.importoLiquidato?.format()}</td>
                                <td>${sinistro.differenzaLiquidare?.format()}</td>
                                <td>${sinistro.dataPagamentoIntegrato?.format("dd-MM-yyyy")}</td>
                                <td style="color: #0a0a0a !important;">
                                    <g:if test="${sinistro.allegati}">
                                        <g:each var="allegato" in="${sinistro.allegati}">
                                            <g:if env="development">
                                                <a href="http://localhost:8080/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}" style="color: #0a0a0a;"><p style="color: #0a0a0a!important;">
                                                    <g:if test="${allegato.fileName.contains(".pdf")}"><i class="fa fa-file-pdf-o"></i></g:if>
                                                    <g:elseif test="${allegato.fileName.contains(".txt")}"><i class="fa fa-file-text"></i></g:elseif>
                                                    <g:elseif test="${allegato.fileName.contains(".jpeg")|| allegato.fileName.contains(".jpg") || allegato.fileName.contains(".png")}"><i class="fa fa-file-image-o"></i></g:elseif>
                                                    <g:else><i class="fa fa-paperclip"></i></g:else>
                                                    ${allegato.fileName}</p></a>
                                            </g:if>
                                            <g:elseif env="test">
                                                <a href="http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}" style="color: #0a0a0a;"><p style="color: #0a0a0a!important;">
                                                    <g:if test="${allegato.fileName.contains(".pdf")}"><i class="fa fa-file-pdf-o"></i></g:if>
                                                    <g:elseif test="${allegato.fileName.contains(".txt")}"><i class="fa fa-file-text"></i></g:elseif>
                                                    <g:elseif test="${allegato.fileName.contains(".jpeg")|| allegato.fileName.contains(".jpg") || allegato.fileName.contains(".png")}"><i class="fa fa-file-image-o"></i></g:elseif>
                                                    <g:else><i class="fa fa-paperclip"></i></g:else>
                                                    ${allegato.fileName}</p></a>
                                            </g:elseif>
                                            <g:else>
                                                <a href="http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}" style="color: #0a0a0a;">><p style="color: #0a0a0a!important;">
                                                    <g:if test="${allegato.fileName.contains(".pdf")}"><i class="fa fa-file-pdf-o"></i></g:if>
                                                    <g:elseif test="${allegato.fileName.contains(".txt")}"><i class="fa fa-file-text"></i></g:elseif>
                                                    <g:elseif test="${allegato.fileName.contains(".jpeg")|| allegato.fileName.contains(".jpg") || allegato.fileName.contains(".png")}"><i class="fa fa-file-image-o"></i></g:elseif>
                                                    <g:else><i class="fa fa-paperclip"></i></g:else>
                                                    ${allegato.fileName}</p></a>
                                            </g:else>
                                        </g:each>
                                    </g:if>
                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>
            %{-- <div class="col-lg-12 col-lg-pull-1 col-sm-6" style="margin-top: 60px;">
                 <div>
                     <table id="example"  class="table table-bordered tabella" width="auto" cellpadding="10">
                         <thead >
                         <tr >
                             <th align="center" valign="middle" >NO. SINISTRO</th>
                             <th align="center" valign="middle">NO. FATTURA</th>
                             <th align="center" valign="middle">INTESTATARIO FATTURA</th>
                             <th align="center" valign="middle">INTESTATARIO POLIZZA</th>
                             <th align="center" valign="middle">INTESTATARIO CONTO</th>
                             <th align="center" valign="middle">DATA PAGAMENTO</th>
                             <th align="center" valign="middle">IMPORTO FATTURA</th>
                             <th align="center" valign="middle">IMPORTO LIQUIDATO</th>
                             <th align="center" valign="middle">DIFFERENZA DA<br>INTEGRARE</th>
                             <th align="center" valign="middle">DATA PAGAMENTO<br>INTEGRATO</th>
                             <th align="center" valign="middle">&nbspFATTURA ALLEGATA &nbsp&nbsp</th>
                         </tr>
                         </thead>
                         <tbody>
                         <g:each var="sinistro" in="${datiEstrazione}">
                             <tr class="">
                                 <td>${sinistro.noSinistro}</td>
                                 <td>${sinistro.noFattura}</td>
                                 <td>${sinistro.intestatarioFattura}</td>
                                 <td>${sinistro.intestatarioPolizza}</td>
                                 <td>${sinistro.intestatarioConto}</td>
                                 <td>${sinistro.dataPagamento?.format("dd-MM-yyyy")}</td>
                                 <td>${sinistro.importoFattura?.format()}</td>
                                 <td>${sinistro.importoLiquidato?.format()}</td>
                                 <td>${sinistro.differenzaLiquidare?.format()}</td>
                                 <td>${sinistro.dataPagamentoIntegrato?.format("dd-MM-yyyy")}</td>
                                 <td style="color: #0a0a0a !important;">
                                     <g:if test="${sinistro.allegati}">
                                         <g:each var="allegato" in="${sinistro.allegati}">
                                             <g:if env="development">
                                                 <a href="http://localhost:8080/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}" style="color: #0a0a0a;"><p style="color: #0a0a0a!important;">
                                                     <g:if test="${allegato.fileName.contains(".pdf")}"><i class="fa fa-file-pdf-o"></i></g:if>
                                                     <g:elseif test="${allegato.fileName.contains(".txt")}"><i class="fa fa-file-text"></i></g:elseif>
                                                     <g:elseif test="${allegato.fileName.contains(".jpeg")|| allegato.fileName.contains(".jpg") || allegato.fileName.contains(".png")}"><i class="fa fa-file-image-o"></i></g:elseif>
                                                     <g:else><i class="fa fa-paperclip"></i></g:else>
                                                     ${allegato.fileName}</p></a>
                                             </g:if>
                                             <g:elseif env="test">
                                                 <a href="http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}" style="color: #0a0a0a;">${allegato.fileName}</a>
                                             </g:elseif>
                                             <g:else>
                                                 <a href="http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}" style="color: #0a0a0a;">${allegato.fileName}</a>
                                             </g:else>
                                         </g:each>
                                     </g:if>
                                 </td>
                             </tr>
                         </g:each>
                         </tbody>
                         <tfoot>
                         </tfoot>
                     </table>
                 </div>
                 --}%%{--<table id="jqGrid"></table>
                 <div id="jqGridPager"></div>--}%%{--
             </div>--}%
            </g:if>

    </div>



    </body>
</html>