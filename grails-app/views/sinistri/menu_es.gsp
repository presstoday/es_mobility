
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <r:external file="/less/sinistri.less"/>
    <r:external file="/css/jquery.dataTables.css"/>
    <r:external file="/css/buttons.dataTables.min.css"/>
    <r:external file="/css/font-awesome.css"/>
    <r:external file="/css/bootstrap/css/bootstrap.css"/>
    <r:external file="/css/animate.css"/>
    <r:external file="/css/ladda.min.css"/>
    <r:external file="/css/ladda-themeless.min.css"/>
    <r:external file="/css/sweetalert.css"/>
</head>

<body>
<div class="col-lg-10 col-lg-push-1" >
    <div class="row">

        <div class="col-lg-12 col-lg-pull-1">
            <h1 style="color: #0a0a0a"><b>PORTALE CONSULTAZIONE SINISTRI</b></h1>
        </div>
        <br><br><br><br>
        <div class="col-lg-12 col-lg-push-4">
            <r:img dir="images/mach1" file="es_mobility_sm.png" id="logo-esmobility"/>
        </div>
    </div>

</div>
</body>
</html>