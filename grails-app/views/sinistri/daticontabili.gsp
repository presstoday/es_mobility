
<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <r:external file="/less/sinistri.less"/>
    <r:external file="/css/jquery.dataTables.css"/>
    <r:external file="/css/buttons.dataTables.min.css"/>
    <r:external file="/css/font-awesome.css"/>
    <r:external file="/css/bootstrap/css/bootstrap.css"/>
    <r:external file="/css/animate.css"/>
    <r:external file="/css/ladda.min.css"/>
    <r:external file="/css/ladda-themeless.min.css"/>
    <r:external file="/css/sweetalert.css"/>
    <r:script>
        $(document).ready(function() {
            $('.datepicker').datepicker(
                    {
                        autoclose: true,
                        multidate:true,
                        multidateSeparator:true,
                        daysOfWeekDisabled: [0,6],
                        changeMonth: true,
                        changeYear: true,
                        //minDate: new Date(2017, 0, 01),
                        dateFormat: 'dd-mm-yy'

                    }
            );
        } );


    </r:script>
</head>

<body>
        <div class="col-lg-12 pull-right" >
            <g:if test="${flash.error}"><div class="labelErrore">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
            <div class="row">&nbsp;</div>
            <g:set var="today" value="${new Date()}"/>
                <g:form id="daticontabili" class="form-horizontal" method="post" action="aggiornaDaticontabili"  enctype="multipart/form-data" >
                    <div class="tab-pane fade in active col-lg-11 dettaglioDatiC">
                        <legend class="legend"><span class="fa fa-file-text-o"></span> DATI CONTABILI</legend>
                        <input type="hidden" name="dettasx" id="dettasx" value="${sinistro}"/>
                        <input type="hidden" name="parametro" id="parametro" value="${parametro}"/>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Tipo polizza</b></label>
                           %{-- <input type="text" class="field field-content ui-widget-content ui-corner-all"  value="${daticontabili?.tipoPolizza}" name="tipoPolizza" id="tipoPolizza"/>--}%
                            <select class="field field-content ui-widget-content ui-corner-all" name="tipoPolizza" id="tipoPolizza" style="background-color: white;">
                                <option value="AXA LLD" <g:if test="${daticontabili?.tipoPolizza == "AXA LLD"}">selected</g:if>>AXA LLD</option>
                                <option value="HELVETIA RCI DEMO" <g:if test="${daticontabili?.tipoPolizza == "HELVETIA RCI DEMO"}">selected</g:if>>HELVETIA RCI DEMO</option>
                                <option value="HELVETIA VIP RCI NOLEGGIO" <g:if test="${daticontabili?.tipoPolizza == "HELVETIA VIP RCI NOLEGGIO"}">selected</g:if> >HELVETIA VIP RCI NOLEGGIO</option>
                                <option value="HELVETIA RCI NOLEGGIO" <g:if test="${daticontabili?.tipoPolizza == "HELVETIA RCI NOLEGGIO"}">selected</g:if> >HELVETIA RCI NOLEGGIO</option>
                            </select>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>No. sinistro</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" name="noSx" id="noSx" value="${nosx}"/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Intestatario polizza</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" name="intestatarioPolizza" id="intestatarioPolizza" value="${daticontabili?.intestatarioPolizza}"/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Intestatario fattura</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" name="intestatarioFattura" id="intestatarioFattura" value="${daticontabili?.intestatarioFattura}"/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Importo fattura</b></label>
                            <input type="number" step=".01" lang="it" class="field field-content ui-widget-content ui-corner-all" name="importoFattura" id="importoFattura" value="${daticontabili?.importoFattura}"/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Importo liquidato</b></label>
                            <input type="number" step=".01" class="field field-content ui-widget-content ui-corner-all" name="importoLiquidato" id="importoLiquidato" value="${daticontabili?.importoLiquidato}"/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Diff. da integrare</b></label>
                            <input type="number" step=".01" class="field field-content ui-widget-content ui-corner-all" name="differenzaLiquidare" id="differenzaLiquidare" value="${daticontabili?.differenzaLiquidare}"/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Intestatario conto</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" name="intestatarioConto" id="intestatarioConto"  value="${daticontabili?.intestatarioConto}"/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>IBAN</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" name="iban" id="iban" value="${daticontabili?.iban}"/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" style="margin-top: 4px;">
                            %{--<f:field type="date" name="dataPagamento" value="${daticontabili?.dataPagamento?.format("dd-MM-yyyy")}"/>--}%
                            <label class="control-label"><b>Data pagamento</b></label>
                            <input class="datepicker field field-content ui-widget-content ui-corner-all" data-date-format="dd-MM-yyyy"  name="dataPagamento" id="dataPagamento" value="${daticontabili?.dataPagamento? daticontabili.dataPagamento.format() : ''}" data-date-autoclose="true">

                        </div>
                        <div class="col-lg-4" style="margin-top: 4px;">
                            %{--<f:field type="date" name="dataPagamentoIntegrato" value="${daticontabili?.dataPagamentoIntegrato?.format("dd-MM-yyyy")}"/>--}%
                            <label class="control-label"><b>Data pagamento integrato</b></label>
                            <input class="datepicker field field-content ui-widget-content ui-corner-all" data-date-format="dd-MM-yyyy"  name="dataPagamentoIntegrato" id="dataPagamentoIntegrato" value="${daticontabili?.dataPagamentoIntegrato? daticontabili.dataPagamentoIntegrato.format() : ''}" data-date-autoclose="true">

                        </div>
                        <div class="col-lg-4" style="margin-top: 4px;">
                            <label class="control-label"><b>Fattura</b></label>
                            <input id="input-fattura" type="file"  class="file  ui-widget-content ui-corner-all" multiple data-preview-file-type="text" name="fileFattura" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="row">&nbsp;</div>
                        <div class="row">&nbsp;</div>

                        <legend class="legend"><span class="fa fa-paperclip"></span> ALLEGATI</legend>
                        <div class="row">&nbsp;</div>
                        <g:if test="${allegati}">
                            <div class="col-lg-8">
                                <g:each var="allegato" in="${allegati}">
                                    <a href="<g:createLink action='downloadAllegato' params='[id: "${allegato?.id}"]'/>"  id="downAttach" name="downAttach" class="settLink"><span style="color: #333">
                                        <g:if test="${allegato?.fileName.toString().contains(".xls")}">
                                            <i class="fa fa-file-excel-o fa-2x"></i>
                                        </g:if><g:elseif test="${allegato?.fileName.toString().contains(".doc") || allegato?.fileName.toString().contains(".txt") }">
                                            <i class="fa fa-file-text-o fa-2x"></i>
                                        </g:elseif><g:elseif test="${allegato?.fileName.toString().contains(".jpeg") || allegato?.fileName.toString().contains(".jpg") || allegato?.fileName.toString().contains(".png") }">
                                            <i class="fa fa-file-picture-o fa-2x"></i>
                                        </g:elseif><g:elseif test="${allegato?.fileName.toString().contains(".pdf")}">
                                            <i class="fa fa-file-pdf-o fa-2x"></i>
                                        </g:elseif>
                                        <g:else>
                                            <i class="fa fa-file-o fa-2x"></i>
                                        </g:else>

                                        ${allegato?.fileName}  </span></a>
                                    <br>
                                    <br>
                                </g:each>
                            </div>
                        </g:if>

                        <div class="row">&nbsp;</div>
                        <div class="row">&nbsp;</div>
                        <div class="row">&nbsp;</div>
                        <div class="row">&nbsp;</div>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>
                    <div class="col-lg-11">
                       %{-- <button type="button" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right close-button bottone-chiude"><span class="">Annulla</span></button>--}%
                        %{--<a href="#" onclick="javascript:window.history.back();"  class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right close-button btn-danger bottone-annulla"><span class="ui-button-text">Annulla</span></a>--}%
                        <g:actionSubmit value='Salva dati' class='ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right btn-success bottone-salva' action='aggiornaDaticontabili'  id="salvaC" name="salvaC" />
                        %{--<button type="submit" id="bottonSalva" value="Salva dati" class="ui-widget ui-state-default ui-corner-left ui-corner-right bottone-commento">Salva dati</button>--}%

                </g:form>
                        %{--<a href="${createLink(controller: 'sinistri', action: 'dettaglio', params: [sinistro:"${sinistro}", parametro:parametro] )}"  class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right close-button btn-danger bottone-annulla"><span class="ui-button-text">Annulla</span></a>--}%
                        <a href="#" onclick="document.getElementById('${parametro.toString().replace('/', '_')}').submit()"  class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right close-button btn-danger bottone-annulla"><span class="ui-button-text">Annulla</span></a>
                        <form  method="post" id="${parametro.toString().replace('/', '_')}" action="${createLink(controller: "sinistri", action: 'dettaglio')}">
                            <input type="hidden" name="sinistro" value="${sinistro}">
                            <input type="hidden" name="parametro" value="${parametro}">
                        </form>
                    </div>

        </div>
</body>
</html>