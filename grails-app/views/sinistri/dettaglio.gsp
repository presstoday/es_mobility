<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<style>
.ui-tooltip {
    /* If max-width does not work, try using width instead */
    max-width: 650px !important;

}
</style>
<html>
    <head>
        <title></title>

        <r:external file="/less/sinistri.less"/>
        <r:external file="/css/jquery.dataTables.css"/>
        <r:external file="/css/buttons.dataTables.min.css"/>
        <r:external file="/css/font-awesome.css"/>
        <r:external file="/css/bootstrap/css/bootstrap.css"/>
        <r:external file="/css/animate.css"/>
        <r:external file="/css/ladda.min.css"/>
        <r:external file="/css/ladda-themeless.min.css"/>
        <r:external file="/css/sweetalert.css"/>
        <r:script>
            $("[title]").tooltip();
            Ladda.bind( '.ladda-button' );
            document.body.style.cursor='wait';
            window.onload=function(){
                 var rispo=$("#rispostaval").val();
                 if(rispo !=null && rispo!='' ){
                    if (rispo.indexOf("errore") >= 0){
                        swal({
                            title: rispo,
                            type: "error",
                            timer: "1800",
                            showConfirmButton: false
                        });
                     }else{
                        swal({
                            title: rispo,
                            type: "success",
                            timer: "1800",
                            showConfirmButton: false
                        });
                     }

                 }
                document.body.style.cursor='default';
            }

            $("body").mousemove(function(event) { $("#spinner").css({top: event.pageY, left: event.pageX + 16}); });
            $(document).ajaxStart(function() { $("#spinner").show(); }).ajaxStop(function() { $("#spinner").hide(); });
            $('#exampleModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('Invio email ')
                modal.find('.modal-body input').val(recipient)
            })
            $('#commentModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('Inserisce commento')
                modal.find('.modal-body input').val(recipient)
            })
            $('#inviaMailB').click(function(e) {
                e.preventDefault();
                invioEmail();
            });
            function invioEmail(){
                var noSinistro=$("#noSinistro").val();
                var messageM=$("#messageM").val();
                var isMail=$("#isMail").val();
                $.post("${createLink(controller: "sinistri", action: "inviaMail")}",{ noSinistro:noSinistro,messageM:messageM,isMail:isMail}, function(response) {
                    var risposta=response.risposta;
                    if(risposta==false){
                        var errore=response.errore;
                        swal({
                            title: "email  non inviato!",
                            type: "error",
                            timer: "1800",
                            showConfirmButton: false
                        });
                    }else{
                    swal({
                    title: "messaggio inviato!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                });
                     $('#exampleModal').modal('hide');
                     window.location.reload();
                    }
                }, "json");
            }
            $('#inviaMailC').click(function(e) {
                e.preventDefault();
                invioEmailC();
            });

            function invioEmailC(){
                var noSinistro=$("#noSinistro").val();
                var messageM=$("#messagetextC").val();
                var isMail=$("#isMailC").val();
                $.post("${createLink(controller: "sinistri", action: "inviaMail")}",{ noSinistro:noSinistro,messageM:messageM,isMail:isMail}, function(response) {
                    var risposta=response.risposta;
                    if(risposta==false){
                        var errore=response.errore;
                        swal({
                            title: "commento non inserito!",
                            type: "error",
                            timer: "1800",
                            showConfirmButton: false
                        });
                    }else{
                    swal({
                    title: "commento inserito!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                    });
                     $('#commentModal').modal('hide');
                     window.location.reload();
                    }
                }, "json");
            }
            $('.torna').click(function(){
               document.body.style.cursor='wait';
               return true;
           });

      function modalPlafond(sinistro,parametro){
          alert("entro qui!!");
          var noSx=$("#noSinistro").val();
          var dettagli=$("#sini").val();
        $.post("${createLink(controller: "sinistri", action: 'getDaticontabili')}",{ parametro: parametro, sinistro:sinistro}, function(response) {
            if(response.risposta==true ){
               /* $( ".alert" ).remove();
                $("#noSx").val(noSx);
                $("#tipoPolizza").val(response.tipoPolizza);
                $("#intestatarioPolizza").val(response.intestatarioPolizza);
                $("#intestatarioFattura").val(response.intestatarioFattura);
                $("#noFattura").val(response.noFattura);
                $("#intestatarioConto").val(response.intestatarioConto);
                $("#iban").val(response.iban);
                $("#dataPagamento").val(response.dataPagamento);
                $("#importoFattura").val(response.importoFattura);
                $("#importoLiquidato").val(response.importoLiquidato);
                $("#differenzaLiquidare").val(response.differenzaLiquidare);
                $("#dettasx").val(response.dettagliosx);
                $("#contabileModal").modal({
                    keyboard: true
                });
                if(response.allegati!=''&& response.allegati!=null){
                    $( ".allegati" ).after( "<div> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append("Allegati").append("<br>").append(response.allegati).append("<br>");
                }*/
            }else{
                /* $("#dettasx").val(response.dettagliosx);
                 $("#noSx").val(noSx);
                 $("#contabileModal").modal({
                        keyboard: true
                    });*/

            }
        }, "json");
    }
        </r:script>

        <style>

        </style>
    </head>

    <body>
        <g:if test="${flash.message}"><div class="labelSuccess">${flash.message.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.error}"><div class="labelErrore">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <div class="row col-lg-12 pull-right">
        <g:if test="${risposta}">
            <input type="hidden" id="rispostaval" name="rispostaval" value="${risposta}"/>
           %{-- <div class="col-lg-6">
                <g:if test="${risposta.toString().contains("errore")}">
                    <span class="label label-danger text-capitalize">${risposta}</span>
                </g:if><g:else>
               <h2><span class="label label-danger text-capitalize" style="padding-top: 10px; padding-bottom: 10px; padding-left: 25px; padding-right: 20px;">${risposta}</span></h2>
            </g:else>
            </div>--}%
        </g:if>
            <div class="col-lg-12" id="sinistro">
                <div class="row">
                    <div class="col-lg-4">
                        <div><a href="${createLink(controller: "sinistri", action: "ricerca", params: [parametro:"${parametro}"])}"  class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right btn-default"><span class="ui-button-text">Torna all'elenco</span></a></div>
                    </div>
                    <div class="col-lg-3 ">
                        %{-- <a href="${createLink(controller: "sinistri", action: "daticontabili", params:[sinistro: sinistroA, parametro: parametro])}" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right btn-success"><span class="ui-button-text">Sezione contabile</span></a>--}%
                        <a href="#" onclick="document.getElementById('${parametro.toString().replace('/', '_')}').submit()" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right btn-success">
                        <form  method="post" id="${parametro.toString().replace('/', '_')}" action="${createLink(controller: "sinistri", action: 'daticontabili')}">
                            <input type="hidden" name="sinistro" value="${sinistroA}">
                            <input type="hidden" name="parametro" value="${parametro}">
                        </form>
                        <span class="ui-button-text">Sezione contabile</span>
                        </a>
                    </div>
                    <div class="col-lg-3 col-lg-push-2">
                        <div>
                            <g:if test="${session.utente instanceof utenti.Admin}">
                                <button type="button" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right btn-primary" data-toggle="modal" data-target="#commentModal" data-id="${sinistri?.noSinistro?.toString().replace("\\[","").replace("]","")}"><span class="ui-button-text">Commenta pratica</span></button>
                            </g:if><g:else>
                            <button type="button" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right btn-primary" data-toggle="modal" data-target="#exampleModal" data-id="${sinistri?.noSinistro?.toString().replace("\\[","").replace("]","")}"><span class="ui-button-text">Sollecita pratica</span></button>
                        </g:else>
                        </div>
                    </div>
            </div><div class="row">&nbsp</div>
            <g:each var="sinistro" in="${sinistri}">
                <div class="tab-pane fade in active col-lg-12 dettaglio">
                    <legend class="legend"><span class="fa fa-user"></span> DATI ASSICURATO</legend>
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="control-label"><b>Nome</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.nome}"/>
                        </div>
                        <div class="col-lg-1">
                            <label class="control-label"><b>Targa</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.targa}"/>
                        </div>
                        <div class="col-lg-3">
                            <label class="control-label"><b>C.F./P.IVA</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.codFiscale}"/>
                        </div>
                        <div class="col-lg-2">
                            <label class="control-label"><b>Polizza</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.noPolizza}"/>
                        </div>
                    </div>
                    <legend class="legend"><span class="fa fa-car"></span> DATI SINISTRO</legend>
                    <div class="row">
                        <div class="col-lg-2" >
                            <label class="control-label"><b>Numero</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.noSinistro}"/>
                        </div>
                        <div class="col-lg-2" >
                            <label class="control-label"><b>Data</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${Date.parse("dd/MM/yyyy",sinistro?.dataSinistro).format("dd/MM/yy")}"/>

                        </div>
                        <div class="col-lg-3" >
                            <label class="control-label "><b>Evento</b></label><i class="fa fa-info-circle fa-2x" id="info" style="cursor: pointer; font-size: 1.38em;" title=" <p style='font-size:0.8em;'> <b>CID ATTIVO MONOFIRMA:</b> IN GESTIONE DA PARTE DELLA COMPAGNIA;  RICHIESTA DI RISARCIMENTO DANNI PERVENUTA SOLO DAL NOSTRO ASSICURATO; LA LIQUIDAZIONE AVVERRA’ ENTRO I 60GG  DALLA DOCUMENTAZIONE COMPLETA</p>
    <p style='font-size:0.8em;'> <b>CID ATTIVA DOPPIAFIRMA:</b>IN GESTIONE DA PARTE DELLA COMPAGNIA; LA RICHIESTA DI RISARCIMENTO CON FIRMA DI ENTRAMBE LE CONTROPARTI E’ COMPLETA, LA LIQUIDAZIONE AVVERRA’ ENTRO I 30GG DALLA DOCUMENTAZIONE COMPLETA</p>
    <p style='font-size:0.8em;'> <b>CID DEBITORE:</b>  SINISTRO NON GESTIBILE DALLA COMPAGNIA. RICHIESTA DI RISARCIMENTO DANNI SOTTOSCRITTA DA ENTRAMBE LA PARTI. RESPONSABILITA’ TOTALE DEL NOSTRO ASSICURATO; LIQUIDABILE SOLO CON APERTURA GARANZIA KASKO CON FRANCHIGIA A CARICO DEL CLIENTE</p>
    <p style='font-size:0.8em;'> <b>RC ATTIVO:</b> SINISTRO NON GESTIBILE DALLA COMPAGNIA; LA LIQUIDAZIONE POTRA’ AVVENIRE SOLO CON RICHIESTA DANNI ALLA COMPAGNIA DI CONTROPARTE (CON L’EVENTUALE PATROCINIO DEL NOSTRO LEGALE). LA GARANZIA KASKO POTRA’ ESSERE ATTIVATA CON FRANCHIGIA A CARICO DEL CLIENTE</p>
    <p style='font-size:0.8em;'> <b>RC PASSIVO:</b> SINISTRO NON GESTIBILE DALLA COMPAGNIA.  RESPONSABILITA’ TOTALE DEL NOSTRO ASSICURATO; LIQUIDABILE SOLO CON APERTURA GARANZIA KASKO CON FRANCHIGIA A CARICO DEL CLIENTE</p>
    <p style='font-size:0.8em;'>NUOVO ITER:<b>CONCORSUALE:</b> </p>
    <p style='font-size:0.8em;'>SINISTRO IN GESTIONE ALLA COMPAGNIA; RICHIESTA DI RISARCIMENTO DANNI DA CUI SI EVINCE LA RESPONSABILITA’ DI ENTRAMBE LE PARTI (E’ POSSIBILE ANCHE CON PERCENTUALI DIVERSE).  GARANZIA KASKO ATTIVABILE CON PAGAMENTO FRANCHIGIA A CARICO DEL CLIENTE SOLO LA COMPLETA DEFINIZIONE DEL SINISTRO DA PARTE DELLA COMPAGNIA</p>
    "></i>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.evento}"/></div>
                        <div class="col-lg-2" >
                            <label class="control-label"><b>Denuncia</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${Date.parse("dd/MM/yyyy",sinistro?.dataRicevimentoDenuncia).format("dd/MM/yy")}"/>
                        </div>
                        <div class="col-lg-2" >
                            <label class="control-label"><b>Stato</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.stato}"/>
                        </div>
                    </div><div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-3" >
                            <label class="control-label"><b>Iter</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.iter}"/>
                        </div>
                        <div class="col-lg-2" >
                            <label class="control-label"><b>Data Iter</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${Date.parse("dd/MM/yyyy",sinistro?.dataIter).format("dd/MM/yy")}"/>
                        </div>
                    </div><div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Riparatore</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.riparatore}"/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                            </div>
                            <div class="col-lg-4" >
                                <label class="control-label"><b>Descrizione</b></label>
                                <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.descrizione}"/>
                                %{--<textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.descrizione}</textarea>--}%
                            </div>
                            <div class="col-lg-4" >
                                <label class="control-label"><b>Descrizione 2</b></label>
                                <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.descrizione2}"/>
                                %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.descrizione2}</textarea>--}%
                            </div>
                        </div>

                        <legend class="legend"><span class="fa fa-money"></span> DATI LIQUIDAZIONE</legend>
                        <div class="row">
                            <div class="col-lg-2">
                                    <label class="control-label"><b>Preventivo</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.preventivo}"/>
                            </div>
                            <div class="col-lg-1">
                                    <label class="control-label"><b>Data</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.dataPreventivo?Date.parse("dd/MM/yyyy",sinistro?.dataPreventivo).format("dd/MM/yy"):""}"/>
                            </div><div class="col-lg-1">&nbsp;</div>
                            <div class="col-lg-2">
                                    <label class="control-label"><b>Concordato</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.concordato}"/>
                            </div>
                            <div class="col-lg-1">
                                    <label class="control-label"><b>Data</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.dataConcordato?Date.parse("dd/MM/yyyy",sinistro?.dataConcordato).format("dd/MM/yy"):""}"/>
                            </div><div class="col-lg-1">&nbsp;</div>
                            <div class="col-lg-2">
                                    <label class="control-label"><b>Pagato</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.pagato}"/>
                            </div>
                            <div class="col-lg-1">
                                    <label class="control-label"><b>Data</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.dataPagamento?Date.parse("dd/MM/yyyy",sinistro?.dataPagamento).format("dd/MM/yy"):""}"/>
                            </div>
                        </div><div class="row">&nbsp;</div>
                        <div class="row">
                            <div class="col-lg-2" >
                                <label class="control-label"><b>Scoperto</b></label>
                                <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.scoperto}"/>
                            </div>
                            <div class="col-lg-2" >
                                <label class="control-label"><b>Franchigia</b></label>
                                <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.franchigia}"/>
                            </div>
                            <div class="col-lg-2" >
                                <label class="control-label"><b>Degrado</b></label>
                                <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.degrado}"/>
                            </div>
                        </div>
                        %{--<legend class="legend"><span class="fa fa-book"></span> DOCUMENTAZIONE MANCANTE</legend>
                        <div class="row">
                            <div class="col-lg-1" >
                                <label class="control-label"><b>Completa</b></label>
                                <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.documentazionecompleta}"/>
                            </div>
                            <div class="col-lg-3">&nbsp;</div>
                                <div class="col-lg-4" >
                                    <label class="control-label"><b>Documentazione 1</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu1}"/>
                                </div>
                                <div class="col-lg-4" >
                                    <label class="control-label"><b>Documentazione 2</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu2}"/>
                                </div>
                        </div><div class="row">&nbsp;</div>
                        <div class="row">
                                <div class="col-lg-4" >
                                    <label class="control-label"><b>Documentazione 3</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu3}"/>
                                </div>
                                <div class="col-lg-4" >
                                    <label class="control-label"><b>Documentazione 4</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu4}"/>
                                </div>
                                <div class="col-lg-4" >
                                    <label class="control-label"><b>Documentazione 5</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu5}"/>
                                </div>
                        </div><div class="row">&nbsp;</div>
                        <div class="row">
                                <div class="col-lg-4" >
                                    <label class="control-label"><b>Documentazione 6</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu6}"/>
                                </div>
                                <div class="col-lg-4" >
                                    <label class="control-label"><b>Documentazione 7</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu7}"/>
                                </div>
                                <div class="col-lg-4" >
                                    <label class="control-label"><b>Documentazione 8</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu8}"/>
                                </div>
                        </div><div class="row">&nbsp;</div>
                        <div class="row">
                                <div class="col-lg-4" >
                                    <label class="control-label"><b>Documentazione 9</b></label>
                                    <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu9}"/>
                                </div>
                        </div>--}%
                        <legend class="legend"><span class="fa fa-book"></span> NOTE</legend>
                        <div class="row">
                            <div class="col-lg-12" >
                                <textarea name="note" style="resize: both;" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.note}</textarea>
                            </div>
                        </div>
                        <legend class="legend"><span class="fa fa-paperclip"></span> ALLEGATI</legend>
                        <div class="row">
                            <div class="col-md-12">
                                <g:if test="${sinistro?.filenames}">
                                    <g:each var="allegato" in="${sinistro?.filenames}">
                                        <div class="row">
                                            <div class="col-lg-2 text-right">${allegato.data?.toString()}</div>
                                            <div class="col-lg-10">
                                                <g:link controller="sinistri" action="downloadFilesx" params="[sre: sinistro?.noSinistro, nomeFile: allegato.nome, ricerca: params.ricerca ?: '']"><i class="fa ${allegato.icon?.toString()}"> </i>
                                                    </g:link>
                                                %{--<a href="http://www.nais.biz/rinnovi-vw/v2/api.php/ftp?f=${allegato.url.replaceAll(" ", "+").replaceAll("\\s","+")}" target="_blank" class="">--}%
                                                    %{--<i class="fa ${allegato.icon?.toString()}"> </i>--}%
                                                %{--</a>--}%

                                                ${allegato.nome?.toString()}
                                            </div>
                                        </div><div class="row">&nbsp;</div>
                                    %{--<i class="fa fa-file-archive-o" style="cursor: pointer; font-size: 1.0em;" onclick="return downloadFile(${sinistro.noSinistro});"></i>
                                    <g:if test="${allegato.toString().replace("[","").replace("]","").contains("doc")}" ><f:field type="output" value="${allegato.toString().replace("[","").replace("]","")}"/><i class="fa fa-file-text" style="cursor: pointer; font-size: 1.0em;" onclick="return viewSx(${sinistro.noSinistro});"></i></g:if>
                                    <g:if test="${allegato.toString().replace("[","").replace("]","").contains("DOC")}" ><f:field type="output" value="${allegato.toString().replace("[","").replace("]","")}"/><i class="fa fa-file-text" style="cursor: pointer; font-size: 1.0em;" onclick="return viewSx(${sinistro.noSinistro});"></i></g:if>--}%
                                    </g:each>
                                </g:if>
                                <g:else><h4 style="margin-right: 480px;"><i>Non ci sono allegati</i></h4></g:else>

                            </div>
                        </div>
                        <g:if test="${commenti}">
                            <legend class="legend"><span class="fa fa-commenting-o"></span> SOLLECITI</legend>
                            <div class="row">
                                <div class="chat-discussion">
                                    <g:each var="commento" in="${commenti}">
                                        <g:if test="${commento.utente.username.toString().replaceAll("\\[","").replaceAll("]","") !="admin"}">
                                            <div class="chat-message left">
                                                <i class="message-avatar" aria-hidden="true">  <r:img dir="images/mach1" file="es_small.png" id="logo-esmobility" height="80%" width="100%"/></i>
                                                <div class="message">
                                                    <div class="message-author">${commento.utente.username.toString().replaceAll("\\[","").replaceAll("]","").capitalize()} &nbsp&nbsp&nbsp ${commento.dataInserimento.format("E, dd MMM yyyy HH:mm:ss")}</div>
                                                    <span class="message-content">${commento.messaggio}</span>
                                                </div>
                                            </div>
                                        </g:if>
                                        <g:else>
                                            <div class="chat-message right">
                                                <i class="message-avatar" aria-hidden="true">  <r:img dir="images/mach1" file="logo_mach1_small.png" id="logo-esmobility" height="80%" width="100%"/></i>
                                                <div class="message">
                                                    <div class="message-author"> ${commento.dataInserimento.format("E, dd MMM yyyy HH:mm:ss")}&nbsp&nbsp&nbsp${commento.utente.username.toString().replaceAll("\\[","").replaceAll("]","").capitalize()}</div>
                                                    <span class="message-date">  </span>
                                                    <span class="message-content">${commento.messaggio}</span>
                                                </div>
                                            </div>
                                        </g:else>
                                    </g:each>
                                </div>
                            </div>
                        </g:if>
                    <div class="row"></div>
                    </div>

                </g:each>
            </div>

    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">New message</h4>
                </div>
                <g:form class="form-horizontal" method="post" params='[isMail:true, noSinistro_:"${sinistri.noSinistro}"]'>
                    <input type="hidden" name="noSinistro" id="noSinistro" value="${sinistri.noSinistro?.toString().replace("[","").replace("]","")}"/>
                    <input type="hidden" name="isMail" id="isMail" value="true"/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="dest">Destinatario:</label>
                            <label style="font-weight: normal">lld@mach-1.it</label>
                            <div>
                                <label class="dest">OGGETTO:</label>
                                <label style="font-weight: normal"> SOLLECITO SINISTRO ${sinistri?.noSinistro.toString().replace("[","").replace("]","")}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="messageM" class="dest">Messaggio:</label>
                            <textarea class="form-control" id="messageM" name="messageM"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right close-button bottone-chiude" data-dismiss="modal"><span class="ui-button-text">Annulla</span></button>
                        %{--<g:link action="inviaMail" params="[tabId: params.tabId]" id="inviaMailB" name="inviaMailB" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right ladda-button pull-left ladda-button" data-style="expand-left"><span class="ladda-label"><i class="fa fa-chevron-left"></i></span><span class="ladda-spinner"></span>&nbsp;&nbsp;Invia mail</g:link>--}%
                        <g:actionSubmit value='Invia mail'  class='ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right bottone-commento' action='inviaMail' id="inviaMailB" name="inviaMailB"/>
                    </div>
                </g:form>

            </div>
        </div>
    </div>
    <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Inserisce commento</h4>
                </div>
                <g:form class="form-horizontal" method="post" params='[isMail:false, noSinistro:"${sinistri.noSinistro}"]'>
                    <input type="hidden" name="noSinistro" id="noSinistro" value="${sinistri.noSinistro?.toString().replace("[","").replace("]","")}"/>
                    <input type="hidden" name="isMailC" id="isMailC" value="false"/>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="messagetextC" class="dest">Commento:</label>
                            <textarea class="form-control" id="messagetextC" name="messagetextC"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right close-button bottone-chiude" data-dismiss="modal"><span class="">Annulla</span></button>
                        <g:actionSubmit value='Inserisce commento' class='ui-widget ui-state-default ui-corner-left ui-corner-right bottone-commento' action='inviaMail'  id="inviaMailC" name="inviaMailC" />

                    </div>
                </g:form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="contabileModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Dati contabili</h4>
            </div>
                <g:form class="form-horizontal" method="post" action="aggiornaDaticontabili" enctype="multipart/form-data" >
                    <div class="modal-body">
                        <input type="hidden" name="dettasx" id="dettasx" value=""/>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Tipo polizza</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all"  value="" name="tipoPolizza" id="tipoPolizza"/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>No. sinistro</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" name="noSx" id="noSx" value=""/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Intestatario polizza</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" name="intestatarioPolizza" id="intestatarioPolizza" value=""/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Intestatario fattura</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" name="intestatarioFattura" id="intestatarioFattura" value=""/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Importo fattura</b></label>
                            <input type="number" step=".01" lang="it" class="field field-content ui-widget-content ui-corner-all" name="importoFattura" id="importoFattura" value=""/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Importo liquidato</b></label>
                            <input type="number" step=".01" class="field field-content ui-widget-content ui-corner-all" name="importoLiquidato" id="importoLiquidato" value=""/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Diff. da integrare</b></label>
                            <input type="number" step=".01" class="field field-content ui-widget-content ui-corner-all" name="differenzaLiquidare" id="differenzaLiquidare" value=" "/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>Intestatario conto</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" name="intestatarioConto" id="intestatarioConto"  value=""/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <label class="control-label"><b>IBAN</b></label>
                            <input type="text" class="field field-content ui-widget-content ui-corner-all" name="iban" id="iban" value=""/>
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-4" >
                            <f:field type="date" name="dataPagamento"/>
                            %{--<label class="control-label"><b>Data pagamento</b></label>
                            <input type="date" class="field field-content ui-widget-content ui-corner-all" name="dataPagamento" id="dataPagamento" value="" />
                            <input class="field field-content ui-widget-content ui-corner-all" type="text" name="dataPagamento"  id="dataPagamento" value="" data-provide="datepicker" data-date-language="it"
                                   data-date-format="dd/mm/yyyy" data-date-today-btn="false" data-date-autoclose="true" data-date-end-date="0d" data-date-today-highlight="true" data-date-toggle-active="false"
                                   data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"  />--}%
                        </div>
                        <div class="col-lg-8" >
                            <label class="control-label"><b>Fattura</b></label>

                            <input id="input-fattura" type="file"  class="file  ui-widget-content ui-corner-all" multiple data-preview-file-type="text" name="fileFattura" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">
                            %{-- <textarea name="note" rows="2" cols="50" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.riparatore}</textarea>--}%
                        </div>
                        <div class="col-lg-12 allegati">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right close-button bottone-chiude" data-dismiss="modal"><span class="">Annulla</span></button>
                        %{--<g:actionSubmit value='Salva dati' class='ui-widget ui-state-default ui-corner-left ui-corner-right bottone-commento' action='salvaContabile'  id="salvaC" name="salvaC" />--}%
                        <button type="submit" id="bottonSalva" value="Salva dati" class="ui-widget ui-state-default ui-corner-left ui-corner-right bottone-commento">Salva dati</button>

                    </div>
                </g:form>
            </div>
        </div>
    </div>

    </body>
</html>