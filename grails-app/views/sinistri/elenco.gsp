<%@ page contentType="text/html;charset=UTF-8" %>

<html>
    <head>
        <title></title>
        <r:external file="/less/sinistri.less"/>
        <r:external file="/css/jquery.dataTables.css"/>
        <r:external file="/css/buttons.dataTables.min.css"/>
        <r:external file="/css/font-awesome.css"/>
        <r:script>
           /* $("#example tr").click(function(){
               $(this).addClass('selected').siblings().removeClass('selected');
                var value=$(this).find('td:first').html();
                alert(value);
            });*/
           $('.riga').click(function(){
               document.body.style.cursor='wait';
               return true;
           });
           window.onload=function(){document.body.style.cursor='default';}
           function dettaglioSinistro(nsx) {
               console.log(nsx);
               var sinistro = $('#'+nsx).val();
               console.log(sinistro);
               $.post("${createLink(controller: "sinistri", action: 'dettaglio')}",{ parametro: parametro, sinistro:sinistro}, function(response) {
                   console.log
            if(response.risposta==true ){

            }else{


            }
        }, "json");
           }
        </r:script>
        <style>
            th{

                font-size: 18px;
                /*text-align: center;*/
            }
        td{
            font-size: 16px;
        }

        </style>
    </head>
    <body>
    <div class="col-lg-12 pull-right">
        <div class="col-lg-12 col-sm-6 col-lg-pull-1" style="margin-bottom: 20px; font-size: 16px;">
            <flash:output/>
        </div>
        <div class="col-lg-12">
            <table id="example" name="example" class="table table-sm">
                <thead>
                <tr>
                    %{-- <th></th>--}%
                    <th>No. polizza</th>
                    <th>CF/P.IVA</th>
                    <th>Scadenza polizza</th>
                    <th>No. sinistro</th>
                    <th>Stato sinistro</th>
                    <th>Data sinistro</th>
                    <th>Evento</th>
                    <th>Iter</th>
                </tr>
                </thead>
                <tbody>
                <g:each var="sinistro" in="${sinistri}">
                    <tr onclick="document.getElementById('${sinistro.noSinistro.toString().replace('/', '_')}').submit()" id="riga" class="riga">
                        %{-- document.location = "<g:createLink action='dettaglio'  params='[sinistro: "${sinistro}", parametro:"${parametro}"]'/>" ' id="riga" class="riga">
                         <td><i class="fa fa-search-plus" style="cursor: pointer; font-size: 1.25em;" onclick="return viewSx(${sinistro.noSinistro});"></i></td>--}%
                        <td>
                            <form  method="post" id="${sinistro.noSinistro.toString().replace('/', '_')}" action="${createLink(controller: "sinistri", action: 'dettaglio')}">

                                <input type="hidden" name="sinistro" value="${sinistro}">
                                <input type="hidden" name="parametro" value="${parametro}">
                            </form>
                            ${sinistro?.noPolizza}</td>
                        <td>${sinistro?.codFiscale}</td>
                        <td>${sinistro?.scadenzadre}</td>
                        <td>${sinistro?.noSinistro}</td>
                        <td>${sinistro?.stato}</td>
                        <td>${sinistro?.dataSinistro}</td>
                        <td>${sinistro?.evento}</td>
                        <td>${sinistro?.iter}</td>
                    </tr>
                </g:each>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <a href="${createLink(controller: "sinistri", action: "ricerca")}"  class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right btn-default"><span class="ui-button-text">Torna alla ricerca</span></a>
        %{--<ui:button action="ricerca" text="Torna alla ricerca" />--}%
    </div>


    </body>
</html>