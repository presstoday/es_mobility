<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title></title>
        <r:external file="/less/sinistri.less"/>
        <r:external file="/css/jquery.dataTables.css"/>
        <r:external file="/css/buttons.dataTables.min.css"/>
        <r:external file="/css/bootstrap/css/bootstrap.css"/>
        <r:external file="/css/ladda.min.css"/>
        <r:external file="/css/ladda-themeless.min.css"/>
        <r:script>
            Ladda.bind( '.ladda-button' );

         $(document).ready(function() {

         } );
        $('.settLink').click(function(e) {
            e.preventDefault();
            /*$("#estTot").addClass("inactiveLink");
            $("#estSett").addClass("inactiveLink");*/
            var form = document.createElement("form"); //created dummy form for submitting.
            var element1 = document.createElement("input");
            form.method = "POST";
            form.action = "downloadpiuSett";
            element1.value="10"; //its a json string I need to pass to server.
            element1.name="dati";
            element1.type = 'hidden'
            form.appendChild(element1);
            document.body.appendChild(form);
            form.submit();
        });
        $('.totLink').click(function(e) {
            e.preventDefault();
            /*$("#estTot").addClass("inactiveLink");
            $("#estSett").addClass("inactiveLink");*/
            var form = document.createElement("form"); //created dummy form for submitting.
            var element1 = document.createElement("input");
            form.method = "POST";
            form.action = "downloadTot";
            element1.value="10"; //its a json string I need to pass to server.
            element1.name="dati";
            element1.type = 'hidden'
            form.appendChild(element1);
            document.body.appendChild(form);
            form.submit();
        });
        $('.apertiLink').click(function(e) {
            e.preventDefault();
            /*$("#estTot").addClass("inactiveLink");
             $("#estSett").addClass("inactiveLink");*/
            var form = document.createElement("form"); //created dummy form for submitting.
            var element1 = document.createElement("input");
            form.method = "POST";
            form.action = "downloadAperti";
            element1.value="10"; //its a json string I need to pass to server.
            element1.name="dati";
            element1.type = 'hidden'
            form.appendChild(element1);
            document.body.appendChild(form);
            form.submit();
        });
            $('.totContLink').click(function(e) {
                e.preventDefault();
                /*$("#estTot").addClass("inactiveLink");
                 $("#estSett").addClass("inactiveLink");*/
                var form = document.createElement("form"); //created dummy form for submitting.
                var element1 = document.createElement("input");
                form.method = "POST";
                form.action = "downloadContabileTot";
                element1.value="10"; //its a json string I need to pass to server.
                element1.name="dati";
                element1.type = 'hidden'
                form.appendChild(element1);
                document.body.appendChild(form);
                form.submit();
            });
        $('#cercabtn').click(function(){
            $('.circo').show();
        });
        /*$("#new-plafond").click(function() { $("#form-plafond").modal("show"); });*/
        </r:script>
    </head>
<style>
    body{
        background-color: white !important;
    }
</style>
    <body>
    <div class="row">
        <div class="col-lg-12 pull-right">
            <div class="col-lg-12 col-sm-6 col-lg-pull-1" style="margin-bottom: 20px; font-size: 16px;">
                <flash:output/>
            </div>
            <div class="col-lg-12" style="border-top: 2px solid #C2B49C; border-bottom: 2px solid #C2B49C;" >
                <legend><br><h1 style="float: left; margin-bottom: 40px;">GESTIONE SINISTRI</h1></legend>
            </div>
            <div class="col-lg-12 col-lg-pull-1 col-sm-6" style="margin-top: 30px;" >
                <form id="form-rango"  method="get">
                    <div class="col-lg-12">
                        <h2>Inserire Targa, no. di Sinistro, no. di Polizza o CF/P.IVA</h2>
                    </div>
                    <div class="col-lg-12">
                      <h3 class="col-lg-12" style="color: #C2B49C; word-wrap:break-word;">ad es. Targa:XX000YY, no.Sin. 000/0000, no. polizza:XX00000000Y, CF/P.IVA: AAABBB11C22D333E / 12345678911  </h3>

                    </div>
                    <div class="col-md-12  form-group">
                        <div class="row">
                            <div class="col-md-3 col-md-push-4 col-sm-4 col-sm-push-4 ">
                                <input class="field form-control" type="text"  id="parametro" name="parametro"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-md-push-4 col-sm-4 col-sm-push-4">
                                %{--<f:button class="aggiorna-button form-control button-ladda" type="submit" name="cerca" value="Cerca" id="cerca"/>--}%
                                <button type="submit" class="aggiorna-button form-control button-ladda button" name="cercabtn" id="cercabtn">Cerca <i class="fa fa-cog fa-spin fa-1x fa-fw circo" style="display: none;"></i></button>
                            </div>
                        </div>
                    </div>
                    %{--<div class="col-md-12" style="margin-top: 105px; border-top-color: #C2B49C; border-top-style: solid;">
                        <h3 class="">Sezione estrazioni</h3>
                        <div class="col-md-12 link1">
                            <div class="col-md-4 col-md-push-1"><a href="<g:createLink action="downloadpiuSett"/>"  id="estSett" name="estSett" class="settLink"><span class="" style="color: #C2B49C;"><i class="fa fa-file-excel-o fa-2x"></i> Estrazione sinistri aperti da più di una settimana</span></a></div>
                            <div class="col-md-4 col-md-push-1"><a id="estTotAperti" name="estTotAperti" href="<g:createLink action="downloadAperti"/>" class="apertiLink"><span class="" style="color: #C2B49C;"><i class="fa fa-file-excel-o fa-2x"></i> Estrazione totale sinistri aperti</span></a></div>
                            <div class="col-md-4 "><a id="estTot" name="estTot" href="<g:createLink action="downloadTot"/>" class="totLink"><span class="" style="color: #C2B49C;"><i class="fa fa-file-excel-o fa-2x"></i> Estrazione totale</span></a></div>
                            <div class="col-md-12" style="margin-bottom: 20px;"></div>

                            <div class="col-md-4 col-md-push-1"><a id="contTot" name="contTot" href="<g:createLink action="downloadContabileTot"/>" class="totContLink"><span class="" style="color: #C2B49C;"><i class="fa fa-file-excel-o fa-2x"></i> Estrazione contabile</span></a></div>
                        </div>
                        --}%%{--<div class="col-md-3"><a href="#" id="new-plafond" class="aggiorna-button form-control button-ladda button" title="Plafond"><b>Carica Contabile</b>  <i class="fa fa-upload"></i></a></div>--}%%{--
                    </div>--}%
                </form>
            </div>

        </div>

    </div>

        %{--<div class="modal" id="form-plafond">
            <div class="vertical-alignment-helper">
                <div class="modal-dialog vertical-align-center">
                    <div class="modal-content1">
                        <div class="modal-header" style="background: #C2B49C">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h3 class="article-title text-center titoli"> Caricamento Plafond</h3>
                        </div>
                        <div class="modal-body" style="background: white">
                            <form action="${createLink(action: "caricaPlafond")}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <input id="input-idExcelPratica" type="file" name="excelPratica" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                                --}%%{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%%{--
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right">Carica</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div>
            </div>
        </div>--}%
    </body>
</html>