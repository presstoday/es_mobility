<%@ page import="grails.converters.JSON" contentType="text/html;charset=UTF-8" %>
<style>
.ui-tooltip {
    /* If max-width does not work, try using width instead */
    max-width: 650px !important;

}
</style>
<html>
    <head>
        <title></title>
        <r:external file="/less/sinistri.less"/>
        <r:external file="/css/jquery.dataTables.css"/>
        <r:external file="/css/buttons.dataTables.min.css"/>
        <r:external file="/css/font-awesome.css"/>
        <r:external file="/css/bootstrap/css/bootstrap.css"/>
        <r:external file="/css/animate.css"/>
        <r:external file="/css/ladda.min.css"/>
        <r:external file="/css/ladda-themeless.min.css"/>
        <r:external file="/css/sweetalert.css"/>
        <r:script>
            $("[title]").tooltip();
            Ladda.bind( '.ladda-button' );
            $("body").mousemove(function(event) { $("#spinner").css({top: event.pageY, left: event.pageX + 16}); });
            $(document).ajaxStart(function() { $("#spinner").show(); }).ajaxStop(function() { $("#spinner").hide(); });
            $('#exampleModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('Invio email ')
                modal.find('.modal-body input').val(recipient)
            })
            $('#commentModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('Inserisce commento')
                modal.find('.modal-body input').val(recipient)
            })
            $('#inviaMailB').click(function(e) {
                e.preventDefault();
                invioEmail();
            });
            function invioEmail(){
                var noSinistro=$("#noSinistro").val();
                var messageM=$("#messageM").val();
                var isMail=$("#isMail").val();
                $.post("${createLink(controller: "sinistri", action: "inviaMail")}",{ noSinistro:noSinistro,messageM:messageM,isMail:isMail}, function(response) {
                    var risposta=response.risposta;
                    if(risposta==false){
                        var errore=response.errore;
                        swal({
                            title: "email  non inviato!",
                            type: "error",
                            timer: "1800",
                            showConfirmButton: false
                        });
                    }else{
                    swal({
                    title: "messaggio inviato!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                });
                     $('#exampleModal').modal('hide');
                     window.location.reload();
                    }
                }, "json");
            }
            $('#inviaMailC').click(function(e) {
                e.preventDefault();
                invioEmailC();
            });
            function invioEmailC(){
                var noSinistro=$("#noSinistro").val();
                var messageM=$("#messagetextC").val();
                var isMail=$("#isMailC").val();
                $.post("${createLink(controller: "sinistri", action: "inviaMail")}",{ noSinistro:noSinistro,messageM:messageM,isMail:isMail}, function(response) {
                    var risposta=response.risposta;
                    if(risposta==false){
                        var errore=response.errore;
                        swal({
                            title: "commento non inserito!",
                            type: "error",
                            timer: "1800",
                            showConfirmButton: false
                        });
                    }else{
                    swal({
                    title: "commento inserito!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                    });
                     $('#commentModal').modal('hide');
                     window.location.reload();
                    }
                }, "json");
            }
        </r:script>
    </head>

    <body>
        <flash:output/>
            <form id="sinistro" method="post" >
                <div id="right"><div class="campi"><a href="#" onclick="javascript:window.history.back();"  class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right"><span class="ui-button-text">Torna all'elenco</span></a></div></div>
                <div id="left"><div class="campi">
                <g:if test="${session.utente instanceof utenti.Admin}">
                    <button type="button" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right" data-toggle="modal" data-target="#commentModal" data-id="${sinistri?.noSinistro?.toString().replace("\\[","").replace("]","")}"><span class="ui-button-text">Commenta pratica</span></button></div></div>
                </g:if><g:else>
                <button type="button" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right" data-toggle="modal" data-target="#exampleModal" data-id="${sinistri?.noSinistro?.toString().replace("\\[","").replace("]","")}"><span class="ui-button-text">Sollecita pratica</span></button></div></div>
                </g:else>
                <g:each var="sinistro" in="${sinistri}">
                        <div id="right">
                            <div class="campi"><label><b>Nome</b></label></div>
                            <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.nome}"/></div>
                            <div class="campi"><label><b>Targa</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.targa}"/></div>
                            <div class="campi"><label><b>Numero Sinistro</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.noSinistro}"/></div>
                            <div class="campi"><label><b>Polizza</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.noPolizza}"/></div>
                            <div class="campi"><label><b>C.F./P.IVA</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.codFiscale}"/></div>
                            <div class="campi"><label><b>Riparatore</b></label></div>
                            <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.riparatore}"/></div>
                            <div class="campi"><label><b>Data Sinistro</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.dataSinistro}"/></div>
                            <div class="campi"><label><b>Data Ricevimento Denuncia</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.dataRicevimentoDenuncia}"/></div>
                            <div class="campi"><label><b>Evento</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.evento}"/>
                                <i class="fa fa-info-circle fa-2x" id="info" style="cursor: pointer; font-size: 1.50em;" title=" <p style='font-size:0.8em;'> <b>CID ATTIVO MONOFIRMA:</b> IN GESTIONE DA PARTE DELLA COMPAGNIA;  RICHIESTA DI RISARCIMENTO DANNI PERVENUTA SOLO DAL NOSTRO ASSICURATO; LA LIQUIDAZIONE AVVERRA’ ENTRO I 60GG  DALLA DOCUMENTAZIONE COMPLETA</p>
    <p style='font-size:0.8em;'> <b>CID ATTIVA DOPPIAFIRMA:</b>IN GESTIONE DA PARTE DELLA COMPAGNIA; LA RICHIESTA DI RISARCIMENTO CON FIRMA DI ENTRAMBE LE CONTROPARTI E’ COMPLETA, LA LIQUIDAZIONE AVVERRA’ ENTRO I 30GG DALLA DOCUMENTAZIONE COMPLETA</p>
    <p style='font-size:0.8em;'> <b>CID DEBITORE:</b>  SINISTRO NON GESTIBILE DALLA COMPAGNIA. RICHIESTA DI RISARCIMENTO DANNI SOTTOSCRITTA DA ENTRAMBE LA PARTI. RESPONSABILITA’ TOTALE DEL NOSTRO ASSICURATO; LIQUIDABILE SOLO CON APERTURA GARANZIA KASKO CON FRANCHIGIA A CARICO DEL CLIENTE</p>
    <p style='font-size:0.8em;'> <b>RC ATTIVO:</b> SINISTRO NON GESTIBILE DALLA COMPAGNIA; LA LIQUIDAZIONE POTRA’ AVVENIRE SOLO CON RICHIESTA DANNI ALLA COMPAGNIA DI CONTROPARTE (CON L’EVENTUALE PATROCINIO DEL NOSTRO LEGALE). LA GARANZIA KASKO POTRA’ ESSERE ATTIVATA CON FRANCHIGIA A CARICO DEL CLIENTE</p>
    <p style='font-size:0.8em;'> <b>RC PASSIVO:</b> SINISTRO NON GESTIBILE DALLA COMPAGNIA.  RESPONSABILITA’ TOTALE DEL NOSTRO ASSICURATO; LIQUIDABILE SOLO CON APERTURA GARANZIA KASKO CON FRANCHIGIA A CARICO DEL CLIENTE</p>
    <p style='font-size:0.8em;'>NUOVO ITER:<b>CONCORSUALE:</b> </p>
    <p style='font-size:0.8em;'>SINISTRO IN GESTIONE ALLA COMPAGNIA; RICHIESTA DI RISARCIMENTO DANNI DA CUI SI EVINCE LA RESPONSABILITA’ DI ENTRAMBE LE PARTI (E’ POSSIBILE ANCHE CON PERCENTUALI DIVERSE).  GARANZIA KASKO ATTIVABILE CON PAGAMENTO FRANCHIGIA A CARICO DEL CLIENTE SOLO LA COMPLETA DEFINIZIONE DEL SINISTRO DA PARTE DELLA COMPAGNIA</p>
    "></i></div>
                            <div class="campi"><label><b>Descrizione</b></label></div>
                            <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.descrizione}"/></div>
                            <div class="campi"><label><b>Descrizione 2</b></label></div>
                            <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.descrizione2}"/></div>
                            <div class="campi"><label><b>Iter</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.iter}"/></div>
                            <div class="campi"><label><b>Data Iter</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.dataIter}"/></div>
                            <div class="campi"><label><b>Stato</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.stato}"/></div>
                            <div class="campi"><label><b>Preventivo</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.preventivo}"/></div>
                            <div class="campi"><label><b>Data Preventivo</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.dataPreventivo}"/></div>
                            <div class="campi"><label><b>Concordato</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.concordato}"/></div>
                            <div class="campi"><label><b>Data Concordato</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.dataConcordato}"/></div>
                        </div>
                        <div id="left">
                            <div class="campi"><label><b>Pagato</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.pagato}"/></div>
                            <div class="campi"><label><b>Data Pagamento</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.dataPagamento}"/></div>
                            <div class="campi"><label><b>Scoperto</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.scoperto}"/></div>
                            <div class="campi"><label><b>Franchigia</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.franchigia}"/></div>
                            <div class="campi"><label><b>Degrado</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.degrado}"/></div>
                            <div class="campi"><label><b>Documentazione completa</b></label></div>
                            <div class="campi"><input type="text" class="field field-data ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.documentazionecompleta}"/></div>
                            <g:if test="${sinistro?.docu1}">
                                <div class="campi"><label><b>Documentazione 1</b></label></div>
                                <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu1}"/></div>
                            </g:if>
                            <g:if test="${sinistro?.docu2}">
                                <div class="campi"><label><b>Documentazione 2</b></label></div>
                                <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu2}"/></div>
                            </g:if>
                            <g:if test="${sinistro?.docu3}">
                                <div class="campi"><label><b>Documentazione 3</b></label></div>
                                <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu3}"/></div>
                            </g:if>
                            <g:if test="${sinistro?.docu4}">
                                <div class="campi"><label><b>Documentazione 4</b></label></div>
                                <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu4}"/></div>
                            </g:if>
                            <g:if test="${sinistro?.docu5}">
                                <div class="campi"><label><b>Documentazione 5</b></label></div>
                                <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu5}"/></div>
                            </g:if>
                            <g:if test="${sinistro?.docu6}">
                                <div class="campi"><label><b>Documentazione 6</b></label></div>
                                <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu6}"/></div>
                            </g:if>
                            <g:if test="${sinistro?.docu7}">
                                <div class="campi"><label><b>Documentazione 7</b></label></div>
                                <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu7}"/></div>
                            </g:if>
                            <g:if test="${sinistro?.docu8}">
                                <div class="campi"><label><b>Documentazione 8</b></label></div>
                                <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu8}"/></div>
                            </g:if>
                            <g:if test="${sinistro?.docu9}">
                                <div class="campi"><label><b>Documentazione 9</b></label></div>
                                <div class="campi"><input type="text" class="field field-content ui-widget-content ui-corner-all" readonly="true" value="${sinistro?.docu9}"/></div>
                            </g:if>
                            <div class="campi"><label><b>Note</b></label></div>
                            <div class="campi"><textarea name="note" rows="10" cols="40" readonly="true" class="field field-content ui-widget-content ui-corner-all">${sinistro?.note}</textarea></div>
                            <div class="campi"><label><b>Allegati</b></label></div>
                            <g:if test="${sinistro?.filenames}">
                                <g:each var="allegato" in="${sinistro?.filenames}">
                                    <div class="campi">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <a href="http://www.nais.biz/rinnovi-vw/v2/api.php/ftp?f=${allegato.url?.toString()}" target="_blank">
                                                    <i class="fa ${allegato.icon?.toString()} fa-2x" style="color: #C2B49C;"></i>
                                                </a>
                                                ${allegato.nome?.toString()}
                                            </div>
                                            <div class="col-md-4">${allegato.data?.toString()}</div>
                                        </div>
                                    </div>
                                    %{--<i class="fa fa-file-archive-o" style="cursor: pointer; font-size: 1.0em;" onclick="return downloadFile(${sinistro.noSinistro});"></i>
                                    <g:if test="${allegato.toString().replace("[","").replace("]","").contains("doc")}" ><f:field type="output" value="${allegato.toString().replace("[","").replace("]","")}"/><i class="fa fa-file-text" style="cursor: pointer; font-size: 1.0em;" onclick="return viewSx(${sinistro.noSinistro});"></i></g:if>
                                    <g:if test="${allegato.toString().replace("[","").replace("]","").contains("DOC")}" ><f:field type="output" value="${allegato.toString().replace("[","").replace("]","")}"/><i class="fa fa-file-text" style="cursor: pointer; font-size: 1.0em;" onclick="return viewSx(${sinistro.noSinistro});"></i></g:if>--}%
                                </g:each>
                            </g:if><g:else>
                            <h4 style="margin-right: 480px;"><i>Non ci sono allegati</i></h4>
                        </g:else>
                        </div>
                        <g:if test="${commenti}">

                        <div class="row">
                            <div class="col-md-12">
                                <h1>Storico solleciti</h1>
                                <div class="chat-discussion">
                                    <g:each var="commento" in="${commenti}">
                                        <g:if test="${commento.utente.username.toString().replaceAll("\\[","").replaceAll("]","") !="admin"}">
                                            <div class="chat-message left">
                                                <i class="message-avatar" aria-hidden="true">  <r:img dir="images/mach1" file="es_small.png" id="logo-esmobility" height="80%" width="100%"/></i>
                                                <div class="message">
                                                    <div class="message-author" href="#"> ${commento.dataInserimento.format("E, dd MMM yyyy HH:mm:ss")}&nbsp&nbsp&nbsp ${commento.utente.username.toString().replaceAll("\\[","").replaceAll("]","").capitalize()}</div>
                                                    <span class="message-content">${commento.messaggio}</span>
                                                </div>
                                            </div>
                                        </g:if>
                                        <g:else>
                                            <div class="chat-message right">
                                                <i class="message-avatar" aria-hidden="true">  <r:img dir="images/mach1" file="logo_mach1_small.png" id="logo-esmobility" height="80%" width="100%"/></i>
                                                <div class="message">
                                                    <div class="message-author" href="#"> ${commento.dataInserimento.format("E, dd MMM yyyy HH:mm:ss")}&nbsp&nbsp&nbsp${commento.utente.username.toString().replaceAll("\\[","").replaceAll("]","").capitalize()}</div>
                                                    <span class="message-date">  </span>
                                                    <span class="message-content">${commento.messaggio}</span>
                                                </div>
                                            </div>
                                        </g:else>

                                    </g:each>


                                </div>

                            </div>

                        </div>

                    </g:if>
                </g:each>
            </form>


            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="exampleModalLabel">New message</h4>
                        </div>
                        <g:form class="form-horizontal" method="post" params='[isMail:true, noSinistro_:"${sinistri.noSinistro}"]'>
                            <input type="hidden" name="noSinistro" id="noSinistro" value="${sinistri.noSinistro?.toString().replace("[","").replace("]","")}"/>
                            <input type="hidden" name="isMail" id="isMail" value="true"/>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="dest">Destinatario:</label>
                                    <label style="font-weight: normal">lld@mach-1.it</label>
                                    <div>
                                        <label class="dest">OGGETTO:</label>
                                        <label style="font-weight: normal"> SOLLECITO SINISTRO ${sinistri?.noSinistro.toString().replace("[","").replace("]","")}</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="messageM" class="dest">Messaggio:</label>
                                    <textarea class="form-control" id="messageM" name="messageM"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right close-button bottone-chiude" data-dismiss="modal"><span class="ui-button-text">Annulla</span></button>
                                %{--<g:link action="inviaMail" params="[tabId: params.tabId]" id="inviaMailB" name="inviaMailB" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right ladda-button pull-left ladda-button" data-style="expand-left"><span class="ladda-label"><i class="fa fa-chevron-left"></i></span><span class="ladda-spinner"></span>&nbsp;&nbsp;Invia mail</g:link>--}%
                                <g:actionSubmit value='Invia mail'  class='ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right bottone-commento' action='inviaMail' id="inviaMailB" name="inviaMailB"/>
                            </div>
                        </g:form>

                    </div>
                </div>
            </div>
            <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">Inserisce commento</h4>
                        </div>
                        <g:form class="form-horizontal" method="post" params='[isMail:false, noSinistro:"${sinistri.noSinistro}"]'>
                            <input type="hidden" name="noSinistro" id="noSinistro" value="${sinistri.noSinistro?.toString().replace("[","").replace("]","")}"/>
                            <input type="hidden" name="isMailC" id="isMailC" value="false"/>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="messagetextC" class="dest">Commento:</label>
                                    <textarea class="form-control" id="messagetextC" name="messagetextC"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right close-button bottone-chiude" data-dismiss="modal"><span class="">Annulla</span></button>
                                <g:actionSubmit value='Inserisce commento' class='ui-widget ui-state-default ui-corner-left ui-corner-right bottone-commento' action='inviaMail'  id="inviaMailC" name="inviaMailC" />

                            </div>
                        </g:form>
                    </div>
                </div>
            </div>

    </body>
</html>