package esmobility

import grails.util.Environment
import groovy.time.TimeCategory
import sinistri.Estrazioni
import sinistri.Log
import sinistri.TipoEst

class EstraApertiJob {
    def IAssicurWebService
    def ExcelReportsService

    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
           // cron name: 'estrazioneAperti', cronExpression: '0 30 07 * * ?'
           // cron name: 'estrazioneAperti', cronExpression: '0 30 7,14 * * ?'

        }
    }
    def execute() {
        log.info 'EstraApertiJob triggered'
        EstrazioneAperti()
    }
    def EstrazioneAperti() {
        def logg
        def dataOdierna=new Date()
        def resultwebS = IAssicurWebService.getSREAperti()
        def sinistriWS = resultwebS.sinistri
        logg = new Log(parametri: "numero di sinistri aperti trovati su IAssciur nel periodo selezionato ${sinistriWS.size()} ", operazione: "generaREPORTAPERTI", pagina: "JOB ESTRAZIONE  APERTI")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
         if(sinistriWS){
             def excel = ExcelReportsService.estrazioneRCAAperti(sinistriWS)
             def nuovaEst=new Estrazioni()
             nuovaEst.fileContent=excel
             nuovaEst.settimanaEstra=dataOdierna
             nuovaEst.fileName="Estrazione sinistri aperti.xls"
             nuovaEst.totRecords=sinistriWS.size()
             nuovaEst.tipo= TipoEst.ELENCO_APERTI
             if(!nuovaEst.save(flush: true)){
                 logg = new Log(parametri: "il file non e' stato salvato controllare ${nuovaEst.errors}", operazione: "generaREPORTAPERTI", pagina: "JOB ESTRAZIONE  APERTI")
                 if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
             }else{
                 logg = new Log(parametri: "file salvato correttamente", operazione: "generaREPORTAPERTI", pagina: "JOB ESTRAZIONE  APERTI")
                 if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
             }

         }
     }
}
