package esmobility

import grails.util.Environment
import groovy.time.TimeCategory
import sinistri.Estrazioni
import sinistri.Log

class EstraSettJob {
    def IAssicurWebService
    def ExcelReportsService

    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            //cron name: 'estrazioneSett', cronExpression: '0 00 06 * * ?'
            cron name: 'estrazioneSett', cronExpression: '0 00 6,12 * * ?'
        }

    }
    def execute() {
        log.info 'EstraSettJob triggered'
        EstrazioneSett()
    }
    def EstrazioneSett() {
        def logg
        def dataOdierna=new Date().format("dd.MM.yyyy")
        def settimana=use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -7.day}.format("dd_MM_yyyy")
        def settimanaEstr=use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -7.day}
        def resultwebS = IAssicurWebService.getSRESett()
        def sinistriWS = resultwebS.sinistri
        logg = new Log(parametri: "numero di sinistri trovati su IAssciur nel periodo selezionato ${sinistriWS.size()} settimana ${settimana}", operazione: "generaREPORTSETT", pagina: "JOB ESTRAZIONE SETTIMANALE")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
         if(sinistriWS){
             def estrazione=Estrazioni.findByFileNameIlike("Estrazione settimanale ${settimana}.xls")
             //println "Estrazione settimanale ${settimana}.xls"
             if(!estrazione){
                 def excel = ExcelReportsService.estrazioneSett(sinistriWS, settimana)
                 def nuovaEst=new Estrazioni()
                 nuovaEst.fileContent=excel
                 nuovaEst.settimanaEstra=settimanaEstr
                 nuovaEst.fileName="Estrazione settimanale ${settimana}.xls"
                 nuovaEst.totRecords=sinistriWS.size()
                 if(!nuovaEst.save(flush: true)){
                     logg = new Log(parametri: "il file non e' stato salvato controllare ${nuovaEst.errors}", operazione: "generaREPORTSETT", pagina: "JOB ESTRAZIONE SETTIMANALE")
                     if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                 }else{
                     logg = new Log(parametri: "file salvato correttamente", operazione: "generaREPORTSETT", pagina: "JOB ESTRAZIONE SETTIMANALE")
                     if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                 }
             }else{
                 logg = new Log(parametri: "estrazione già generata", operazione: "generaREPORTSETT", pagina: "JOB ESTRAZIONE SETTIMANALE")
                 if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
             }


         }
     }
}
