package esmobility

import grails.util.Environment
import sinistri.Estrazioni
import sinistri.Log
import sinistri.TipoEst

class EstraNottJob {
    def IAssicurWebService
    def ExcelReportsService

    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            cron name: 'estrazioneNott', cronExpression: '0 00 23 * * ?'
        }
        //cron name: 'estrazioneNott', cronExpression: '0 45 8,9 * * ?'

    }
    def execute() {
        log.info 'EstraNottJob triggered'
        EstrazioneNott()
    }
    def EstrazioneNott() {
        def logg
        def dataOdierna=new Date()
        def resultwebS = IAssicurWebService.getSRENott()
        def sinistriWS = resultwebS.sinistri
        def caricato=false
        logg = new Log(parametri: "numero di sinistri trovati su IAssciur nel periodo selezionato ${sinistriWS.size()} ", operazione: "generaREPORTNOTT", pagina: "JOB ESTRAZIONE TOTALE NOTT")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        if(sinistriWS){
            def excel = ExcelReportsService.estrazioneNott(sinistriWS)
            if(excel){
                def nomefile="sinistri${dataOdierna.format("yyyyMMddHHmmss")}.csv"
                def nuovaEst=new Estrazioni()
                nuovaEst.fileContent=excel
                nuovaEst.fileName=nomefile
                nuovaEst.totRecords=sinistriWS.size()
                nuovaEst.settimanaEstra=dataOdierna
                nuovaEst.tipo= TipoEst.ELENCO_NOTTURNI
                if(!nuovaEst.save(flush: true)){
                    logg = new Log(parametri: "il file non e' stato salvato controllare ${nuovaEst.errors}", operazione: "generaREPORTNOTT", pagina: "JOB ESTRAZIONE TOTALE")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{
                    logg = new Log(parametri: "file salvato correttamente", operazione: "generaREPORTNOTT", pagina: "JOB ESTRAZIONE TOTALE")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    caricato= ExcelReportsService.ftpsuploadFile(excel, nomefile)
                    if(caricato){
                        logg = new Log(parametri: "file caricato correttamente", operazione: "generaREPORTNOTT", pagina: "JOB ESTRAZIONE TOTALE")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }else{
                        logg = new Log(parametri: "file non caricato controllare", operazione: "generaREPORTNOTT", pagina: "JOB ESTRAZIONE TOTALE")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }

                }
            }

        }
    }
}
