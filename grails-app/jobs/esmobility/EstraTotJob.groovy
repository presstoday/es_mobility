package esmobility

import grails.util.Environment
import groovy.time.TimeCategory
import sinistri.Estrazioni
import sinistri.Log
import sinistri.TipoEst


class EstraTotJob {
    def IAssicurWebService
    def ExcelReportsService

    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            //cron name: 'estrazioneTot', cronExpression: '0 30 6 * * ?'
           // cron name: 'estrazioneTot', cronExpression: '0 0 0/8 1/1 * ?'
            cron name: 'estrazioneTot', cronExpression: '0 30 6,9,15,18 * * ?'
            //cron name: 'estrazioneTotUnaTantum', cronExpression: '0 35 16 19 10 ? 2018'
        }
        //cron name: 'estrazioneTot', cronExpression: '0 15 9,10 * * ?'
    }
    def execute() {
        log.info 'EstraTotJob triggered'
        EstrazioneTot()
    }
    def EstrazioneTot() {
        def logg
        def dataOdierna=new Date()
        def resultwebS = IAssicurWebService.getSRETot()
        def sinistriWS = resultwebS.sinistri
        logg = new Log(parametri: "numero di sinistri trovati su IAssciur nel periodo selezionato ${sinistriWS.size()} ", operazione: "generaREPORTTOT", pagina: "JOB ESTRAZIONE TOTALE")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

        if(sinistriWS){
           /* def vecchiaEst=Estrazioni.withCriteria {
                ge 'dateCreated', dataOdierna.clearTime()
                eq 'tipo', TipoEst.ELENCO_TOTALE
                order 'lastUpdated', 'desc'
            }

            if(vecchiaEst){
                if(vecchiaEst.first().totRecords ==sinistriWS.size()){
                    logg = new Log(parametri: "il totale dei records e' uguale a quello precedente", operazione: "generaREPORTTOT", pagina: "JOB ESTRAZIONE TOTALE")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{*/
                    def excel = ExcelReportsService.estrazioneTot(sinistriWS)
                    def nuovaEst=new Estrazioni()
                    nuovaEst.fileContent=excel
                    nuovaEst.fileName="Estrazione totale.xls"
                    nuovaEst.totRecords=sinistriWS.size()
                    nuovaEst.settimanaEstra=dataOdierna
                    nuovaEst.tipo= TipoEst.ELENCO_TOTALE
                    if(!nuovaEst.save(flush: true)){
                        logg = new Log(parametri: "il file non e' stato salvato controllare ${nuovaEst.errors}", operazione: "generaREPORTTOT", pagina: "JOB ESTRAZIONE TOTALE")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }else{
                        logg = new Log(parametri: "file salvato correttamente", operazione: "generaREPORTTOT", pagina: "JOB ESTRAZIONE TOTALE")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                /*}
            }else{
                logg = new Log(parametri: "non ci sono estrazioni generate nelle ultime ore", operazione: "generaREPORTTOT", pagina: "JOB ESTRAZIONE TOTALE")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                def excel = ExcelReportsService.estrazioneTot(sinistriWS)
                println "ho il file"
                def nuovaEst=new Estrazioni()
                nuovaEst.fileContent=excel
                nuovaEst.fileName="Estrazione totale.xls"
                nuovaEst.totRecords=sinistriWS.size()
                nuovaEst.settimanaEstra=dataOdierna
                nuovaEst.tipo= TipoEst.ELENCO_TOTALE
                if(!nuovaEst.save(flush: true)){
                    logg = new Log(parametri: "il file non e' stato salvato controllare ${nuovaEst.errors}", operazione: "generaREPORTTOT", pagina: "JOB ESTRAZIONE TOTALE")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{
                    logg = new Log(parametri: "file salvato correttamente", operazione: "generaREPORTTOT", pagina: "JOB ESTRAZIONE TOTALE")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
            }*/

        }
    }
}
