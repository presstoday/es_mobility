package esmobility

import grails.util.Environment
import sinistri.Daticontabili
import sinistri.Estrazioni
import sinistri.Log
import sinistri.OrigineDatocontabile
import sinistri.TipoEst

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class EstraDatiContJob {
    def IAssicurWebService

    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
          //non caricare questo job, perchè vengono caricati dal job EstraSxDBJob  cron name: 'EstraDatiCont', cronExpression: '0 00 8,20 * * ?'
        }
    }
    def execute() {
        log.info 'EstraDatiContJob triggered'
        EstrazioneDatiCont()
    }
    def EstrazioneDatiCont() {
        def logg
        def resultwebS = IAssicurWebService.getDatiCont()
        def sinistriWS = resultwebS.sinistri
        logg = new Log(parametri: "numero di sinistri trovati su IAssciur nel periodo selezionato ${sinistriWS.size()} ", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        if(sinistriWS){
            def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
            String pattern = "#,##0.0#"
            symbols.setGroupingSeparator((char) ',')
            symbols.setDecimalSeparator((char) '.')
            def  decimalFormat = new DecimalFormat(pattern, symbols)
            decimalFormat.setParseBigDecimal(true)
            sinistriWS.each { sinistro ->
                def nosx=sinistro.noSinistro
                def olddatoC=Daticontabili.findByNoSinistro(nosx.toString().trim())
                if(!olddatoC){
                    /*sinistro.importofattura=sinistro.importofattura.toString().replace(".","")
                    sinistro.importofattura=sinistro.importofattura.toString().replace(",",".")*/
                    sinistro.importoliquidato=sinistro.importoliquidato.toString().replace(".","")
                    sinistro.importoliquidato=sinistro.importoliquidato.toString().replace(",",".")
                    def datoC=new Daticontabili()
                    datoC.noSinistro=sinistro.noSinistro.toString().trim()
                    datoC.noFattura=sinistro.noSinistro.toString().trim()
                    datoC.intestatarioPolizza=sinistro.intestatariopolizza
                    datoC.intestatarioConto=sinistro.intestatarioconto
                   // datoC.importoFattura=(BigDecimal) decimalFormat.parse(sinistro.importofattura)
                    datoC.importoLiquidato=(BigDecimal) decimalFormat.parse(sinistro.importoliquidato)
                    //datoC.differenzaLiquidare=0.0
                    datoC.dataPagamento=sinistro.datapagamento? Date.parse( 'dd/MM/yyyy', sinistro.datapagamento):null
                    datoC.origine=OrigineDatocontabile.CARICATODAIASSICUR
                    datoC.ramo=sinistro.ramo.toString().trim()
                    if((sinistro.ramo.toString().contains('L/90') || sinistro.ramo.toString().contains('L/74') || sinistro.ramo.toString().contains('L/75')) && sinistro.compagnia.toString().contains('A32')){
                        datoC.tipoPolizza='AXA LLD'
                    }else if((sinistro.ramo.toString().contains('L/90') || sinistro.ramo.toString().contains('K/44') || sinistro.ramo.toString().contains('K/45') || sinistro.ramo.toString().contains('O/29')) && (sinistro.compagnia.toString().contains('A44') || sinistro.compagnia.toString().contains('A54'))){
                        datoC.tipoPolizza='HELVETIA RCI DEMO'
                    }else if((sinistro.ramo.toString().contains('O/94')|| (sinistro.ramo.toString().contains('O/95'))) && (sinistro.compagnia.toString().contains('A54'))){
                        datoC.tipoPolizza='HELVETIA RCI NOLEGGIO'
                    }else if((sinistro.ramo.toString().contains('K/36')) && (sinistro.compagnia.toString().contains('A44') || sinistro.compagnia.toString().contains('A54'))){
                        datoC.tipoPolizza='HELVETIA VIP RCI NOLEGGIO'
                    }
                    if(!datoC.save(flush:true)){
                        logg =new Log(parametri: "errore caricamento dato contabile ${datoC.errors}", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }else{
                        logg = new Log(parametri: "dato salvato correttamente", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }else{
                    if(olddatoC.ramo =='' || olddatoC.ramo ==null){
                        //println "entro qui ${olddatoC.noSinistro}"
                        olddatoC.ramo=sinistro.ramo.toString().trim()
                        if(!olddatoC.save(flush:true)){
                            logg =new Log(parametri: "errore caricamento dato contabile ${olddatoC.errors}", operazione: "aggiornaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }else{
                            logg = new Log(parametri: "dato salvato correttamente", operazione: "aggiornaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                    }
                    logg = new Log(parametri: "il dato contabile e' gia' stato caricato", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }

            }
        }
    }
}
