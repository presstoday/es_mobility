package esmobility

import grails.util.Environment
import sinistri.Daticontabili
import sinistri.Log
import sinistri.Sinistridb
import sinistri.OrigineDatocontabile

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class EstraSxDBJob {
    def IAssicurWebService

    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            cron name: 'EstraDatiSxDB', cronExpression: '0 00 09,11,15,19 * * ?'
        }
        //cron name: 'EstraDatiSxDB', cronExpression: '0 30 09,11,15,17,19 * * ?'
    }
    def execute() {
        log.info 'EstraDatiSxDBJob triggered'
        EstrazioneSinistriDB()
    }
    def EstrazioneSinistriDB() {
        def logg
        def resultwebS = IAssicurWebService.getDatiSxIassicur()
        def sinistriWS = resultwebS.sinistri
        logg = new Log(parametri: "numero di sinistri trovati su IAssciur nel periodo selezionato ${sinistriWS.size()} ", operazione: "carica sx", pagina: "JOB ESTRAZIONE SINISTRI")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        if(sinistriWS){
            def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
            String pattern = "#,##0.0#"
            symbols.setGroupingSeparator((char) ',')
            symbols.setDecimalSeparator((char) '.')
            def  decimalFormat = new DecimalFormat(pattern, symbols)
            decimalFormat.setParseBigDecimal(true)
            sinistriWS.each { sinistro ->
                logg = new Log(parametri: "questi sono i dati del sinistro: ${sinistro}", operazione: "carica sx", pagina: "JOB ESTRAZIONE SINISTRI")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                def nosx=sinistro.noSinistro
                def olddatoSX=Sinistridb.findByNosinistro(nosx.toString().trim())
                if(!olddatoSX){
                    sinistro.offerta=sinistro.offerta.toString().replace(".","")
                    sinistro.offerta=sinistro.offerta.toString().replace(",",".")
                    sinistro.richiesta=sinistro.richiesta.toString().replace(".","")
                    sinistro.richiesta=sinistro.richiesta.toString().replace(",",".")
                    sinistro.liquidazione=sinistro.liquidazione.toString().replace(".","")
                    sinistro.liquidazione=sinistro.liquidazione.toString().replace(",",".")
                    sinistro.scoperto=sinistro.scoperto.toString().replace(".","")
                    sinistro.scoperto=sinistro.scoperto.toString().replace(",",".")
                    sinistro.franchigia=sinistro.franchigia.toString().replace(".","")
                    sinistro.franchigia=sinistro.franchigia.toString().replace(",",".")
                    def sinx=new Sinistridb()
                    sinx.nosinistro=sinistro.noSinistro.toString().trim()
                    sinx.nopolizza=sinistro.nopolizza.toString().trim()
                    sinx.nome=sinistro.nome
                    sinx.codicefiscale=sinistro.codicefiscale
                    sinx.riparatore=sinistro.riparatore?:null
                    sinx.partitaivariparatore=sinistro.partitaivariparatore?:null
                    sinx.targa=sinistro.targa
                    sinx.evento=sinistro.evento
                    sinx.descrizione=sinistro.descrizione
                    sinx.descrizione2=sinistro.descrizione2
                    sinx.iter=sinistro.iter
                    sinx.stato=sinistro.stato
                    sinx.nota=sinistro.note
                    sinx.ipertesto=sinistro.ipertesto
                    sinx.richiesta=sinistro.richiesta ? (BigDecimal) decimalFormat.parse(sinistro.richiesta):0.0
                    sinx.liquidazione=sinistro.liquidazione ?(BigDecimal) decimalFormat.parse(sinistro.liquidazione) :0.0
                    sinx.offerta=sinistro.offerta ? (BigDecimal) decimalFormat.parse(sinistro.offerta):0.0
                    sinx.scoperto=sinistro.scoperto ? (BigDecimal) decimalFormat.parse(sinistro.scoperto):0.0
                    sinx.franchigia=sinistro.franchigia ? (BigDecimal) decimalFormat.parse(sinistro.franchigia) :0.0
                    sinx.dataapertura=sinistro.dataapertura? Date.parse( 'dd/MM/yyyy', sinistro.dataapertura):null
                    sinx.datasinistro=sinistro.datasinistro? Date.parse( 'dd/MM/yyyy', sinistro.datasinistro):null
                    sinx.datadenuncia=sinistro.datadenuncia? Date.parse( 'dd/MM/yyyy', sinistro.datadenuncia):null
                    sinx.dataiter=sinistro.dataiter? Date.parse( 'dd/MM/yyyy', sinistro.dataiter):null
                    sinx.datarichiesta=sinistro.datarichiesta? Date.parse( 'dd/MM/yyyy', sinistro.datarichiesta):null
                    sinx.dataofferta=sinistro.dataofferta? Date.parse( 'dd/MM/yyyy', sinistro.dataofferta):null
                    sinx.dataliquidazione=sinistro.dataliquidazione? Date.parse( 'dd/MM/yyyy', sinistro.dataliquidazione):null
                    sinx.ramo=sinistro.ramo.toString().trim()
                    //println "${sinistro.ramo.toString()}"
                    //println "${sinistro.compagnia.toString()}"
                    if((sinistro.ramo.toString().contains('L/90') || sinistro.ramo.toString().contains('L/74') || sinistro.ramo.toString().contains('L/75')) && sinistro.compagnia.toString().contains('A32')){
                        sinx.compagnia='AXA LLD'
                    }else if((sinistro.ramo.toString().contains('L/90') || sinistro.ramo.toString().contains('K/44') || sinistro.ramo.toString().contains('K/45') || sinistro.ramo.toString().contains('O/29')) && (sinistro.compagnia.toString().contains('A44') || sinistro.compagnia.toString().contains('A54'))){
                        sinx.compagnia='HELVETIA RCI DEMO'
                    }else if((sinistro.ramo.toString().contains('O/94') || (sinistro.ramo.toString().contains('O/95'))) && (sinistro.compagnia.toString().contains('A54'))){
                        sinx.compagnia='HELVETIA RCI NOLEGGIO'
                    }else if((sinistro.ramo.toString().contains('K/36')) && (sinistro.compagnia.toString().contains('A44') || sinistro.compagnia.toString().contains('A54'))){
                        sinx.compagnia='HELVETIA VIP RCI NOLEGGIO'
                    }
                    if(!sinx.save(flush:true)){
                        logg =new Log(parametri: "errore caricamento sinistro: ${sinistro.noSinistro} --> ${sinx.errors}", operazione: "generaSX", pagina: "JOB ESTRAZIONE SX")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }else{
                        if(sinx.stato=="DEFINITO" && (sinx.dataliquidazione !="" && sinx.dataliquidazione !=null && sinx.dataliquidazione !="NULL" && sinx.dataliquidazione !="null")){
                            logg = new Log(parametri: "entro qui per generarne uno nuovo--> ${sinx.nosinistro} --> ${sinx.dataliquidazione}--> ramo ${sinx.ramo.toString()}", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE SINISTRI")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            def olddatoC=Daticontabili.findByNoSinistro(nosx.toString().trim())
                            if(!olddatoC){
                                /*sinistro.importofattura=sinistro.importofattura.toString().replace(".","")
                                sinistro.importofattura=sinistro.importofattura.toString().replace(",",".")*/
                                sinistro.importoliquidato=sinistro.importoliquidato.toString().replace(".","")
                                sinistro.importoliquidato=sinistro.importoliquidato.toString().replace(",",".")
                                def datoC=new Daticontabili()
                                datoC.noSinistro=sinx.nosinistro.toString().trim()
                                datoC.noFattura=sinx.nosinistro.toString().trim()
                                datoC.intestatarioPolizza=sinx.nome
                                datoC.intestatarioConto=sinx.riparatore
                                // datoC.importoFattura=(BigDecimal) decimalFormat.parse(sinistro.importofattura)
                                datoC.importoLiquidato=sinx.liquidazione ? sinx.liquidazione:0.0
                                //datoC.differenzaLiquidare=0.0
                                datoC.dataPagamento=sinx.dataliquidazione? sinx.dataliquidazione:null
                                datoC.origine=OrigineDatocontabile.CARICATODAIASSICUR
                                datoC.ramo=sinx.ramo.toString().trim()
                                if((sinx.ramo.toString().contains('L/90') || sinx.ramo.toString().contains('L/74') || sinx.ramo.toString().contains('L/75')) && sinistro.compagnia.toString().contains('A32')){
                                    datoC.tipoPolizza='AXA LLD'
                                }else if((sinx.ramo.toString().contains('L/90') || sinx.ramo.toString().contains('K/44') || sinx.ramo.toString().contains('K/45') || sinx.ramo.toString().contains('O/29')) && (sinistro.compagnia.toString().contains('A44') || sinx.compagnia.toString().contains('A54'))){
                                    datoC.tipoPolizza='HELVETIA RCI DEMO'
                                }else if((sinx.ramo.toString().contains('O/94') || (sinx.ramo.toString().contains('O/95'))) && (sinistro.compagnia.toString().contains('A54'))){
                                    datoC.tipoPolizza='HELVETIA RCI NOLEGGIO'
                                }else if((sinx.ramo.toString().contains('K/36')) && (sinistro.compagnia.toString().contains('A44') || sinistro.compagnia.toString().contains('A54'))){
                                    datoC.tipoPolizza='HELVETIA VIP RCI NOLEGGIO'
                                }
                                if(!datoC.save(flush:true)){
                                    logg =new Log(parametri: "errore caricamento dato contabile ${sinx.nosinistro} --> ${sinx.ramo.toString()} --> ${datoC.errors} ", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE SINISTRI")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }else{
                                    logg = new Log(parametri: "dato salvato correttamente", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE SINISTRI")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }
                            }else{
                                if(olddatoC.ramo =='' || olddatoC.ramo ==null){
                                    //println "entro qui ${olddatoC.noSinistro}"
                                    olddatoC.ramo=sinistro.ramo.toString().trim()
                                    if(!olddatoC.save(flush:true)){
                                        logg =new Log(parametri: "errore caricamento dato contabile ${sinx.nosinistro} --> ${olddatoC.errors}", operazione: "aggiornaDATICONT", pagina: "JOB ESTRAZIONE SINISTRI")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }else{
                                        logg = new Log(parametri: "dato salvato correttamente", operazione: "aggiornaDATICONT", pagina: "JOB ESTRAZIONE SINISTRI")
                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                }
                                logg = new Log(parametri: "il dato contabile e' gia' stato caricato", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE SINISTRI")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }
                        }
                        logg = new Log(parametri: "sinistro salvato correttamente", operazione: "generaSX", pagina: "JOB ESTRAZIONE SX")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }else{
                    if(olddatoSX.stato=="DEFINITO" && (olddatoSX.dataliquidazione !="" && olddatoSX.dataliquidazione !=null   && olddatoSX.dataliquidazione != "NULL" && olddatoSX.dataliquidazione != "null")){
                        println "entro qui --> ${olddatoSX.nosinistro}--> ${olddatoSX.dataliquidazione } --> ramo ${olddatoSX.ramo.toString()}"
                        def olddatoC=Daticontabili.findByNoSinistro(nosx.toString().trim())
                        if(!olddatoC){
                            logg = new Log(parametri: "entro qui per generarne un nuovo dato contabile--> ${olddatoSX.nosinistro} --> ramo ${olddatoSX.ramo.toString()}--> ${olddatoSX.dataliquidazione}", operazione: "genera DATO CONTABILE", pagina: "JOB ESTRAZIONE SX")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            /*sinistro.importofattura=sinistro.importofattura.toString().replace(".","")
                            sinistro.importofattura=sinistro.importofattura.toString().replace(",",".")*/
                            sinistro.importoliquidato=sinistro.liquidazione.toString().replace(".","")
                            sinistro.importoliquidato=sinistro.liquidazione.toString().replace(",",".")
                            def datoC=new Daticontabili()
                            datoC.noSinistro=olddatoSX.nosinistro
                            datoC.noFattura=olddatoSX.nosinistro
                            datoC.intestatarioPolizza=olddatoSX.nome
                            datoC.intestatarioConto=olddatoSX.riparatore
                            // datoC.importoFattura=(BigDecimal) decimalFormat.parse(sinistro.importofattura)
                            datoC.importoLiquidato= olddatoSX.liquidazione ? olddatoSX.liquidazione:0.0
                            //datoC.differenzaLiquidare=0.0
                            datoC.dataPagamento=olddatoSX.dataliquidazione?  olddatoSX.dataliquidazione:null
                            datoC.origine=OrigineDatocontabile.CARICATODAIASSICUR
                            datoC.ramo=olddatoSX.ramo.toString().trim()
                            if((olddatoSX.ramo.toString().contains('L/90') || olddatoSX.ramo.toString().contains('L/74') || olddatoSX.ramo.toString().contains('L/75')) && sinistro.compagnia.toString().contains('A32')){
                                datoC.tipoPolizza='AXA LLD'
                            }else if((olddatoSX.ramo.toString().contains('L/90') || olddatoSX.ramo.toString().contains('K/44') || olddatoSX.ramo.toString().contains('K/45') || olddatoSX.ramo.toString().contains('O/29')) && (sinistro.compagnia.toString().contains('A44') || sinistro.compagnia.toString().contains('A54'))){
                                datoC.tipoPolizza='HELVETIA RCI DEMO'
                            }else if((olddatoSX.ramo.toString().contains('O/94') || (olddatoSX.ramo.toString().contains('O/95'))) && (sinistro.compagnia.toString().contains('A54'))){
                                datoC.tipoPolizza='HELVETIA RCI NOLEGGIO'
                            }else if((olddatoSX.ramo.toString().contains('K/36')) && (olddatoSX.compagnia.toString().contains('A44') || sinistro.compagnia.toString().contains('A54'))){
                                datoC.tipoPolizza='HELVETIA VIP RCI NOLEGGIO'
                            }
                            if(!datoC.save(flush:true)){
                                logg =new Log(parametri: "errore caricamento dato contabile ${olddatoSX.nosinistro} -->${olddatoSX.ramo}-->  ${datoC.errors}", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }else{
                                logg = new Log(parametri: "dato salvato correttamente", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }
                        }else{
                            if(olddatoC.ramo =='' || olddatoC.ramo ==null){
                                //println "entro qui ${olddatoC.noSinistro}"
                                olddatoC.ramo=olddatoSX.ramo.toString().trim()
                                if(!olddatoC.save(flush:true)){
                                    logg =new Log(parametri: "errore caricamento dato contabile ${olddatoSX.nosinistro} -- -->${olddatoSX.ramo}--> ${olddatoC.errors}", operazione: "aggiornaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }else{
                                    logg = new Log(parametri: "dato aggiornato correttamente ${olddatoSX.nosinistro} -- -->${olddatoSX.ramo}", operazione: "aggiornaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }
                            }
                            logg = new Log(parametri: "il dato contabile e' gia' stato caricato", operazione: "generaDATICONT", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                    }
                    logg = new Log(parametri: "il sinistro e' gia' stato caricato", operazione: "generaSX", pagina: "JOB ESTRAZIONE DATI CONTABILI")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }

            }
        }
    }
}
