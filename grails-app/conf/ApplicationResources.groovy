modules = {
    jquery {
        defaultBundle 'core'
        resource url: '/js/jquery-1.12.4.js', disposition: 'head'
        resource url: '/js/jquery.browser.js', disposition: 'head'
        resource url: '/js/numeral/numeral.js', disposition: 'head'
        resource url: '/js/numeral/it.js', disposition: 'head'
        resource url: '/js/jquery.utils.js', disposition: 'head'
        resource url: '/js/utils.js', disposition: 'head'
        resource url: '/js/jquery.js', disposition: 'head'
        resource url: '/js/moment.min.js', disposition: 'head'
        resource url: '/js/jquery.form.js', disposition: 'head'
        resource url: '/js/bootstrap.js', disposition: 'head'
        resource url: '/js/sweetalert.min.js', disposition: 'head'
        resource url: '/js/ladda.jquery.min.js', disposition: 'head'
        resource url: '/js/ladda.min.js', disposition: 'head'
        resource url: '/js/spin.min.js', disposition: 'head'
        resource url: '/js/jquery.dataTables.js', disposition: 'head'
        resource url: '/js/dataTables.buttons.min.js', disposition: 'head'
        resource url: '/js/buttons.flash.min.js', disposition: 'head'
        resource url: '/js/jszip.min.js', disposition: 'head'
        resource url: '/js/dataTables.bootstrap.min.js', disposition: 'head'
        resource url: '/js/bootstrap-datetimepicker.js', disposition: 'head'
        resource url: '/js/jquery.jqGrid.min.js', disposition: 'head'
        resource url: '/js/grid.locale-it.js', disposition: 'head'
        resource url: '/js/tableHeadFixer.js', disposition: 'head'

    }

    'jquery-ui' {
        dependsOn 'jquery'
        defaultBundle 'core'
        resource url: '/css/jquery-ui/cupertino/jquery-ui.css', disposition: 'head'
        resource url: '/js/jquery-ui-1.10.3.min.js', disposition: 'head'
        resource url: '/js/jquery.ui.combobox.js', disposition: 'head'
        resource url: '/js/jquery.ui.datepicker-it.js', disposition: 'head'
        resource url: '/js/jquery-ui.config.js', disposition: 'head'
    }
}