class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{ constraints { } }
		"/"(controller: "sinistri", action: "elenco")
		"/login"(controller: 'utenti', action: 'login')
		"/logout"(controller: 'utenti', action: 'logout')
		"/grails"(controller: 'sinistri', action: 'grails')
		"500"(view:'/error')
	}
}
