package esmobility

class UrlFilters {

    def filters = {
        loginCheck(action: 'login|logout|downloadAllegato', invert: true) {
            before = {
                if(session.utente == null) {
                    redirect(controller: "utenti", action: "login")
                    return false
                }
            }
        }

        adminCheck(controller: 'utenti') {
            before = {
                if(actionName in ['login', 'logout'] || session.utente instanceof utenti.Admin) return true
                else {
                    redirect(controller: 'sinistri', action: 'elenco')
                    return false
                }
            }
        }
    }
}
