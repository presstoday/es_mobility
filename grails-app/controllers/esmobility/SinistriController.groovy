package esmobility
import grails.converters.JSON
import grails.util.Environment
import groovy.time.TimeCategory
import org.apache.commons.net.ftp.FTP
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPFile
import org.apache.poi.common.usermodel.Hyperlink
import org.apache.poi.ss.usermodel.CellStyle
import org.apache.poi.ss.usermodel.FillPatternType
import org.apache.poi.ss.usermodel.Font
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.IndexedColors
import org.apache.poi.ss.usermodel.VerticalAlignment
import org.apache.poi.xssf.usermodel.XSSFHyperlink
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.apache.tomcat.util.http.Cookies
import org.codehaus.groovy.grails.web.json.JSONObject
import sinistri.Allegati
import sinistri.Daticontabili
import sinistri.Estrazioni
import sinistri.Log
import sinistri.OrigineDatocontabile
import sinistri.Sinistridb
import sinistri.Sinistro
import sinistri.TipoEst
import excelbld.ExcelBuilder
import excel.reader.ExcelReader

import javax.activation.MimetypesFileTypeMap
import javax.servlet.http.Cookie
import javax.xml.ws.Response
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.regex.Pattern

class SinistriController {

    static defaultAction = "elenco"
    def IAssicurWebService
    def ExcelReportsService
    def mailService

    def elenco(def sinistriT) {
        render view: 'elenco', model: [sinistri: sinistriT.flatten()]

    }
    def menu_es() {
        render view: 'menu_es'

    }
    def estrazioni() {
        //render view: 'menu_es'

    }
    def estrazionisx() {
        //render view: 'menu_es'

    }
    def downloadFilesx(String sre, String nomeFile, String ricerca) {
        def result = IAssicurWebService.downloadFile(sre, nomeFile)
        if((result.xml.Segnalazioni.NumeroErrori.text() as int) > 0) {
            flash.error = "Errore iAssicur: " + (result.xml.Segnalazioni.Errore.Descrizione.text() ?: "Errore nel download del file $nomeFile")
            redirect uri: "/dettagliSinistro", params: [sre: sre, ricerca: ricerca]
        } else {
            def data = result.xml.FileDownload.Data
            def file = data.decodeBase64()
            response.setContentType("application/octet-stream")
            response.setHeader("Content-disposition", "filename=\"${nomeFile}\"")
            response.outputStream << file
        }
    }
    def ricerca(){
        //EstraDatiContJob.triggerNow()
       // EstraSxDBJob.triggerNow()
        //EstraNottJob.triggerNow()
        //EstraSettJob.triggerNow()
        //EstraTotJob.triggerNow()
        //EstraApertiJob.triggerNow()
        def logg
        def sinistriT=[]
        def sinistroA=[nome:null,targa:null,noSinistro:null,noPolizza:null,riparatore:null,dataSinistro:null,dataRicevimentoDenuncia:null,evento:null,descrizione:null,descrizione2:null,iter:null,dataIter:null,stato:null,preventivo:null,dataPreventivo:null,dataConcordato:null,concordato:null,pagato:null,dataPagamento:null,scoperto:null,franchigia:null,degrado:null,documentazionecompleta:null,docu1:null,docu2:null,docu3:null,docu4:null,docu5:null,docu6:null,docu7:null,docu8:null,docu9:null,note:null,statodre:null,scadenzadre:null,codFiscale:null]
        def isSx=false
        def resultwebS, sinistri, polizze
        def utente = session.utente
        if(request.post || params.parametro)
        {
            if(params.parametro.toString().trim()){
                def  targaRExp = /^[a-zA-Z]{2}\d{3}[a-zA-Z]{2}$/
                def  sxRExp = /^\d{3}\/\d{4}$/
                def  polRExp =/^[a-zA-Z]{2}\d{8}[a-zA-Z]{1}$/
                def pIvaRExp=/^[A-Za-z]{6}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}$/
                def codFisExp=/^[0-9]{11}$/

                if(params.parametro.toString().trim().matches(targaRExp) || params.parametro.toString().trim().matches(sxRExp) || params.parametro.toString().trim().matches(polRExp) || params.parametro.toString().trim().matches(pIvaRExp) || params.parametro.toString().trim().matches(codFisExp)){
                    if(utente.toString().contains("Esmobility-test")){
                        logg =new Log(parametri: "sono entrato come utente di test  ${utente.toString()}", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI come tester")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        List<String> sx = Arrays.asList("416/6212", "117/3162", "416/7284", "117/3039", "416/1911")
                        boolean containsSx = sx.contains(params.parametro.toString().trim())
                        List<String> targa = Arrays.asList("FD826BC", "FD608SJ", "FE198HV", "FC706GN", "FE439ME")
                        boolean containsTarga = targa.contains(params.parametro.toString().trim())
                        List<String> dre = Arrays.asList("EF31600215A", "EF31600554R", "EF51600498R", "EF31500606R", "EF51600686R")
                        boolean containsDre = dre.contains(params.parametro.toString().trim())
                        List<String> cfpiva = Arrays.asList("02064940352", "00854520384", "07969750962", "05790160963", "FRNFRN56B44G713J")
                        boolean containsCf = cfpiva.contains(params.parametro.toString().trim())
                        if(containsSx){
                            logg =new Log(parametri: "il parametro e' fra l'elenco dei sinistri da testare  ${containsSx} parametro-->${params.parametro.toString().trim()}", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI come tester")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            logg =new Log(parametri: "cerco per numero di sinistro  ${params.parametro.toString().trim()} sinistri", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI come tester")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            resultwebS = IAssicurWebService.getSRE(params.parametro.toString().trim(), true)
                            sinistri = resultwebS.sinistri
                            isSx=true
                            if(sinistri){
                                logg =new Log(parametri: "sono stati trovati ${sinistri.size()} sinistri", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI come tester")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                                String pattern = "#,##0.0#"
                                //symbols.setGroupingSeparator((char) ',')
                                symbols.setDecimalSeparator((char) ',')
                                def  decimalFormat = new DecimalFormat(pattern, symbols)
                                decimalFormat.setParseBigDecimal(true)
                                sinistri.each{ sinistro->
                                    sinistroA.nome=sinistro.nome
                                    sinistroA.targa=sinistro.targa
                                    sinistroA.noSinistro=sinistro.numerosinistro
                                    sinistroA.noPolizza=sinistro.polizza
                                    sinistroA.riparatore=sinistro.rubricaripara?:''
                                    def datasinistro = sinistro.datasinistro
                                    sinistroA.dataSinistro=datasinistro
                                    def dataricevDen = sinistro.dataricevimentodenuncia?:''
                                    sinistroA.dataRicevimentoDenuncia=dataricevDen
                                    sinistroA.evento=sinistro.evento
                                    sinistroA.descrizione=sinistro.descrizione
                                    if(sinistro.descrizione2.toString()!='' || sinistro.descrizione2.toString()!=null){
                                        sinistroA.descrizione2=sinistro.descrizione2.toString()
                                    }else{
                                        sinistroA.descrizione2=''

                                    }
                                    sinistroA.iter=sinistro.iter
                                    def dataIter = sinistro.dataIter?:''
                                    sinistroA.dataIter=dataIter
                                    sinistroA.stato=sinistro.stato
                                    sinistroA.preventivo=sinistro.preventivo?(BigDecimal) decimalFormat.parse(sinistro.preventivo) :0.0
                                    def datapreventivo = sinistro.datapreventivo?:''
                                    sinistroA.dataPreventivo=datapreventivo
                                    def dataconcordato = sinistro.dataconcordato?:''
                                    sinistroA.dataConcordato=dataconcordato
                                    sinistroA.concordato=sinistro.concordato?(BigDecimal) decimalFormat.parse(sinistro.concordato):0.0
                                    sinistroA.pagato=sinistro.pagato?(BigDecimal) decimalFormat.parse(sinistro.pagato):0.0
                                    def datapagamento = sinistro.datapagamento?:''
                                    sinistroA.dataPagamento=datapagamento
                                    sinistroA.scoperto=sinistro.scoperto? (BigDecimal) decimalFormat.parse(sinistro.scoperto):0.0
                                    sinistroA.franchigia=sinistro.franchigia? (BigDecimal) decimalFormat.parse(sinistro.franchigia):0.0
                                    sinistroA.degrado=sinistro.degrado
                                    sinistroA.documentazionecompleta=sinistro.docucompleta?.toString().trim()
                                    sinistroA.docu1=sinistro.docu1.toString()!=''?sinistro.docu1.toString():''
                                    sinistroA.docu2=sinistro.docu2.toString()!=''?sinistro.docu2.toString():''
                                    sinistroA.docu3=sinistro.docu3.toString()!=''?sinistro.docu3.toString():''
                                    sinistroA.docu4=sinistro.docu4.toString()!=''?sinistro.docu4.toString():''
                                    sinistroA.docu5=sinistro.docu5.toString()!=''?sinistro.docu5.toString():''
                                    sinistroA.docu6=sinistro.docu6.toString()!=''?sinistro.docu6.toString():''
                                    sinistroA.docu7=sinistro.docu7.toString()!=''?sinistro.docu7.toString():''
                                    sinistroA.docu8=sinistro.docu8.toString()!=''?sinistro.docu8.toString():''
                                    sinistroA.docu9=sinistro.docu9.toString()!=''?sinistro.docu9.toString():''
                                    sinistroA.note=sinistro.note.toString()!=''?sinistro.note.toString():''
                                    sinistroA.statodre=sinistro.statodre
                                    sinistroA.scadenzadre=sinistro.scadenzadre
                                    sinistroA.codFiscale=sinistro.codFiscale
                                    logg =new Log(parametri: "mi collego al server ftp per prender i file allegati", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI COME TESTER")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                                    //println sinistroA
                                    sinistriT<<sinistroA
                                    logg =new Log(parametri: "valori messi sull'array sinistro ${sinistroA} sinistri", operazione: "salvataggio dati", pagina: "CERCA SINISTRI COME TESTER")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    sinistroA=[nome:null,targa:null,noSinistro:null,noPolizza:null,riparatore:null,dataSinistro:null,dataRicevimentoDenuncia:null,evento:null,descrizione:null,descrizione2:null,iter:null,dataIter:null,stato:null,preventivo:null,dataPreventivo:null,dataConcordato:null,concordato:null,pagato:null,dataPagamento:null,scoperto:null,franchigia:null,degrado:null,documentazionecompleta:null,docu1:null,docu2:null,docu3:null,docu4:null,docu5:null,docu6:null,docu7:null,docu8:null,docu9:null,note:null,statodre:null,scadenzadre:null,codFiscale:null]
                                }

                            }else{
                                flash.error="il sinistro non e' stato trovato, controllare"
                            }
                        }else if(containsTarga || containsDre || containsCf){
                            logg =new Log(parametri: "il parametro e' fra l'elenco numero polizza-->${containsDre}, targa--> ${containsTarga} o codice fiscale-->${containsCf}  parametro-->${params.parametro.toString().trim()}", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI COME TESTER")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            logg =new Log(parametri: "cerco per targa,numero di polizza o cf/piva--> ${params.parametro.toString().trim()}", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI come tester")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            polizze = IAssicurWebService.getDRE(params.parametro.toString().trim()).polizze.codicepolizza
                            polizze=polizze.toString().replaceAll("\\[","").replaceAll("]","")
                            resultwebS=IAssicurWebService.getSRE(polizze,false)
                            sinistri = resultwebS.sinistri
                            isSx=false
                            if(sinistri){
                                logg =new Log(parametri: "sono stati trovati ${sinistri.size()} sinistri", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI come tester")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                                String pattern = "#,##0.0#"
                                //symbols.setGroupingSeparator((char) ',')
                                symbols.setDecimalSeparator((char) ',')
                                def  decimalFormat = new DecimalFormat(pattern, symbols)
                                decimalFormat.setParseBigDecimal(true)
                                sinistri.each{ sinistro->
                                    sinistroA.nome=sinistro.nome
                                    sinistroA.targa=sinistro.targa
                                    sinistroA.noSinistro=sinistro.numerosinistro
                                    sinistroA.noPolizza=sinistro.polizza
                                    sinistroA.riparatore=sinistro.rubricaripara?:''
                                    def datasinistro = sinistro.datasinistro
                                    sinistroA.dataSinistro=datasinistro
                                    def dataricevDen = sinistro.dataricevimentodenuncia?:''
                                    sinistroA.dataRicevimentoDenuncia=dataricevDen
                                    sinistroA.evento=sinistro.evento
                                    sinistroA.descrizione=sinistro.descrizione
                                    if(sinistro.descrizione2.toString()!='' || sinistro.descrizione2.toString()!=null){
                                        sinistroA.descrizione2=sinistro.descrizione2.toString()
                                    }else{
                                        sinistroA.descrizione2=''

                                    }
                                    sinistroA.iter=sinistro.iter
                                    def dataIter = sinistro.dataIter?:''
                                    sinistroA.dataIter=dataIter
                                    sinistroA.stato=sinistro.stato
                                    sinistroA.preventivo=sinistro.preventivo?(BigDecimal) decimalFormat.parse(sinistro.preventivo) :0.0
                                    def datapreventivo = sinistro.datapreventivo?:''
                                    sinistroA.dataPreventivo=datapreventivo
                                    def dataconcordato = sinistro.dataconcordato?:''
                                    sinistroA.dataConcordato=dataconcordato
                                    sinistroA.concordato=sinistro.concordato?(BigDecimal) decimalFormat.parse(sinistro.concordato):0.0
                                    sinistroA.pagato=sinistro.pagato?(BigDecimal) decimalFormat.parse(sinistro.pagato):0.0
                                    def datapagamento = sinistro.datapagamento?:''
                                    sinistroA.dataPagamento=datapagamento
                                    sinistroA.scoperto=sinistro.scoperto? (BigDecimal) decimalFormat.parse(sinistro.scoperto):0.0
                                    sinistroA.franchigia=sinistro.franchigia? (BigDecimal) decimalFormat.parse(sinistro.franchigia):0.0
                                    sinistroA.degrado=sinistro.degrado
                                    sinistroA.documentazionecompleta=sinistro.docucompleta?.toString().trim()
                                    sinistroA.docu1=sinistro.docu1.toString()!=''?sinistro.docu1.toString():''
                                    sinistroA.docu2=sinistro.docu2.toString()!=''?sinistro.docu2.toString():''
                                    sinistroA.docu3=sinistro.docu3.toString()!=''?sinistro.docu3.toString():''
                                    sinistroA.docu4=sinistro.docu4.toString()!=''?sinistro.docu4.toString():''
                                    sinistroA.docu5=sinistro.docu5.toString()!=''?sinistro.docu5.toString():''
                                    sinistroA.docu6=sinistro.docu6.toString()!=''?sinistro.docu6.toString():''
                                    sinistroA.docu7=sinistro.docu7.toString()!=''?sinistro.docu7.toString():''
                                    sinistroA.docu8=sinistro.docu8.toString()!=''?sinistro.docu8.toString():''
                                    sinistroA.docu9=sinistro.docu9.toString()!=''?sinistro.docu9.toString():''
                                    sinistroA.note=sinistro.note.toString()!=''?sinistro.note.toString():''
                                    sinistroA.statodre=sinistro.statodre
                                    sinistroA.scadenzadre=sinistro.scadenzadre
                                    sinistroA.codFiscale=sinistro.codFiscale
                                    logg =new Log(parametri: "mi collego al server ftp per prender i file allegati", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI COME TESTER")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                                    //println sinistroA
                                    sinistriT<<sinistroA
                                    logg =new Log(parametri: "valori messi sull'array sinistro ${sinistroA} sinistri", operazione: "salvataggio dati", pagina: "CERCA SINISTRI COME TESTER")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    sinistroA=[nome:null,targa:null,noSinistro:null,noPolizza:null,riparatore:null,dataSinistro:null,dataRicevimentoDenuncia:null,evento:null,descrizione:null,descrizione2:null,iter:null,dataIter:null,stato:null,preventivo:null,dataPreventivo:null,dataConcordato:null,concordato:null,pagato:null,dataPagamento:null,scoperto:null,franchigia:null,degrado:null,documentazionecompleta:null,docu1:null,docu2:null,docu3:null,docu4:null,docu5:null,docu6:null,docu7:null,docu8:null,docu9:null,note:null,statodre:null,scadenzadre:null,codFiscale:null]
                                }

                            }else{
                                flash.error="il sinistro non e' stato trovato, controllare"
                            }
                        }else {
                            flash.error="il parametro inserito non e' fra l'elenco dei parametri a testare, controllare"
                        }
                    }else{
                        if(params.parametro.toString().trim().matches(sxRExp)){
                            logg =new Log(parametri: "cerco per numero di sinistro  ${params.parametro.toString().trim()} sinistri", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            resultwebS = IAssicurWebService.getSRE(params.parametro.toString().trim(), true)
                            sinistri = resultwebS.sinistri
                            isSx=true
                        }else{
                            logg =new Log(parametri: "cerco per targa,numero di polizza o cf/piva--> ${params.parametro.toString().trim()}", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            polizze = IAssicurWebService.getDRE(params.parametro.toString().trim()).polizze.codicepolizza
                            polizze=polizze.toString().replaceAll("\\[","").replaceAll("]","")
                            resultwebS=IAssicurWebService.getSRE(polizze,false)
                            sinistri = resultwebS.sinistri
                            isSx=false
                        }
                        if(sinistri){
                            logg =new Log(parametri: "sono stati trovati ${sinistri.size()} sinistri", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                            String pattern = "#,##0.0#"
                            //symbols.setGroupingSeparator((char) ',')
                            symbols.setDecimalSeparator((char) ',')
                            def  decimalFormat = new DecimalFormat(pattern, symbols)
                            decimalFormat.setParseBigDecimal(true)
                            sinistri.each{ sinistro->
                                sinistroA.nome=sinistro.nome
                                sinistroA.targa=sinistro.targa
                                sinistroA.noSinistro=sinistro.numerosinistro
                                sinistroA.noPolizza=sinistro.polizza
                                sinistroA.riparatore=sinistro.rubricaripara?:''
                                def datasinistro = sinistro.datasinistro
                                sinistroA.dataSinistro=datasinistro
                                def dataricevDen = sinistro.dataricevimentodenuncia?:''
                                sinistroA.dataRicevimentoDenuncia=dataricevDen
                                sinistroA.evento=sinistro.evento
                                sinistroA.descrizione=sinistro.descrizione
                                if(sinistro.descrizione2.toString()!='' || sinistro.descrizione2.toString()!=null){
                                    sinistroA.descrizione2=sinistro.descrizione2.toString()
                                }else{
                                    sinistroA.descrizione2=''

                                }
                                sinistroA.iter=sinistro.iter
                                def dataIter = sinistro.dataIter?:''
                                sinistroA.dataIter=dataIter
                                sinistroA.stato=sinistro.stato
                                sinistroA.preventivo=sinistro.preventivo?(BigDecimal) decimalFormat.parse(sinistro.preventivo) :0.0
                                def datapreventivo = sinistro.datapreventivo?:''
                                sinistroA.dataPreventivo=datapreventivo
                                def dataconcordato = sinistro.dataconcordato?:''
                                sinistroA.dataConcordato=dataconcordato
                                sinistroA.concordato=sinistro.concordato?(BigDecimal) decimalFormat.parse(sinistro.concordato):0.0
                                sinistroA.pagato=sinistro.pagato?(BigDecimal) decimalFormat.parse(sinistro.pagato):0.0
                                def datapagamento = sinistro.datapagamento?:''
                                sinistroA.dataPagamento=datapagamento
                                sinistroA.scoperto=sinistro.scoperto? (BigDecimal) decimalFormat.parse(sinistro.scoperto):0.0
                                sinistroA.franchigia=sinistro.franchigia? (BigDecimal) decimalFormat.parse(sinistro.franchigia):0.0
                                sinistroA.degrado=sinistro.degrado
                                /*sinistroA.documentazionecompleta=sinistro.docucompleta?.toString().trim()
                                sinistroA.docu1=sinistro.docu1.toString()!=''?sinistro.docu1.toString():''
                                sinistroA.docu2=sinistro.docu2.toString()!=''?sinistro.docu2.toString():''
                                sinistroA.docu3=sinistro.docu3.toString()!=''?sinistro.docu3.toString():''
                                sinistroA.docu4=sinistro.docu4.toString()!=''?sinistro.docu4.toString():''
                                sinistroA.docu5=sinistro.docu5.toString()!=''?sinistro.docu5.toString():''
                                sinistroA.docu6=sinistro.docu6.toString()!=''?sinistro.docu6.toString():''
                                sinistroA.docu7=sinistro.docu7.toString()!=''?sinistro.docu7.toString():''
                                sinistroA.docu8=sinistro.docu8.toString()!=''?sinistro.docu8.toString():''
                                sinistroA.docu9=sinistro.docu9.toString()!=''?sinistro.docu9.toString():''*/
                                sinistroA.note=sinistro.note.toString()!=''?sinistro.note.toString():''
                                sinistroA.statodre=sinistro.statodre
                                sinistroA.scadenzadre=sinistro.scadenzadre
                                sinistroA.codFiscale=sinistro.codFiscale
                                logg =new Log(parametri: "mi collego al server ftp per prender i file allegati", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                                //println sinistroA
                                sinistriT<<sinistroA
                                logg =new Log(parametri: "valori messi sull'array sinistro ${sinistroA} sinistri", operazione: "salvataggio dati", pagina: "CERCA SINISTRI")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                sinistroA=[nome:null,targa:null,noSinistro:null,noPolizza:null,riparatore:null,dataSinistro:null,dataRicevimentoDenuncia:null,evento:null,descrizione:null,descrizione2:null,iter:null,dataIter:null,stato:null,preventivo:null,dataPreventivo:null,dataConcordato:null,concordato:null,pagato:null,dataPagamento:null,scoperto:null,franchigia:null,degrado:null,documentazionecompleta:null,docu1:null,docu2:null,docu3:null,docu4:null,docu5:null,docu6:null,docu7:null,docu8:null,docu9:null,note:null,statodre:null,scadenzadre:null,codFiscale:null]
                            }

                        }else{
                            flash.error="il sinistro non e' stato trovato, controllare"
                        }
                    }



                }else{
                    flash.error="il parametro inserito non corrisponde con qualcuno dei parametri permessi, controllare"
                }
            }else{

                flash.error="inserire un parametro"
            }
            logg =new Log(parametri: "questi sono i sinistri trovati ${sinistriT.flatten()}", operazione: "queryIASSICURSinistri", pagina: "CERCA SINISTRI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            render view: 'elenco', model: [sinistri: sinistriT.flatten(), isSx: isSx, parametro:params.parametro.toString().trim()]
        }else{
        }

    }
    def dettaglio(){
        //println "params arrivati in action detaglio:>>>${params}"
        def logg
        logg =new Log(parametri: "params arrivati in action detaglio:>>>${params}", operazione: "dettaglioSinistro", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def sinistriT=[]
        def sinistro=[nome:null,targa:null,noSinistro:null,noPolizza:null,riparatore:null,dataSinistro:null,dataRicevimentoDenuncia:null,evento:null,descrizione:null,descrizione2:null,iter:null,dataIter:null,stato:null,preventivo:null,dataPreventivo:null,dataConcordato:null,concordato:null,pagato:null,dataPagamento:null,scoperto:null,franchigia:null,degrado:null,documentazionecompleta:null,docu1:null,docu2:null,docu3:null,docu4:null,docu5:null,docu6:null,docu7:null,docu8:null,docu9:null,note:null,statodre:null,scadenzadre:null,codFiscale:null,filenames:[:]]
        def sinistroA=params.sinistro
        def filenames = []
        logg =new Log(parametri: "questi sono i dati del sinistro scelto ${sinistroA}", operazione: "dettaglioSinistro", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        //println  "sin a ${sinistroA}"
        sinistroA=params.sinistro.toString().replaceAll("\\{","").replaceAll("}","").replaceAll("\\[","").replaceAll("]","")
        def map = sinistroA.split(', ').collectEntries { entry ->
                    def pair = entry.split('=')
                    [(pair.first()): pair.last()]
                }
        def noSinistro=map.noSinistro
        def sx=map.noSinistro
        noSinistro=noSinistro.toString().replaceAll("/","-")

        def commenti=Sinistro.withCriteria {
            eq ("noSinistro", "${map.noSinistro}")
            order ("dataInserimento", "asc")
        }
        //println "questi sono i commenti ${commenti}"
        logg =new Log(parametri: "questi sono i commenti inseriti del sinistro scelto ${commenti}", operazione: "dettaglioSinistro", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

        def allegati=IAssicurWebService.getFile(map.noSinistro).file
        //println "allegati ${allegati}"
        if( allegati.file.size()>0){
            //println "sono qui"
            allegati.collect { commento ->
                //println "commento ${commento.data}"
                def dataparse=Date.parse('yyyyMMddhhmmss', commento.data)
                //println "datapare ${dataparse}"
                def nome = commento.nome.toString()
                def pos = nome.lastIndexOf('.')
                def ext = false
                def icon = 'fa-file-text-o'
                if( pos != -1 )
                    ext = nome.substring(pos+1).toLowerCase()
                switch(ext) {
                    case 'zip':
                        icon = 'fa-file-archive-o'
                        break
                    case 'pdf':
                        icon = 'fa-file-pdf-o'
                        break
                    case 'jpeg':
                    case 'jpg':
                        icon = 'fa-camera-retro'
                        break
                }
                if(commento.nome.toString().length()>9){
                    filenames<<[
                            nome:commento.nome.toString(),
                            url:commento.nome.toString(),
                            data:dataparse.format("dd-MMM-yyyy"),
                            dataordine:dataparse,
                            icon:icon
                    ]
                }
            }.grep().join("\n")

        }


        //println "file names ${filenames}"
        filenames.sort { it.dataordine }

        logg =new Log(parametri: "questi sono i dati dei file trovati sinistro scelto ${filenames}  e la dimensione è ${filenames.size()}", operazione: "dettaglioSinistro", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

        def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
        String pattern = "#,##0.0#"
        symbols.setDecimalSeparator((char) ',')
        def  decimalFormat = new DecimalFormat(pattern, symbols)
        decimalFormat.setParseBigDecimal(true)

        sinistro.nome=map.nome
        sinistro.targa=map.targa
        sinistro.noSinistro=map.noSinistro
        sinistro.noPolizza=map.noPolizza
        sinistro.riparatore=map.riparatore!="riparatore"?map.riparatore:''
        sinistro.dataSinistro=map.dataSinistro
        sinistro.dataRicevimentoDenuncia=map.dataRicevimentoDenuncia
        sinistro.evento=map.evento
        sinistro.descrizione=map.descrizione
        sinistro.descrizione2=map.descrizione2!="descrizione2"?map.descrizione2:''
        sinistro.iter=map.iter
        sinistro.dataIter=map.dataIter
        sinistro.stato=map.stato
        sinistro.preventivo=map.preventivo?:0.0
        sinistro.dataPreventivo=map.dataPreventivo!="dataPreventivo"?map.dataPreventivo:''
        sinistro.dataConcordato=map.dataConcordato!="dataConcordato"?map.dataConcordato:''
        sinistro.concordato=map.concordato?:0.0
        sinistro.pagato=map.pagato?:0.0
        sinistro.dataPagamento=map.dataPagamento!="dataPagamento"?map.dataPagamento:''
        sinistro.scoperto=map.scoperto?:0.0
        sinistro.franchigia=map.franchigia?:0.0
        sinistro.degrado=map.degrado
        /*sinistro.documentazionecompleta=map.documentazionecompleta?map.documentazionecompleta.toString().trim():''
        sinistro.docu1=map.docu1!="docu1"?map.docu1:''
        sinistro.docu2=map.docu2!="docu2"?map.docu2:''
        sinistro.docu3=map.docu3!="docu3"?map.docu3:''
        sinistro.docu4=map.docu4!="docu4"?map.docu4:''
        sinistro.docu5=map.docu5!="docu5"?map.docu5:''
        sinistro.docu6=map.docu6!="docu6"?map.docu6:''
        sinistro.docu7=map.docu7!="docu7"?map.docu7:''
        sinistro.docu8=map.docu8!="docu8"?map.docu8:''
        sinistro.docu9=map.docu9!="docu9"?map.docu9:''*/
        sinistro.note=map.note
        sinistro.statodre=map.statodre
        sinistro.scadenzadre=map.scadenzadre
        sinistro.codFiscale=map.codFiscale
        if(filenames.size()>1){
            sinistro.filenames=filenames
        }
        //println " le note--> ${map.note}"
        sinistriT<<sinistro
        //commenti=commenti? commenti as JSON:""
        logg =new Log(parametri: "sinistro inviato a video ${sinistriT}", operazione: "dettaglioSinistro", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

        if(params.risposta){
            render view: 'dettaglio', model: [sinistri: sinistriT.flatten(), commenti:commenti, sinistroA:sinistroA, parametro:params.parametro.toString().trim(), risposta: params.risposta]

        }else{
            render view: 'dettaglio', model: [sinistri: sinistriT.flatten(), commenti:commenti, sinistroA:sinistroA,parametro:params.parametro.toString().trim()]

        }
    }
    def downloadFile(){
        def logg
        def filename
        filename = params.filename
        logg =new Log(parametri: "file da scaricare  ${params.filename}", operazione: "download file", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        //println filename
        filename=filename.toString().replaceAll("\\[","").replaceAll("]","")
        def localfile
        def outputStream
        def bytesOut = new ByteArrayOutputStream()
        logg =new Log(parametri: "mi collego al server ftp", operazione: "download file", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def ftpClient = new FTPClient()
        try {
            ftpClient.connect "194.209.163.192"
            ftpClient.enterLocalPassiveMode()
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE) ///FTP.ASCII_FILE_TYPE)
            ftpClient.login ("IADMIN-EMANSUTTI", "!3m4n5u1")
            def remoteFilePath = "Mach1_produzione/FILE/sre/${filename}"
            //outputStream = new BufferedOutputStream(new FileOutputStream(localfile))
            outputStream = new BufferedOutputStream(bytesOut)
            boolean success = ftpClient.retrieveFile(remoteFilePath, outputStream)
            outputStream.close()
            if (success) {
                logg =new Log(parametri: "file scaricato", operazione: "download file", pagina: "DETTAGLIO SINISTRO")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Ftp file successfully download."
            }
        }catch (IOException ex) {
            logg =new Log(parametri: "errore download file ${ex.getMessage()}", operazione: "download file", pagina: "DETTAGLIO SINISTRO")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "Error occurs in downloading files from ftp Server : " + ex.getMessage()
        }
        finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout()
                    ftpClient.disconnect()
                }
            } catch (IOException ex) {ex.printStackTrace()}
        }
        response.setContentType("application/octet-stream")
        /*
        if(filename.toString().toLowerCase().contains(".zip")){
            response.setContentType("application/octet-stream")

        }else if(filename.toString().toLowerCase().contains(".pdf")){
            response.setContentType("application/pdf")
        }else{
            response.setContentType("application/octet-stream")
        }*/
        response.setHeader("Content-disposition", "attachment; filename=${filename}")
        response.outputStream << bytesOut.toByteArray()
}
    def inviaMail(){
        def logg
        def utente = session.utente
        //println "questo è il utente ${utente.id} e i params ${params}"
        logg =new Log(parametri: "questo è il utente ${utente.id} e i parametri che arrivano ${params}", operazione: "inviaMail/commento", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def stringaresponse=""
        def risposta=false
        if(params.isMail && (params.messageM )&& params.noSinistro){
            def isMail=Boolean.valueOf(params.isMail)
            def ncommSin=new Sinistro()
            def destinatario="priscila@presstoday.com"
            if(Environment.current != Environment.DEVELOPMENT) {
                destinatario="lld@mach-1.it"
            }
            if(isMail){
                try {
                    def messaggio=params.messageM
                    def noSinistro=params.noSinistro.toString().replaceAll("\\[","").replaceAll("]","")
                    ncommSin.messaggio=messaggio
                    ncommSin.dataInserimento=new Date()
                    ncommSin.noSinistro=noSinistro
                    if(!ncommSin.save(flush: true)){
                        logg =new Log(parametri: "errore salvattaggio del sinistro inviato ${ncommSin.errors}", operazione: "inviaMail", pagina: "DETTAGLIO SINISTRO")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }else{
                        ncommSin.addToUtente(utente)
                        logg =new Log(parametri: "messaggio del sinistro inviato ${ncommSin.id}", operazione: "inviaMailo", pagina: "DETTAGLIO SINISTRO")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                    def cheMail=mailService.sendMail {
                        to "${destinatario}"
                        subject "SOLLECITO SINISTRO ${noSinistro}"
                        text "${messaggio}"
                    }
                    risposta=cheMail.asBoolean()
                    logg =new Log(parametri: "risposta invio della mail inviata  ${cheMail.asBoolean()}", operazione: "inviaMail", pagina: "DETTAGLIO SINISTRO")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                } catch (e) {

                }
            }else{
                def messaggio=params.messageM
                ncommSin.messaggio=messaggio
                ncommSin.noSinistro=params.noSinistro.toString().replaceAll("\\[","").replaceAll("]","")
                ncommSin.dataInserimento=new Date()
                if(!ncommSin.save(flush: true)){
                    logg =new Log(parametri: "errore salvattaggio del sinistro inviato solo como commento ${ncommSin.errors}", operazione: "commento", pagina: "DETTAGLIO SINISTRO")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{
                    ncommSin.addToUtente(utente)
                    logg =new Log(parametri: "messaggio del sinistro inviato, id --> ${ncommSin.id}", operazione: "commento", pagina: "DETTAGLIO SINISTRO")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    risposta=true
                }

            }
        }
        stringaresponse=[risposta:risposta]
        render stringaresponse as JSON
    }
    def downloadpiuSett(){
        def dataOdierna=new Date().format("dd.MM.yyyy")
        def settimana=use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -7.day}
        def fileEstra=Estrazioni.withCriteria {
            ge 'settimanaEstra', settimana.clearTime()
            eq 'tipo', TipoEst.ELENCO_SETTIMANALE
            order 'dateCreated', "desc"
        }
        try {
            if(fileEstra){
                def excel = fileEstra.first().fileContent
                response.setContentType("application/vnd.ms-excel")
                response.setHeader("Content-disposition", "attachment; filename=Estrazione settimanale ${settimana}.xls")
                response.outputStream << excel
            }
            else{

                redirect action: "estrazionisx"
            }
        }catch (e){
            response.sendError(404)
        }


    }
    def downloadTot(){
        def dataOdierna=new Date().clearTime()
        def fileEstra=Estrazioni.withCriteria {
            ge 'dateCreated', dataOdierna
            eq 'tipo', TipoEst.ELENCO_TOTALE
            order 'dateCreated', "desc"
        }
        try {
            if(fileEstra){
                def excel = fileEstra.first().fileContent
                response.setContentType("application/vnd.ms-excel")
                response.setHeader("Content-disposition", "attachment; filename=Estrazione totale_${dataOdierna.format("dd_MM_yyyy")}.xls")
                response.outputStream << excel
            }
            else{
                redirect action: "estrazionisx"
            }
        }catch (e){
            response.sendError(404)
        }
       /* def excel = ExcelReportsService.estrazioneTot()
        response.setContentType("application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment; filename=Estrazione totale.xls")
        response.outputStream << excel*/
    }
    def downloadContabileTot(){
        def fileEstra=Daticontabili.withCriteria {
            isNull ('dataPagamentoIntegrato')
            order 'dataPagamento', "asc"
        }
        try {
            if(fileEstra){
                def wb =new XSSFWorkbook()
                def createHelper = wb.getCreationHelper()
                CellStyle hlink_style = wb.createCellStyle()
                Font hlink_font = wb.createFont()
                hlink_font.setUnderline(Font.U_SINGLE)
                hlink_font.setColor(IndexedColors.BLUE.getIndex())
                hlink_style.setFont(hlink_font)
                def link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                def my_sheet = wb.createSheet("Dati contabili")
                def  style = wb.createCellStyle()
                style.setAlignment(HorizontalAlignment.CENTER)
                style.setVerticalAlignment(VerticalAlignment.CENTER)
                style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex())
                style.setFillPattern(FillPatternType.SOLID_FOREGROUND)
                def row = my_sheet.createRow(0)
                def cell = row.createCell(0)
                cell.setCellValue("Numero Sinistro")
                cell.setCellStyle(style)
                cell = row.createCell(1)
                cell.setCellValue("Numero Fattura")
                cell.setCellStyle(style)
                cell = row.createCell(2)
                cell.setCellValue("Intestatario fattura")
                cell.setCellStyle(style)
                cell = row.createCell(3)
                cell.setCellValue("Intestatario polizza")
                cell.setCellStyle(style)
                cell = row.createCell(4)
                cell.setCellValue("Intestatario conto")
                cell.setCellStyle(style)
                cell = row.createCell(5)
                cell.setCellValue("Data pagamento")
                cell.setCellStyle(style)
                cell = row.createCell(6)
                cell.setCellValue("Importo fattura")
                cell.setCellStyle(style)
                cell = row.createCell(7)
                cell.setCellValue("Importo liquidato")
                cell.setCellStyle(style)
                cell = row.createCell(8)
                cell.setCellValue("Differenza da integrare")
                cell.setCellStyle(style)
                cell = row.createCell(9)
                cell.setCellValue("Data pagamento integrato")
                cell.setCellStyle(style)
                cell = row.createCell(10)
                cell.setCellValue("Fattura allegata")
                cell.setCellStyle(style)
                if(fileEstra){
                    fileEstra.eachWithIndex { datoconta, idx ->
                        if(datoconta.allegati && datoconta.allegati.size()>1){
                                    row = my_sheet.createRow(idx+1)
                                    cell = row.createCell(0)
                                    cell.setCellValue(datoconta.noSinistro)
                                    cell = row.createCell(1)
                                    cell.setCellValue(datoconta.noFattura)
                                    cell = row.createCell(2)
                                    cell.setCellValue(datoconta.intestatarioFattura? datoconta.intestatarioFattura.toString().toUpperCase():"")
                                    cell = row.createCell(3)
                                    cell.setCellValue(datoconta.intestatarioPolizza? datoconta.intestatarioPolizza.toString().toUpperCase():"")
                                    cell = row.createCell(4)
                                    cell.setCellValue(datoconta.intestatarioConto? datoconta.intestatarioConto.toString().toUpperCase():"")
                                    cell = row.createCell(5)
                                    cell.setCellValue(datoconta.dataPagamento?.format("dd/MM/yyyy"))
                                    cell = row.createCell(6)
                                    cell.setCellValue(datoconta.importoFattura)
                                    cell = row.createCell(7)
                                    cell.setCellValue(datoconta.importoLiquidato)
                                    cell = row.createCell(8)
                                    cell.setCellValue(datoconta.differenzaLiquidare)
                                    cell = row.createCell(9)
                                    cell.setCellValue(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                    def celnum=10
                                    datoconta.allegati.sort().eachWithIndex { allegato, count ->
                                        cell = row.createCell(celnum+count)
                                        if ( Environment.current != Environment.PRODUCTION) {
                                            link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}")
                                            cell.setCellValue("${allegato.fileName}")
                                            cell.setHyperlink(link)
                                            cell.setCellStyle(hlink_style)
                                        }else{
                                            link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                            cell.setCellValue("${allegato.fileName}")
                                            cell.setHyperlink(link)
                                            cell.setCellStyle(hlink_style)
                                        }
                                    }

                            datoconta.discard()
                        }else if(datoconta.allegati && datoconta.allegati.size()==1){
                            def allegato=datoconta.allegati.first()
                            row = my_sheet.createRow(idx+1)
                            cell = row.createCell(0)
                            cell.setCellValue(datoconta.noSinistro)
                            cell = row.createCell(1)
                            cell.setCellValue(datoconta.noFattura)
                            cell = row.createCell(2)
                            cell.setCellValue(datoconta.intestatarioFattura? datoconta.intestatarioFattura.toString().toUpperCase():"")
                            cell = row.createCell(3)
                            cell.setCellValue(datoconta.intestatarioPolizza? datoconta.intestatarioPolizza.toString().toUpperCase():"")
                            cell = row.createCell(4)
                            cell.setCellValue(datoconta.intestatarioConto? datoconta.intestatarioConto.toString().toUpperCase():"")
                            cell = row.createCell(5)
                            cell.setCellValue(datoconta.dataPagamento?.format("dd/MM/yyyy"))
                            cell = row.createCell(6)
                            cell.setCellValue(datoconta.importoFattura)
                            cell = row.createCell(7)
                            cell.setCellValue(datoconta.importoLiquidato)
                            cell = row.createCell(8)
                            cell.setCellValue(datoconta.differenzaLiquidare)
                            cell = row.createCell(9)
                            cell.setCellValue(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                            cell = row.createCell(10)
                            if ( Environment.current != Environment.PRODUCTION) {
                                link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                                link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}")
                                cell.setCellValue("${allegato.fileName}")
                                cell.setHyperlink(link)
                                cell.setCellStyle(hlink_style)
                            }else{
                                link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                                link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                cell.setCellValue("${allegato.fileName}")
                                cell.setHyperlink(link)
                                cell.setCellStyle(hlink_style)
                            }
                            datoconta.discard()
                        }
                        else{
                            row = my_sheet.createRow(idx+1)
                            cell = row.createCell(0)
                            cell.setCellValue(datoconta.noSinistro)
                            cell = row.createCell(1)
                            cell.setCellValue(datoconta.noFattura)
                            cell = row.createCell(2)
                            cell.setCellValue(datoconta.intestatarioFattura? datoconta.intestatarioFattura.toString().toUpperCase():"")
                            cell = row.createCell(3)
                            cell.setCellValue(datoconta.intestatarioPolizza? datoconta.intestatarioPolizza.toString().toUpperCase():"")
                            cell = row.createCell(4)
                            cell.setCellValue(datoconta.intestatarioConto? datoconta.intestatarioConto.toString().toUpperCase():"")
                            cell = row.createCell(5)
                            cell.setCellValue(datoconta.dataPagamento?.format("dd/MM/yyyy"))
                            cell = row.createCell(6)
                            cell.setCellValue(datoconta.importoFattura)
                            cell = row.createCell(7)
                            cell.setCellValue(datoconta.importoLiquidato)
                            cell = row.createCell(8)
                            cell.setCellValue(datoconta.differenzaLiquidare)
                            cell = row.createCell(9)
                            cell.setCellValue(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                            cell = row.createCell(10)
                            cell.setCellValue("non ci sono allegati")
                            datoconta.discard()
                        }
                    }

                }else{
                    row = my_sheet.createRow(1)
                    cell = row.createCell(0)
                    cell.setCellValue("non ci sono dati contabili")
                }
                /*wb = ExcelBuilder.createXls {
                    sheet("Dati contabili") {
                        row(style: "header") {
                            cell("Numero Sinistro")
                            cell("Numero Fattura")
                            cell("Intestatario fattura")
                            cell("Intestatario polizza")
                            cell("Intestatario conto")
                            cell("Data pagamento")
                            cell("Importo fattura")
                            cell("Importo liquidato")
                            cell("Differenza da integrare")
                            cell("Data pagamento integrato")
                            cell("Fattura allegata")
                        }
                        if(fileEstra){
                            fileEstra.each { datoconta ->
                                if(datoconta.allegati && datoconta.allegati.size()>1){
                                    datoconta.allegati.sort().eachWithIndex {allegato, count ->
                                        if(count==0){
                                            row {
                                                cell(datoconta.noSinistro)
                                                cell(datoconta.noFattura)
                                                cell(datoconta.intestatarioFattura.toString().toUpperCase())
                                                cell(datoconta.intestatarioPolizza.toString().toUpperCase())
                                                cell(datoconta.intestatarioConto.toString().toUpperCase())
                                                cell(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                                cell(datoconta.importoFattura)
                                                cell(datoconta.importoLiquidato)
                                                cell(datoconta.differenzaLiquidare)
                                                cell(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                                if ( Environment.current != Environment.PRODUCTION) {

                                                    link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                    cell.setHyperlink(link)
                                                    cell.setCellStyle(hlink_style)
                                                    //cell(link)
                                                }else{
                                                    link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                    cell.setHyperlink(link)
                                                    cell.setCellStyle(hlink_style)
                                                    //cell("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                }
                                            }
                                        }else{
                                            row {
                                                cell("")
                                                cell("ulteriore")
                                                cell("allegato")
                                                cell("del sinistro")
                                                cell("${datoconta.noSinistro}")
                                                cell("")
                                                cell("")
                                                cell("")
                                                cell("")
                                                cell("")
                                                if ( Environment.current != Environment.PRODUCTION) {
                                                    link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                    cell.setHyperlink(link)
                                                    cell.setCellStyle(hlink_style)
                                                }else{

                                                    link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                    cell.setHyperlink(link)
                                                    cell.setCellStyle(hlink_style)
                                                    //cell("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                }
                                            }
                                        }

                                    }
                                    datoconta.discard()
                                }else if(datoconta.allegati && datoconta.allegati.size()==1){
                                    def allegato=datoconta.allegati.first().id.toString()
                                    row {
                                        cell(datoconta.noSinistro)
                                        cell(datoconta.noFattura)
                                        cell(datoconta.intestatarioFattura.toString().toUpperCase())
                                        cell(datoconta.intestatarioPolizza.toString().toUpperCase())
                                        cell(datoconta.intestatarioConto.toString().toUpperCase())
                                        cell(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                        cell(datoconta.importoFattura)
                                        cell(datoconta.importoLiquidato)
                                        cell(datoconta.differenzaLiquidare)
                                        cell(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                        if ( Environment.current != Environment.PRODUCTION) {
                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.toString()}")
                                            cell.setHyperlink(link)
                                            cell.setCellStyle(hlink_style)
                                           // cell("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.toString()}")
                                        }else{
                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.toString()}")
                                            cell.setHyperlink(link)
                                            cell.setCellStyle(hlink_style)
                                           //cell("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.toString()}")
                                        }
                                    }
                                    datoconta.discard()
                                }
                                else{
                                    row {
                                        cell(datoconta.noSinistro)
                                        cell(datoconta.noFattura)
                                        cell(datoconta.intestatarioFattura.toString().toUpperCase())
                                        cell(datoconta.intestatarioPolizza.toString().toUpperCase())
                                        cell(datoconta.intestatarioConto.toString().toUpperCase())
                                        cell(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                        cell(datoconta.importoFattura)
                                        cell(datoconta.importoLiquidato)
                                        cell(datoconta.differenzaLiquidare)
                                        cell(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                        cell("non ci sono allegati per questo sinistro")
                                    }
                                    datoconta.discard()
                                }

                            }
                            for (int i = 0; i < 17; i++) {
                                sheet.autoSizeColumn(i)
                            }
                        }else{
                            row{
                                cell("non ci sono dati contabili")
                            }
                        }

                    }

                }*/
                for (int i = 0; i < 11; i++) {
                    my_sheet.setColumnWidth(i, 30*256)
                }
                def stream = new ByteArrayOutputStream()
                wb.write(stream)
                stream.close()
                response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                response.addHeader "Content-disposition", "inline; filename=daticontabili_${new Date().format("yyyyMMdd")}.xlsx"
                response.outputStream << stream.toByteArray()
            }
            else{
                redirect action: "estrazioni"
            }
        }catch (e){
            response.sendError(404)
        }
       /* def excel = ExcelReportsService.estrazioneTot()
        response.setContentType("application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment; filename=Estrazione totale.xls")
        response.outputStream << excel*/
    }
    def downloadAperti(){
        def dataOdierna=new Date().clearTime()
        def fileEstra=Estrazioni.withCriteria {
            ge 'dateCreated', dataOdierna
            eq 'tipo', TipoEst.ELENCO_APERTI
            order 'dateCreated', "desc"
        }
        try {
            if(fileEstra){
                def excel = fileEstra.first().fileContent
                response.setContentType("application/vnd.ms-excel")
                response.setHeader("Content-disposition", "attachment; filename=Estrazione totale_${dataOdierna.format("dd_MM_yyyy")}_aperti.xls")
                response.outputStream << excel
            }
            else{
                redirect action: "ricerca"
            }
        }catch (e){
            response.sendError(404)
        }
       /* def excel = ExcelReportsService.estrazioneTot()
        response.setContentType("application/vnd.ms-excel")
        response.setHeader("Content-disposition", "attachment; filename=Estrazione totale.xls")
        response.outputStream << excel*/
    }
    def caricaPlafond() {
        def polizze
        def logg
        def listPratiche=""
        def listFlussiPAI=""
        def listErrori=""
        def totaleErr=0
        def totale=0
        boolean speciale=false
        boolean rinnovi=false
        if (request.post) {
            def flussiPAI=[], errorePratica=[]
            def file =params.excelPratica
            def filename=""
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def risposta =[]
                Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
                Pattern pattern = ~/(\w+)/
                String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>";
                def sinistro
                try {
                    def wb = ExcelReader.readXlsx(myInputStream) {
                        sheet (0) {
                            rows(from: 1) {
                                def cellaNoSinistro = cell("B")?.value?:""
                                def dataDecorrenza=""
                                if((cellaNoSinistro.toString().equals("null") || cellaNoSinistro.toString()=='')){
                                    logg =new Log(parametri: "il numero di sinistro non e' stato inserito nella cella", operazione: "caricamento plafond", pagina: "ricerca")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    errorePratica.add("il numero di sinistro non e' stato inserito nella cella,")
                                }else{
                                    //cellaNoPolizza=cellaNoPolizza.toString().replaceFirst ("^0*", "")
                                    if(cellaNoSinistro.toString().contains(".")){
                                        def punto=cellaNoSinistro.toString().indexOf(".")
                                        //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                                        cellaNoSinistro=cellaNoSinistro.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                    }
                                    logg =new Log(parametri: "numero di sinistro inserito ${cellaNoSinistro}", operazione: "caricamento plafond", pagina: "ricerca")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    sinistro=Daticontabili.findByNoSinistro(cellaNoSinistro.toString().trim())
                                    if(!sinistro){
                                        def newSx=new Daticontabili()
                                        newSx.noSinistro=cellaNoSinistro
                                        def cellatipoPolizza = cell("A")?.value?:""
                                        if((cellatipoPolizza.toString().equals("null") || cellatipoPolizza.toString()=='')) {
                                            logg = new Log(parametri: "il tipo polizza non e' stato inserito", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            newSx.tipoPolizza=cellatipoPolizza
                                        }
                                        def cellaintestatario = cell("C")?.value?:""
                                        if((cellaintestatario.toString().equals("null") || cellaintestatario.toString()=='')) {
                                            logg = new Log(parametri: "il intestatario non e' stato inserito", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            newSx.intestatarioPolizza=cellaintestatario

                                        }
                                        def cellaintestatarioFat = cell("D")?.value?:""
                                        if((cellaintestatarioFat.toString().equals("null") || cellaintestatarioFat.toString()=='')) {
                                            logg = new Log(parametri: "il intestatario fatt non e' stato inserito", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            newSx.intestatarioFattura=cellaintestatarioFat
                                        }
                                        def cellaFattura = cell("E")?.value?:""
                                        if((cellaFattura.toString().equals("null") || cellaFattura.toString()=='')) {
                                            logg = new Log(parametri: "la fattura non e' stata inserita", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            newSx.noFattura=cellaFattura
                                        }
                                        def cellaImportoFattura = cell("F")?.value?:""
                                        if((cellaImportoFattura.toString().equals("null") || cellaImportoFattura.toString()=='')) {
                                            logg = new Log(parametri: "l'importo fattura non e' stato inserito", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            newSx.importoFattura=cellaImportoFattura
                                        }
                                        def cellaImportoLiq = cell("G")?.value?:""
                                        if((cellaImportoLiq.toString().equals("null") || cellaImportoLiq.toString()=='')) {
                                            logg = new Log(parametri: "l'importo liquidato non e' stato inserito", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            newSx.importoLiquidato=cellaImportoLiq
                                        }
                                        def cellaDiff = cell("H")?.value?:""
                                        if((cellaDiff.toString().equals("null") || cellaDiff.toString()=='')) {
                                            logg = new Log(parametri: "l'importo liquidato non e' stato inserito", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            newSx.differenzaLiquidare=cellaDiff
                                        }
                                        def cellaIntestatConto = cell("I")?.value?:""
                                        if((cellaIntestatConto.toString().equals("null") || cellaIntestatConto.toString()=='')) {
                                            logg = new Log(parametri: "l'intestatario conto non e' stato inserito", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            newSx.intestatarioConto=cellaIntestatConto
                                        }
                                        def cellaIban = cell("J")?.value?:""
                                        if((cellaIban.toString().equals("null") || cellaIban.toString()=='')) {
                                            logg = new Log(parametri: "l'IBAN non e' stato inserito", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            newSx.iban=cellaIban
                                        }
                                        def cellaDataPaga = cell("K")?.value?:""
                                        if((cellaDataPaga.toString().equals("null") || cellaDataPaga.toString()=='')) {
                                            logg = new Log(parametri: "la data paga non e' stata inserita", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            /*println "sinistro= ${cellaNoSinistro}"
                                            println "datapaga= ${cellaDataPaga}"*/
                                                if(cellaDataPaga.getClass()!=java.util.Date){
                                                    cellaDataPaga=Date.parse('dd/MM/yyyy',cellaDataPaga)
                                                }
                                            newSx.dataPagamento=cellaDataPaga
                                        }

                                        /*def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                                        String patternd = "#,##0.0#"
                                        symbols.setGroupingSeparator((char) ',')
                                        symbols.setDecimalSeparator((char) '.')
                                        def  decimalFormat = new DecimalFormat(patternd, symbols)
                                        decimalFormat.setParseBigDecimal(true)*/
                                        if(!newSx.save(flush: true)){
                                            logg = new Log(parametri: "il sinistro non e' stato salvato ${newSx.errors}", operazione: "caricamento plafond", pagina: "ricerca")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            println "il sinistro non e' stato salvato ${newSx.errors}"
                                        }
                                    }else{
                                        logg = new Log(parametri: "il sinistro esiste gia'", operazione: "caricamento plafond", pagina: "ricerca")
                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                }
                            }
                        }
                    }
                }catch (e){
                    println "c'\u00E8 un problema con il file, ${e.toString()}"
                    risposta.add("c'\u00E8 un problema con il file, ${e.toString()}")
                }
                if(errorePratica.size()>0){
                    for (String s : errorePratica)
                    {
                        listErrori += s + "\r\n\r\n"
                    }
                }
                if(flussiPAI.size()>0){
                    for (String s : flussiPAI)
                    {
                        listFlussiPAI += s + "\r\n\r\n"
                    }
                }
                if(flussiPAI.size()>0){
                    flash.flussiPAI=listFlussiPAI+="totale flussi generati ${totale}"
                }
                if(errorePratica.size()>0){
                    flash.errorePratica=listErrori+="totale flussi non generati ${totaleErr}"
                }
            } else flash.error = "Specificare l'elenco xlsx"
            redirect action: "ricerca"
        }else response.sendError(404)
    }
    def daticontabili(){
        def logg
        logg =new Log(parametri: "parametri che arrivano a dati contabili-->> ${params}", operazione: "daticontabili", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        //println "parametri che arrivano a dati contabili-->> ${params}"
        def datiSx=params.sinistro
        def noSx=datiSx.toString().substring(datiSx.toString().indexOf("noSinistro")+11, datiSx.toString().indexOf("noPolizza")-2)
        //println "nosx ${noSx.toString().trim()}"
        logg =new Log(parametri: "nosx ${noSx.toString().trim()}", operazione: "daticontabili", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def daticontabili=Daticontabili.findByNoSinistro(noSx.toString().trim())
        def allegati
        if(daticontabili?.allegati){
            allegati=daticontabili.allegati
        }
        [daticontabili:daticontabili, allegati:allegati, sinistro: datiSx, nosx:noSx, parametro:params.parametro]
    }
    def getDaticontabili(){
        //println "parametri che arrivano-->> ${params}"
        def logg
        logg =new Log(parametri: "parametri che arrivano a get dati contabili-->> ${params}", operazione: "daticontabili", pagina: "DETTAGLIO SINISTRO")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def daticontabili=Daticontabili.findByNoSinistro(params.noSx)
        def risposta
        if(daticontabili){
            render view: daticontabili, model: [daticontabili:daticontabili, parametro: params.parametro.toString().trim()]
            /*risposta=[risposta:true, noSinistro:daticontabili.noSinistro,tipoPolizza:daticontabili.tipoPolizza,intestatarioPolizza:daticontabili.intestatarioPolizza,intestatarioFattura:daticontabili.intestatarioPolizza,noFattura:daticontabili.noFattura,intestatarioConto:daticontabili.intestatarioConto,iban:daticontabili.iban,dataPagamento:daticontabili.dataPagamento.format("dd/MM/yyyy"),importoFattura:daticontabili.importoFattura,importoLiquidato:daticontabili.importoLiquidato,differenzaLiquidare:daticontabili.differenzaLiquidare,dettagliosx:params.dettagli,allegati:daticontabili.allegati.fileName]
            logg =new Log(parametri: "questi sono i valori contabili trovati del sinistro scelto ${risposta}", operazione: "dettaglioSinistro", pagina: "DETTAGLIO SINISTRO")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"*/

        }else{
            /*risposta=[risposta:false, dettagliosx:params.dettagli]*/
        }
        /*println risposta
        render risposta as JSON*/

    }
    def aggiornaDaticontabili(){
        def logg
        def risposta
        def sinistro
        def noSin
        if(request.post || params.dettasx){
            sinistro="[${params.dettasx}]"
            noSin=params.noSx
            noSin=noSin.toString().trim()
            //println "questi parametri vengono passati ${params}"
            logg =new Log(parametri: "questi parametri vengono passati quando aggiorno/aggiungo i dati contabili--> ${params}", operazione: "aggiunge/agiorna daticontabili", pagina: "DATI CONTABILI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(noSin){
                def dataPagamento=params.dataPagamento
                def dataPagamentoIntegrato=params.dataPagamentoIntegrato?:null
                dataPagamento=dataPagamento.toString().replace("/","-")
                def datocontabile=Daticontabili.findByNoSinistro(noSin)
                if(datocontabile){
                   /* if(!(params.tipoPolizza.toString()) || !(params.importoLiquidato.toString()) || !(params.intestatarioPolizza.toString()) || !(params.intestatarioConto.toString()) || !(params.dataPagamento.toString())){
                        flash.error="errore sezione contabile non salvata: controllare che i seguenti campi siano stati compilati--> tipo Polizza, importo Liquidato, intestatario polizza, intestatario Conto, data Pagamento"
                    }else {*/
                        def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                        String pattern = "#,##0.0#"
                        symbols.setGroupingSeparator((char) ',')
                        symbols.setDecimalSeparator((char) '.')
                        def decimalFormat = new DecimalFormat(pattern, symbols)
                        decimalFormat.setParseBigDecimal(true)

                        datocontabile.importoFattura = params.importoFattura.toString() ? (BigDecimal) decimalFormat.parse(params.importoFattura.toString()) : 0.0
                        datocontabile.differenzaLiquidare = params.differenzaLiquidare.toString() ? (BigDecimal) decimalFormat.parse(params.differenzaLiquidare.toString()) : 0.0
                        datocontabile.importoLiquidato = params.importoLiquidato.toString() ? (BigDecimal) decimalFormat.parse(params.importoLiquidato.toString()) : 0.0
                        datocontabile.dataPagamento = dataPagamento ? Date.parse('dd-MM-yyyy', dataPagamento) : null
                        datocontabile.dataPagamentoIntegrato = dataPagamentoIntegrato ? Date.parse('dd-MM-yyyy', dataPagamentoIntegrato) : null
                        datocontabile.iban = params.iban
                        datocontabile.intestatarioConto = params.intestatarioConto
                        datocontabile.intestatarioPolizza = params.intestatarioPolizza
                        datocontabile.intestatarioFattura = params.intestatarioFattura
                        datocontabile.tipoPolizza = params.tipoPolizza
                        datocontabile.origine = OrigineDatocontabile.SALVATODAPORTALE

                        if (!datocontabile.save(flush: true)) {
                            flash.error = "errore: sezione contabile non salvata, ${datocontabile.errors}"
                            //risposta="errore: sezione contabile non salvata, controllare"
                            logg = new Log(parametri: "errore aggiornamento dato contabile ${datocontabile.errors}", operazione: "dettaglioSinistro", pagina: "DATI CONTABILI")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        } else {
                            if ((params.fileFattura?.size > 0)) {
                                def allegati = new Allegati()
                                allegati.fileContent = params.fileFattura?.bytes
                                allegati.fileName = params.fileFattura?.originalFilename
                                if (!allegati.save(flush: true)) {
                                    //risposta="errore: allegati non salvati, controllare"
                                    flash.error = "errore: allegati non salvati, controllare"
                                    logg = new Log(parametri: "errore caricamento allegato ${allegati.errors}", operazione: "dettaglioSinistro", pagina: "DATI CONTABILI")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                } else {
                                    datocontabile.addToAllegati(allegati)
                                    if (!datocontabile.save(flush: true)) {
                                        //risposta="errore: sezione contabile non salvata, controllare"
                                        flash.error = "errore: sezione contabile non salvata, controllare"
                                        logg = new Log(parametri: "errore aggiornamento dato contabile ${datocontabile.errors}", operazione: "dettaglioSinistro", pagina: "DATI CONTABILI")
                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    } else {
                                        //risposta="sezione contabile salvata!"
                                        flash.message = "sezione contabile salvata!"
                                        logg = new Log(parametri: "dato per sinistro ${noSin} salvato", operazione: "dettaglioSinistro", pagina: "DATI CONTABILI")
                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                }

                            } else {
                                //risposta="sezione contabile salvata!"
                                flash.message = "sezione contabile salvata!"
                                logg = new Log(parametri: "dato per sinistro ${noSin} salvato", operazione: "dettaglioSinistro", pagina: "DATI CONTABILI")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }

                        }
                   // }
                }else{
                    /*if(!(params.tipoPolizza.toString()) && !(params.importoLiquidato.toString()) && !(params.intestatarioPolizza.toString()) && !(params.intestatarioConto.toString()) && !(params.dataPagamento.toString())){
                        flash.error="errore sezione contabile non salvata: controllare che i seguenti campi siano stati compilati--> tipo Polizza, importo Liquidato, intestatario polizza, intestatario Conto, data Pagamento"
                    }else{*/
                        dataPagamentoIntegrato=params.dataPagamentoIntegrato?:null
                        def newdatocontabile=new Daticontabili()
                        def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                        String pattern = "#,##0.0#"
                        symbols.setGroupingSeparator((char) ',')
                        symbols.setDecimalSeparator((char) '.')
                        def  decimalFormat = new DecimalFormat(pattern, symbols)
                        decimalFormat.setParseBigDecimal(true)
                        newdatocontabile.importoFattura = params.importoFattura.toString() ? (BigDecimal) decimalFormat.parse(params.importoFattura.toString()) :0.0
                        newdatocontabile.differenzaLiquidare=params.differenzaLiquidare.toString() ? (BigDecimal) decimalFormat.parse(params.differenzaLiquidare.toString()):0.0
                        newdatocontabile.importoLiquidato=params.importoLiquidato.toString() ? (BigDecimal) decimalFormat.parse(params.importoLiquidato.toString()):0.0
                        newdatocontabile.dataPagamento= dataPagamento? Date.parse( 'dd-MM-yyyy', params.dataPagamento ):null
                        newdatocontabile.iban=params.iban
                        newdatocontabile.intestatarioConto=params.intestatarioConto
                        newdatocontabile.intestatarioPolizza=params.intestatarioPolizza
                        newdatocontabile.intestatarioFattura=params.intestatarioFattura
                        newdatocontabile.noSinistro=noSin
                        newdatocontabile.tipoPolizza=params.tipoPolizza
                        newdatocontabile.noFattura=noSin
                        newdatocontabile.dataPagamentoIntegrato= dataPagamentoIntegrato? Date.parse( 'dd-MM-yyyy', dataPagamentoIntegrato):null

                        if(!newdatocontabile.save(flush:true)){
                            //risposta="errore: sezione contabile non salvata, controllare"
                            flash.error="errore: sezione contabile non salvata, controllare"
                            logg =new Log(parametri: "errore aggiornamento dato contabile ${newdatocontabile.errors}", operazione: "dettaglioSinistro", pagina: "DATI CONTABILI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }else{
                            if((params.fileFattura?.size>0)){
                                def allegati=new Allegati()
                                allegati.fileContent=params.fileFattura?.bytes
                                allegati.fileName=params.fileFattura?.originalFilename
                                if(!allegati.save(flush:true)){
                                    //risposta="errore: allegati non salvati, controllare"
                                    flash.error="errore: allegati non salvati, controllare"
                                    logg =new Log(parametri: "errore caricamento allegato ${allegati.errors}", operazione: "dettaglioSinistro", pagina: "DATI CONTABILI")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }else{
                                    newdatocontabile.addToAllegati(allegati)
                                    if(!newdatocontabile.save(flush:true)){
                                        //risposta="errore: sezione contabile non salvata, controllare"
                                        flash.error="errore: sezione contabile non salvata, controllare"
                                        logg =new Log(parametri: "errore aggiornamento dato contabile ${newdatocontabile.errors}", operazione: "dettaglioSinistro", pagina: "DATI CONTABILI")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }else{
                                        //risposta="sezione contabile salvata!"
                                        flash.message="sezione contabile salvata!"
                                        logg =new Log(parametri: "dato per sinistro ${noSin} salvato", operazione: "dettaglioSinistro", pagina: "DATI CONTABILI")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                }
                            }else{
                                //risposta="sezione contabile salvata!"
                                flash.message="sezione contabile salvata!"
                                logg =new Log(parametri: "dato per sinistro ${noSin} salvato", operazione: "dettaglioSinistro", pagina: "DATI CONTABILI")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }

                        }
                    //}

                }

            }

        }
       // println "risposta $risposta"
        redirect(action: "dettaglio", params:[sinistro:sinistro, risposta: risposta, sx:noSin, parametro: params.parametro.toString().trim()] )

    }
    def downloadAllegato(){
        def logg
        //println params
        logg =new Log(parametri: "id allegato ${params.id} ", operazione: "download allegato", pagina: "DATI CONTABILI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def id=Integer.parseInt(params.id)
        def allegato=Allegati.get(id)
        try {
            if(allegato){
                def file = allegato.fileContent
                response.setContentType("multipart/form-data")
                response.setHeader("Content-disposition", "attachment; filename=${allegato.fileName}")
                response.outputStream << file
            }
            /*else{
                redirect action: "daticontabili"
            }*/
        }catch (e){
            response.sendError(404)
        }
        /* def excel = ExcelReportsService.estrazioneTot()
         response.setContentType("application/vnd.ms-excel")
         response.setHeader("Content-disposition", "attachment; filename=Estrazione totale.xls")
         response.outputStream << excel*/
    }
    def estrazioniContabili(){
        def logg
        if(request.post || params)
        {
            //def company
            //company=params.compagnia
            def compagnia=params.compagnia
            def companyarray = []
            compagnia.split(',').collect { entry ->
                companyarray<<entry
            }
            def company = new String[companyarray.size()]
            for(int j =0;j<companyarray.size();j++){
                company[j] = companyarray.getAt(j)
            }
            def data1=params.dataaperturada ? Date.parse("dd-MM-yyyy", params.dataaperturada): null
            def data2=params.dataaperturaal ? Date.parse("dd-MM-yyyy", params.dataaperturaal) : null

            if(data1 && data2 && company){
                /*def fileEstra=Sinistridb.withCriteria {
                    if(company){
                        inList ( 'compagnia', company)
                    }
                    if(data1 && data2)
                        between "dataapertura", data1, data2
                    order  "dataapertura","asc"
                }*/

                def fileEstra=Daticontabili.withCriteria {
                    if (company) {
                        inList('tipoPolizza', company)
                    }
                    if (data1 && data2)
                        between "dataPagamentoIntegrato", data1, data2
                    order "dataPagamentoIntegrato", "asc"
                }
                try {
                    if(fileEstra){
                        def wb =new XSSFWorkbook()
                        def createHelper = wb.getCreationHelper()
                        CellStyle hlink_style = wb.createCellStyle()
                        Font hlink_font = wb.createFont()
                        hlink_font.setUnderline(Font.U_SINGLE)
                        hlink_font.setColor(IndexedColors.BLUE.getIndex())
                        hlink_style.setFont(hlink_font)
                        def link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                        def my_sheet = wb.createSheet("Estrazione contabile")
                        def  style = wb.createCellStyle()
                        style.setAlignment(HorizontalAlignment.CENTER)
                        style.setVerticalAlignment(VerticalAlignment.CENTER)
                        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex())
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND)
                        def row = my_sheet.createRow(0)
                        def cell = row.createCell(0)
                        cell.setCellValue("NUMERO SINISTRO")
                        cell.setCellStyle(style)
                        cell = row.createCell(1)
                        cell.setCellValue("NUMERO FATTURA")
                        cell.setCellStyle(style)
                        cell = row.createCell(2)
                        cell.setCellValue("INTESTATARIO FATTURA")
                        cell.setCellStyle(style)
                        cell = row.createCell(3)
                        cell.setCellValue("INTESTATARIO POLIZZA")
                        cell.setCellStyle(style)
                        cell = row.createCell(4)
                        cell.setCellValue("INTESTATARIO CONTO")
                        cell.setCellStyle(style)
                        cell = row.createCell(5)
                        cell.setCellValue("DATA PAGAMENTO")
                        cell.setCellStyle(style)
                        cell = row.createCell(6)
                        cell.setCellValue("IMPORTO FATTURA")
                        cell.setCellStyle(style)
                        cell = row.createCell(7)
                        cell.setCellValue("IMPORTO LIQUIDATO")
                        cell.setCellStyle(style)
                        cell = row.createCell(8)
                        cell.setCellValue("DIFFERENZA DA INTEGRARE")
                        cell.setCellStyle(style)
                        cell = row.createCell(9)
                        cell.setCellValue("DATA PAGAMENTO INTEGRATO")
                        cell.setCellStyle(style)
                        cell = row.createCell(10)
                        cell.setCellValue("FATTURA ALLEGATA")
                        cell.setCellStyle(style)
                        if(fileEstra){
                            fileEstra.eachWithIndex { datoconta, idx ->
                                if(datoconta.allegati && datoconta.allegati.size()>1){
                                    row = my_sheet.createRow(idx+1)
                                    cell = row.createCell(0)
                                    cell.setCellValue(datoconta.noSinistro)
                                    cell = row.createCell(1)
                                    cell.setCellValue(datoconta.noFattura)
                                    cell = row.createCell(2)
                                    cell.setCellValue((datoconta.intestatarioFattura.toString() !='null') ? datoconta.intestatarioFattura.toString().toUpperCase(): '')
                                    cell = row.createCell(3)
                                    cell.setCellValue(datoconta.intestatarioPolizza.toString().toUpperCase())
                                    cell = row.createCell(4)
                                    cell.setCellValue(datoconta.intestatarioConto.toString().toUpperCase())
                                    cell = row.createCell(5)
                                    cell.setCellValue(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                    cell = row.createCell(6)
                                    cell.setCellValue(datoconta.importoFattura)
                                    cell = row.createCell(7)
                                    cell.setCellValue(datoconta.importoLiquidato)
                                    cell = row.createCell(8)
                                    cell.setCellValue(datoconta.differenzaLiquidare)
                                    cell = row.createCell(9)
                                    cell.setCellValue(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                    def celnum=10
                                    datoconta.allegati.sort().eachWithIndex { allegato, count ->
                                        cell = row.createCell(celnum+count)
                                        if ( Environment.current != Environment.PRODUCTION) {
                                            link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}")
                                            cell.setCellValue("${allegato.fileName}")
                                            cell.setHyperlink(link)
                                            cell.setCellStyle(hlink_style)
                                        }else{
                                            link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                            cell.setCellValue("${allegato.fileName}")
                                            cell.setHyperlink(link)
                                            cell.setCellStyle(hlink_style)
                                        }
                                    }

                                    datoconta.discard()
                                }else if(datoconta.allegati && datoconta.allegati.size()==1){
                                    def allegato=datoconta.allegati.first()
                                    row = my_sheet.createRow(idx+1)
                                    cell = row.createCell(0)
                                    cell.setCellValue(datoconta.noSinistro)
                                    cell = row.createCell(1)
                                    cell.setCellValue(datoconta.noFattura)
                                    cell = row.createCell(2)
                                    cell.setCellValue((datoconta.intestatarioFattura.toString() !='null') ? datoconta.intestatarioFattura.toString().toUpperCase():'')
                                    cell = row.createCell(3)
                                    cell.setCellValue(datoconta.intestatarioPolizza.toString().toUpperCase())
                                    cell = row.createCell(4)
                                    cell.setCellValue(datoconta.intestatarioConto.toString().toUpperCase())
                                    cell = row.createCell(5)
                                    cell.setCellValue(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                    cell = row.createCell(6)
                                    cell.setCellValue(datoconta.importoFattura)
                                    cell = row.createCell(7)
                                    cell.setCellValue(datoconta.importoLiquidato)
                                    cell = row.createCell(8)
                                    cell.setCellValue(datoconta.differenzaLiquidare)
                                    cell = row.createCell(9)
                                    cell.setCellValue(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                    cell = row.createCell(10)
                                    if ( Environment.current != Environment.PRODUCTION) {
                                        link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                                        link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}")
                                        cell.setCellValue("${allegato.fileName}")
                                        cell.setHyperlink(link)
                                        cell.setCellStyle(hlink_style)
                                    }else{
                                        link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                                        link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                        cell.setCellValue("${allegato.fileName}")
                                        cell.setHyperlink(link)
                                        cell.setCellStyle(hlink_style)
                                    }
                                    datoconta.discard()
                                }
                                else{
                                    row = my_sheet.createRow(idx+1)
                                    cell = row.createCell(0)
                                    cell.setCellValue(datoconta.noSinistro)
                                    cell = row.createCell(1)
                                    cell.setCellValue(datoconta.noFattura)
                                    cell = row.createCell(2)
                                    cell.setCellValue( (datoconta.intestatarioFattura.toString() !='null') ?  datoconta.intestatarioFattura.toString().toUpperCase():'')
                                    cell = row.createCell(3)
                                    cell.setCellValue(datoconta.intestatarioPolizza.toString().toUpperCase())
                                    cell = row.createCell(4)
                                    cell.setCellValue(datoconta.intestatarioConto.toString().toUpperCase())
                                    cell = row.createCell(5)
                                    cell.setCellValue(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                    cell = row.createCell(6)
                                    cell.setCellValue(datoconta.importoFattura)
                                    cell = row.createCell(7)
                                    cell.setCellValue(datoconta.importoLiquidato)
                                    cell = row.createCell(8)
                                    cell.setCellValue(datoconta.differenzaLiquidare)
                                    cell = row.createCell(9)
                                    cell.setCellValue(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                    cell = row.createCell(10)
                                    cell.setCellValue("non ci sono allegati")
                                    datoconta.discard()
                                }
                            }
                        }else{
                            row = my_sheet.createRow(1)
                            cell = row.createCell(0)
                            cell.setCellValue("non ci sono sinistri")
                        }
                        /*wb = ExcelBuilder.createXls {
                            sheet("Dati contabili") {
                                row(style: "header") {
                                    cell("Numero Sinistro")
                                    cell("Numero Fattura")
                                    cell("Intestatario fattura")
                                    cell("Intestatario polizza")
                                    cell("Intestatario conto")
                                    cell("Data pagamento")
                                    cell("Importo fattura")
                                    cell("Importo liquidato")
                                    cell("Differenza da integrare")
                                    cell("Data pagamento integrato")
                                    cell("Fattura allegata")
                                }
                                if(fileEstra){
                                    fileEstra.each { datoconta ->
                                        if(datoconta.allegati && datoconta.allegati.size()>1){
                                            datoconta.allegati.sort().eachWithIndex {allegato, count ->
                                                if(count==0){
                                                    row {
                                                        cell(datoconta.noSinistro)
                                                        cell(datoconta.noFattura)
                                                        cell(datoconta.intestatarioFattura.toString().toUpperCase())
                                                        cell(datoconta.intestatarioPolizza.toString().toUpperCase())
                                                        cell(datoconta.intestatarioConto.toString().toUpperCase())
                                                        cell(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                                        cell(datoconta.importoFattura)
                                                        cell(datoconta.importoLiquidato)
                                                        cell(datoconta.differenzaLiquidare)
                                                        cell(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                                        if ( Environment.current != Environment.PRODUCTION) {

                                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                            cell.setHyperlink(link)
                                                            cell.setCellStyle(hlink_style)
                                                            //cell(link)
                                                        }else{
                                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                            cell.setHyperlink(link)
                                                            cell.setCellStyle(hlink_style)
                                                            //cell("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                        }
                                                    }
                                                }else{
                                                    row {
                                                        cell("")
                                                        cell("ulteriore")
                                                        cell("allegato")
                                                        cell("del sinistro")
                                                        cell("${datoconta.noSinistro}")
                                                        cell("")
                                                        cell("")
                                                        cell("")
                                                        cell("")
                                                        cell("")
                                                        if ( Environment.current != Environment.PRODUCTION) {
                                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                            cell.setHyperlink(link)
                                                            cell.setCellStyle(hlink_style)
                                                        }else{

                                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                            cell.setHyperlink(link)
                                                            cell.setCellStyle(hlink_style)
                                                            //cell("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                        }
                                                    }
                                                }

                                            }
                                            datoconta.discard()
                                        }else if(datoconta.allegati && datoconta.allegati.size()==1){
                                            def allegato=datoconta.allegati.first().id.toString()
                                            row {
                                                cell(datoconta.noSinistro)
                                                cell(datoconta.noFattura)
                                                cell(datoconta.intestatarioFattura.toString().toUpperCase())
                                                cell(datoconta.intestatarioPolizza.toString().toUpperCase())
                                                cell(datoconta.intestatarioConto.toString().toUpperCase())
                                                cell(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                                cell(datoconta.importoFattura)
                                                cell(datoconta.importoLiquidato)
                                                cell(datoconta.differenzaLiquidare)
                                                cell(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                                if ( Environment.current != Environment.PRODUCTION) {
                                                    link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.toString()}")
                                                    cell.setHyperlink(link)
                                                    cell.setCellStyle(hlink_style)
                                                   // cell("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.toString()}")
                                                }else{
                                                    link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.toString()}")
                                                    cell.setHyperlink(link)
                                                    cell.setCellStyle(hlink_style)
                                                   //cell("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.toString()}")
                                                }
                                            }
                                            datoconta.discard()
                                        }
                                        else{
                                            row {
                                                cell(datoconta.noSinistro)
                                                cell(datoconta.noFattura)
                                                cell(datoconta.intestatarioFattura.toString().toUpperCase())
                                                cell(datoconta.intestatarioPolizza.toString().toUpperCase())
                                                cell(datoconta.intestatarioConto.toString().toUpperCase())
                                                cell(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                                cell(datoconta.importoFattura)
                                                cell(datoconta.importoLiquidato)
                                                cell(datoconta.differenzaLiquidare)
                                                cell(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                                cell("non ci sono allegati per questo sinistro")
                                            }
                                            datoconta.discard()
                                        }

                                    }
                                    for (int i = 0; i < 17; i++) {
                                        sheet.autoSizeColumn(i)
                                    }
                                }else{
                                    row{
                                        cell("non ci sono dati contabili")
                                    }
                                }

                            }

                        }*/
                        for (int i = 0; i < 26; i++) {
                            my_sheet.setColumnWidth(i, 30*256)
                        }
                        def stream = new ByteArrayOutputStream()
                        wb.write(stream)
                        stream.close()
                        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        response.addHeader "Content-disposition", "inline; filename=estrazionecontabile_${new Date().format("yyyyMMdd")}.xlsx"
                        response.outputStream << stream.toByteArray()
                    }
                    else{
                        flash.error= "non ci sono sinistri nella data e compagnie selezionate"
                        redirect action: "estrazioni"
                    }
                }catch (e){
                    response.sendError(404)
                }
            }else{
                flash.error ="selezionare un parametro"
            }

        }else{
            flash.error= "selezionare un parametro"
        }
    }
    def estrazioniContabiliTable(){
        def logg
        def risposta
        if(request.post || params)
        {
            def company
            company=params.compagnia1

            def data1=params.datada ? Date.parse("dd-MM-yyyy", params.datada): null
            def data2=params.dataal ? Date.parse("dd-MM-yyyy", params.dataal) : null

            if(data1 && data2){
                /*def fileEstra=Sinistridb.withCriteria {
                    if(company){
                        inList ( 'compagnia', company)
                    }
                    if(data1 && data2)
                        between "dataapertura", data1, data2

                    order  "dataapertura","asc"
                }*/
                def fileEstra=Daticontabili.withCriteria {
                    if (company) {
                        inList('tipoPolizza', company)
                    }
                    if (data1 && data2)
                        between "dataPagamentoIntegrato", data1, data2
                    order "dataPagamentoIntegrato", "asc"
                }
                try {
                    if(fileEstra){
                        render view: 'estrazioni', model: [datiEstrazione:fileEstra, datada:params.datada,dataal: params.dataal, company:params.compagnia1]

                        /*def wb =new XSSFWorkbook()
                        def createHelper = wb.getCreationHelper()
                        CellStyle hlink_style = wb.createCellStyle()
                        Font hlink_font = wb.createFont()
                        hlink_font.setUnderline(Font.U_SINGLE)
                        hlink_font.setColor(IndexedColors.BLUE.getIndex())
                        hlink_style.setFont(hlink_font)
                        def link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                        def my_sheet = wb.createSheet("Estrazione contabile")
                        def  style = wb.createCellStyle()
                        style.setAlignment(HorizontalAlignment.CENTER)
                        style.setVerticalAlignment(VerticalAlignment.CENTER)
                        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex())
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND)
                        def row = my_sheet.createRow(0)
                        def cell = row.createCell(0)
                        cell.setCellValue("NOME")
                        cell.setCellStyle(style)
                        cell = row.createCell(1)
                        cell.setCellValue("CODICE FISCALE")
                        cell.setCellStyle(style)
                        cell = row.createCell(2)
                        cell.setCellValue("TARGA")
                        cell.setCellStyle(style)
                        cell = row.createCell(3)
                        cell.setCellValue("NO. SINISTRO")
                        cell.setCellStyle(style)
                        cell = row.createCell(4)
                        cell.setCellValue("NO. POLIZZA")
                        cell.setCellStyle(style)
                        cell = row.createCell(5)
                        cell.setCellValue("RIPARATORE")
                        cell.setCellStyle(style)
                        cell = row.createCell(6)
                        cell.setCellValue("PARTITA IVA RIPARATORE")
                        cell.setCellStyle(style)
                        cell = row.createCell(7)
                        cell.setCellValue("DATA APERTURA")
                        cell.setCellStyle(style)
                        cell = row.createCell(8)
                        cell.setCellValue("DATA SINISTRO")
                        cell.setCellStyle(style)
                        cell = row.createCell(9)
                        cell.setCellValue("DATA DENUNCIA")
                        cell.setCellStyle(style)
                        cell = row.createCell(10)
                        cell.setCellValue("EVENTO")
                        cell.setCellStyle(style)
                        cell = row.createCell(11)
                        cell.setCellValue("DESCRIZIONE")
                        cell.setCellStyle(style)
                        cell = row.createCell(12)
                        cell.setCellValue("DESCRIZIONE2")
                        cell.setCellStyle(style)
                        cell = row.createCell(13)
                        cell.setCellValue("ITER")
                        cell.setCellStyle(style)
                        cell = row.createCell(14)
                        cell.setCellValue("DATA ITER")
                        cell.setCellStyle(style)
                        cell = row.createCell(15)
                        cell.setCellValue("STATO")
                        cell.setCellStyle(style)
                        cell = row.createCell(16)
                        cell.setCellValue("RICHIESTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(17)
                        cell.setCellValue("DATA RICHIESTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(18)
                        cell.setCellValue("OFFERTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(19)
                        cell.setCellValue("DATA OFFERTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(20)
                        cell.setCellValue("LIQUIDAZIONE")
                        cell.setCellStyle(style)
                        cell = row.createCell(21)
                        cell.setCellValue("DATA LIQUIDAZIONE")
                        cell.setCellStyle(style)
                        cell = row.createCell(22)
                        cell.setCellValue("SCOPERTO %")
                        cell.setCellStyle(style)
                        cell = row.createCell(23)
                        cell.setCellValue("FRANCHIGIA")
                        cell.setCellStyle(style)
                        cell = row.createCell(24)
                        cell.setCellValue("NOTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(25)
                        cell.setCellValue("IPERTESTO")
                        cell.setCellStyle(style)
                        if(fileEstra){
                            fileEstra.eachWithIndex { datoconta, idx ->
                                row = my_sheet.createRow(idx+1)
                                cell = row.createCell(0)
                                cell.setCellValue(datoconta.nome.toString().toUpperCase())
                                cell = row.createCell(1)
                                cell.setCellValue(datoconta.codicefiscale.toString().toUpperCase())
                                cell = row.createCell(2)
                                cell.setCellValue(datoconta.targa.toString().toUpperCase())
                                cell = row.createCell(3)
                                cell.setCellValue(datoconta.nosinistro)
                                cell = row.createCell(4)
                                cell.setCellValue(datoconta.nopolizza)
                                cell = row.createCell(5)
                                cell.setCellValue(datoconta.riparatore ? datoconta.riparatore.toString().toUpperCase(): '')
                                cell = row.createCell(6)
                                cell.setCellValue(datoconta.partitaivariparatore ? datoconta.partitaivariparatore.toString().toUpperCase() : '')
                                cell = row.createCell(7)
                                cell.setCellValue(datoconta.dataapertura ? datoconta.dataapertura.format("dd/MM/yyyy"):"")
                                cell = row.createCell(8)
                                cell.setCellValue(datoconta.datasinistro ? datoconta.datasinistro.format("dd/MM/yyyy"):"")
                                cell = row.createCell(9)
                                cell.setCellValue(datoconta.datadenuncia ? datoconta.datadenuncia.format("dd/MM/yyyy"):"")
                                cell = row.createCell(10)
                                cell.setCellValue(datoconta.evento)
                                cell = row.createCell(11)
                                cell.setCellValue(datoconta.descrizione)
                                cell = row.createCell(12)
                                cell.setCellValue(datoconta.descrizione2)
                                cell = row.createCell(13)
                                cell.setCellValue(datoconta.iter)
                                cell = row.createCell(14)
                                cell.setCellValue(datoconta.dataiter ? datoconta.dataiter.format("dd/MM/yyyy"):"")
                                cell = row.createCell(15)
                                cell.setCellValue(datoconta.stato)
                                cell = row.createCell(16)
                                cell.setCellValue(datoconta.richiesta)
                                cell = row.createCell(17)
                                cell.setCellValue(datoconta.datarichiesta ? datoconta.datarichiesta.format("dd/MM/yyyy"):"")
                                cell = row.createCell(18)
                                cell.setCellValue(datoconta.offerta)
                                cell = row.createCell(19)
                                cell.setCellValue(datoconta.dataofferta ? datoconta.dataofferta.format("dd/MM/yyyy"):"")
                                cell = row.createCell(20)
                                cell.setCellValue(datoconta.liquidazione)
                                cell = row.createCell(21)
                                cell.setCellValue(datoconta.dataliquidazione ? datoconta.dataliquidazione.format("dd/MM/yyyy"):"")
                                cell = row.createCell(22)
                                cell.setCellValue(datoconta.scoperto)
                                cell = row.createCell(23)
                                cell.setCellValue(datoconta.franchigia)
                                cell = row.createCell(24)
                                cell.setCellValue(datoconta.nota)
                                cell = row.createCell(25)
                                cell.setCellValue(datoconta.ipertesto)
                                datoconta.discard()
                            }
                        }else{
                            row = my_sheet.createRow(1)
                            cell = row.createCell(0)
                            cell.setCellValue("non ci sono sinistri")
                        }

                        for (int i = 0; i < 26; i++) {
                            my_sheet.setColumnWidth(i, 30*256)
                        }
                        def stream = new ByteArrayOutputStream()
                        wb.write(stream)
                        stream.close()
                        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        response.addHeader "Content-disposition", "inline; filename=estrazionecontabile_${new Date().format("yyyyMMdd")}.xlsx"
                        response.outputStream << stream.toByteArray()*/
                    }
                    else{
                        flash.error= "non ci sono sinistri nella data e compagnie selezionate"
                        render view: 'estrazioni', model: [ datada:params.datada,dataal: params.dataal, company:params.compagnia1]
                    }
                }catch (e){
                    //response.sendError(404)
                    render view: 'estrazioni', model: [ datada:params.datada,dataal: params.dataal, company:params.compagnia1]
                }
            }else{

                flash.error ="selezionare un parametro"
                render view: 'estrazioni'
            }

        }else{

            flash.error= "selezionare un parametro"
            render view: 'estrazioni', model: [ datada:params.datada,dataal: params.dataal, company:params.compagnia1]
        }

    }
    def estrazioniSinistri(){
        def logg
        if(request.post || params)
        {
            def stato
            stato=params.stato
            def statoArr
            //println "sono qui ${params}"
            if(stato.toString().trim().contains("1") && stato.toString().trim().contains("2"))
                statoArr=["APERTO", "DEFINITO","SENZA SEGUITO"]
            else if (stato.toString().trim().contains("1") && !stato.toString().trim().contains("2"))
                statoArr=["APERTO"]
            else if(!stato.toString().trim().contains("1") && stato.toString().trim().contains("2"))
                statoArr=["DEFINITO","SENZA SEGUITO"]


            def data1=params.dataaperturada ? Date.parse("dd-MM-yyyy", params.dataaperturada): null
            def data2=params.dataaperturaal ? Date.parse("dd-MM-yyyy", params.dataaperturaal) : null

            if(data1 && data2 && stato){
                def fileEstra=Sinistridb.withCriteria {
                    if(stato){
                        inList ( 'stato', statoArr)
                    }
                    if(data1 && data2)
                      between "dataapertura", data1, data2

                    order  "dataapertura","asc"
                }
                //try {
                    if(fileEstra){

                        def wb =new XSSFWorkbook()
                        def createHelper = wb.getCreationHelper()
                        CellStyle hlink_style = wb.createCellStyle()
                        Font hlink_font = wb.createFont()
                        hlink_font.setUnderline(Font.U_SINGLE)
                        hlink_font.setColor(IndexedColors.BLUE.getIndex())
                        hlink_style.setFont(hlink_font)
                        def link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                        def my_sheet = wb.createSheet("Estrazione sinistri")
                        def  style = wb.createCellStyle()
                        style.setAlignment(HorizontalAlignment.CENTER)
                        style.setVerticalAlignment(VerticalAlignment.CENTER)
                        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex())
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND)
                        def row = my_sheet.createRow(0)
                        def cell = row.createCell(0)
                        cell.setCellValue("NOME")
                        cell.setCellStyle(style)
                        cell = row.createCell(1)
                        cell.setCellValue("TARGA")
                        cell.setCellStyle(style)
                        cell = row.createCell(2)
                        cell.setCellValue("NO. SINISTRO")
                        cell.setCellStyle(style)
                        cell = row.createCell(3)
                        cell.setCellValue("NO. POLIZZA")
                        cell.setCellStyle(style)
                        cell = row.createCell(4)
                        cell.setCellValue("CODICE FISCALE")
                        cell.setCellStyle(style)
                        cell = row.createCell(5)
                        cell.setCellValue("RIPARATORE")
                        cell.setCellStyle(style)
                        cell = row.createCell(6)
                        cell.setCellValue("PARTITA IVA RIPARATORE")
                        cell.setCellStyle(style)
                        cell = row.createCell(7)
                        cell.setCellValue("DATA APERTURA")
                        cell.setCellStyle(style)
                        cell = row.createCell(8)
                        cell.setCellValue("DATA SINISTRO")
                        cell.setCellStyle(style)
                        cell = row.createCell(9)
                        cell.setCellValue("DATA DENUNCIA")
                        cell.setCellStyle(style)
                        cell = row.createCell(10)
                        cell.setCellValue("EVENTO")
                        cell.setCellStyle(style)
                        cell = row.createCell(11)
                        cell.setCellValue("DESCRIZIONE")
                        cell.setCellStyle(style)
                        cell = row.createCell(12)
                        cell.setCellValue("DESCRIZIONE2")
                        cell.setCellStyle(style)
                        cell = row.createCell(13)
                        cell.setCellValue("ITER")
                        cell.setCellStyle(style)
                        cell = row.createCell(14)
                        cell.setCellValue("DATA ITER")
                        cell.setCellStyle(style)
                        cell = row.createCell(15)
                        cell.setCellValue("STATO")
                        cell.setCellStyle(style)
                        cell = row.createCell(16)
                        cell.setCellValue("RICHIESTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(17)
                        cell.setCellValue("DATA RICHIESTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(18)
                        cell.setCellValue("OFFERTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(19)
                        cell.setCellValue("DATA OFFERTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(20)
                        cell.setCellValue("LIQUIDAZIONE")
                        cell.setCellStyle(style)
                        cell = row.createCell(21)
                        cell.setCellValue("DATA LIQUIDAZIONE")
                        cell.setCellStyle(style)
                        cell = row.createCell(22)
                        cell.setCellValue("SCOPERTO %")
                        cell.setCellStyle(style)
                        cell = row.createCell(23)
                        cell.setCellValue("FRANCHIGIA")
                        cell.setCellStyle(style)
                        cell = row.createCell(24)
                        cell.setCellValue("NOTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(25)
                        cell.setCellValue("IPERTESTO")
                        cell.setCellStyle(style)
                        cell = row.createCell(26)
                        cell.setCellValue("COMPAGNIA")
                        cell.setCellStyle(style)
                        cell = row.createCell(27)
                        cell.setCellValue("RAMO")
                        cell.setCellStyle(style)
                        if(fileEstra){
                            fileEstra.eachWithIndex { datoconta, idx ->
                                row = my_sheet.createRow(idx+1)
                                cell = row.createCell(0)
                                cell.setCellValue(datoconta.nome.toString().toUpperCase())
                                cell = row.createCell(1)
                                cell.setCellValue(datoconta.targa.toString().toUpperCase())
                                cell = row.createCell(2)
                                cell.setCellValue(datoconta.nosinistro)
                                cell = row.createCell(3)
                                cell.setCellValue(datoconta.nopolizza)
                                cell = row.createCell(4)
                                cell.setCellValue(datoconta.codicefiscale.toString().toUpperCase())
                                cell = row.createCell(5)
                                cell.setCellValue(datoconta.riparatore.toString()!='null' ? datoconta.riparatore.toString().toUpperCase(): '')
                                cell = row.createCell(6)
                                cell.setCellValue(datoconta.partitaivariparatore.toString()!='null' ? datoconta.partitaivariparatore.toString().toUpperCase():'')
                                cell = row.createCell(7)
                                cell.setCellValue(datoconta.dataapertura ? datoconta.dataapertura.format("dd/MM/yyyy"):"")
                                cell = row.createCell(8)
                                cell.setCellValue(datoconta.datasinistro ? datoconta.datasinistro.format("dd/MM/yyyy"):"")
                                cell = row.createCell(9)
                                cell.setCellValue(datoconta.datadenuncia ? datoconta.datadenuncia.format("dd/MM/yyyy"):"")
                                cell = row.createCell(10)
                                cell.setCellValue(datoconta.evento)
                                cell = row.createCell(11)
                                cell.setCellValue(datoconta.descrizione)
                                cell = row.createCell(12)
                                cell.setCellValue(datoconta.descrizione2)
                                cell = row.createCell(13)
                                cell.setCellValue(datoconta.iter)
                                cell = row.createCell(14)
                                cell.setCellValue(datoconta.dataiter ? datoconta.dataiter.format("dd/MM/yyyy"):"")
                                cell = row.createCell(15)
                                cell.setCellValue(datoconta.stato)
                                cell = row.createCell(16)
                                cell.setCellValue(datoconta.richiesta)
                                cell = row.createCell(17)
                                cell.setCellValue(datoconta.datarichiesta ? datoconta.datarichiesta.format("dd/MM/yyyy"):"")
                                cell = row.createCell(18)
                                cell.setCellValue(datoconta.offerta)
                                cell = row.createCell(19)
                                cell.setCellValue(datoconta.dataofferta ? datoconta.dataofferta.format("dd/MM/yyyy"):"")
                                cell = row.createCell(20)
                                cell.setCellValue(datoconta.liquidazione)
                                cell = row.createCell(21)
                                cell.setCellValue(datoconta.dataliquidazione ? datoconta.dataliquidazione.format("dd/MM/yyyy"):"")
                                cell = row.createCell(22)
                                cell.setCellValue(datoconta.scoperto)
                                cell = row.createCell(23)
                                cell.setCellValue(datoconta.franchigia)
                                cell = row.createCell(24)
                                cell.setCellValue(datoconta.nota)
                                cell = row.createCell(25)
                                cell.setCellValue(datoconta.ipertesto)
                                cell = row.createCell(26)
                                cell.setCellValue(datoconta.compagnia)
                                cell = row.createCell(27)
                                cell.setCellValue(datoconta.ramo)
                                datoconta.discard()
                            }
                        }else{
                            row = my_sheet.createRow(1)
                            cell = row.createCell(0)
                            cell.setCellValue("non ci sono sinistri")
                        }
                        /*wb = ExcelBuilder.createXls {
                            sheet("Dati contabili") {
                                row(style: "header") {
                                    cell("Numero Sinistro")
                                    cell("Numero Fattura")
                                    cell("Intestatario fattura")
                                    cell("Intestatario polizza")
                                    cell("Intestatario conto")
                                    cell("Data pagamento")
                                    cell("Importo fattura")
                                    cell("Importo liquidato")
                                    cell("Differenza da integrare")
                                    cell("Data pagamento integrato")
                                    cell("Fattura allegata")
                                }
                                if(fileEstra){
                                    fileEstra.each { datoconta ->
                                        if(datoconta.allegati && datoconta.allegati.size()>1){
                                            datoconta.allegati.sort().eachWithIndex {allegato, count ->
                                                if(count==0){
                                                    row {
                                                        cell(datoconta.noSinistro)
                                                        cell(datoconta.noFattura)
                                                        cell(datoconta.intestatarioFattura.toString().toUpperCase())
                                                        cell(datoconta.intestatarioPolizza.toString().toUpperCase())
                                                        cell(datoconta.intestatarioConto.toString().toUpperCase())
                                                        cell(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                                        cell(datoconta.importoFattura)
                                                        cell(datoconta.importoLiquidato)
                                                        cell(datoconta.differenzaLiquidare)
                                                        cell(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                                        if ( Environment.current != Environment.PRODUCTION) {

                                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                            cell.setHyperlink(link)
                                                            cell.setCellStyle(hlink_style)
                                                            //cell(link)
                                                        }else{
                                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                            cell.setHyperlink(link)
                                                            cell.setCellStyle(hlink_style)
                                                            //cell("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                        }
                                                    }
                                                }else{
                                                    row {
                                                        cell("")
                                                        cell("ulteriore")
                                                        cell("allegato")
                                                        cell("del sinistro")
                                                        cell("${datoconta.noSinistro}")
                                                        cell("")
                                                        cell("")
                                                        cell("")
                                                        cell("")
                                                        cell("")
                                                        if ( Environment.current != Environment.PRODUCTION) {
                                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                            cell.setHyperlink(link)
                                                            cell.setCellStyle(hlink_style)
                                                        }else{

                                                            link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                            cell.setHyperlink(link)
                                                            cell.setCellStyle(hlink_style)
                                                            //cell("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.id.toString()}")
                                                        }
                                                    }
                                                }

                                            }
                                            datoconta.discard()
                                        }else if(datoconta.allegati && datoconta.allegati.size()==1){
                                            def allegato=datoconta.allegati.first().id.toString()
                                            row {
                                                cell(datoconta.noSinistro)
                                                cell(datoconta.noFattura)
                                                cell(datoconta.intestatarioFattura.toString().toUpperCase())
                                                cell(datoconta.intestatarioPolizza.toString().toUpperCase())
                                                cell(datoconta.intestatarioConto.toString().toUpperCase())
                                                cell(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                                cell(datoconta.importoFattura)
                                                cell(datoconta.importoLiquidato)
                                                cell(datoconta.differenzaLiquidare)
                                                cell(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                                if ( Environment.current != Environment.PRODUCTION) {
                                                    link.setAddress("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.toString()}")
                                                    cell.setHyperlink(link)
                                                    cell.setCellStyle(hlink_style)
                                                   // cell("http://pro.mach-1.it/ESMOBILITY_TEST/sinistri/downloadAllegato/${allegato.toString()}")
                                                }else{
                                                    link.setAddress("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.toString()}")
                                                    cell.setHyperlink(link)
                                                    cell.setCellStyle(hlink_style)
                                                   //cell("http://pro.mach-1.it/ESMOBILITY/sinistri/downloadAllegato/${allegato.toString()}")
                                                }
                                            }
                                            datoconta.discard()
                                        }
                                        else{
                                            row {
                                                cell(datoconta.noSinistro)
                                                cell(datoconta.noFattura)
                                                cell(datoconta.intestatarioFattura.toString().toUpperCase())
                                                cell(datoconta.intestatarioPolizza.toString().toUpperCase())
                                                cell(datoconta.intestatarioConto.toString().toUpperCase())
                                                cell(datoconta.dataPagamento.format("dd/MM/yyyy"))
                                                cell(datoconta.importoFattura)
                                                cell(datoconta.importoLiquidato)
                                                cell(datoconta.differenzaLiquidare)
                                                cell(datoconta.dataPagamentoIntegrato?.format("dd/MM/yyyy"))
                                                cell("non ci sono allegati per questo sinistro")
                                            }
                                            datoconta.discard()
                                        }

                                    }
                                    for (int i = 0; i < 17; i++) {
                                        sheet.autoSizeColumn(i)
                                    }
                                }else{
                                    row{
                                        cell("non ci sono dati contabili")
                                    }
                                }

                            }

                        }*/
                        for (int i = 0; i < 26; i++) {
                            my_sheet.setColumnWidth(i, 30*256)
                        }
                        def stream = new ByteArrayOutputStream()
                        wb.write(stream)
                        stream.close()
                        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        response.addHeader "Content-disposition", "inline; filename=estrazionesinistri_${new Date().format("yyyyMMdd")}.xlsx"
                        response.outputStream << stream.toByteArray()
                    }
                    else{
                        flash.error= "non ci sono sinistri nella data e stati selezionati"
                        redirect action: "estrazionisx"
                    }
                /*}catch (e){
                    response.sendError(404)
                }*/
            }else{
                flash.error ="selezionare un parametro"
                redirect action: "estrazionisx"
            }

        }else{
            flash.error= "selezionare un parametro"
            redirect action: "estrazionisx"
        }
    }
    def estrazioniSinistriTable(){
        def logg
        if(request.post || params)
        {
            def stato
            stato=params.stato1
            def statoArr
            if(stato.toString().trim().contains("1") && stato.toString().trim().contains("2"))
                statoArr=["APERTO", "DEFINITO","SENZA SEGUITO"]
            else if (stato.toString().trim().contains("1") && !stato.toString().trim().contains("2"))
                statoArr=["APERTO"]
            else if(!stato.toString().trim().contains("1") && stato.toString().trim().contains("2"))
                statoArr=["DEFINITO","SENZA SEGUITO"]


            def data1=params.datada ? Date.parse("dd-MM-yyyy", params.datada): null
            def data2=params.dataal ? Date.parse("dd-MM-yyyy", params.dataal) : null

            if(data1 && data2 && stato){
                def fileEstra=Sinistridb.withCriteria {
                    if(stato){
                        inList ( 'stato', statoArr)
                    }
                    if(data1 && data2)
                        between "dataapertura", data1, data2

                    order  "dataapertura","asc"
                }
                try {
                    if(fileEstra){
                        render view: 'estrazionisx', model: [datiEstrazione:fileEstra, datada:params.datada,dataal: params.dataal, stato:params.stato1]

                        /*def wb =new XSSFWorkbook()
                        def createHelper = wb.getCreationHelper()
                        CellStyle hlink_style = wb.createCellStyle()
                        Font hlink_font = wb.createFont()
                        hlink_font.setUnderline(Font.U_SINGLE)
                        hlink_font.setColor(IndexedColors.BLUE.getIndex())
                        hlink_style.setFont(hlink_font)
                        def link = createHelper.createHyperlink(Hyperlink.LINK_URL)
                        def my_sheet = wb.createSheet("Estrazione contabile")
                        def  style = wb.createCellStyle()
                        style.setAlignment(HorizontalAlignment.CENTER)
                        style.setVerticalAlignment(VerticalAlignment.CENTER)
                        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex())
                        style.setFillPattern(FillPatternType.SOLID_FOREGROUND)
                        def row = my_sheet.createRow(0)
                        def cell = row.createCell(0)
                        cell.setCellValue("NOME")
                        cell.setCellStyle(style)
                        cell = row.createCell(1)
                        cell.setCellValue("CODICE FISCALE")
                        cell.setCellStyle(style)
                        cell = row.createCell(2)
                        cell.setCellValue("TARGA")
                        cell.setCellStyle(style)
                        cell = row.createCell(3)
                        cell.setCellValue("NO. SINISTRO")
                        cell.setCellStyle(style)
                        cell = row.createCell(4)
                        cell.setCellValue("NO. POLIZZA")
                        cell.setCellStyle(style)
                        cell = row.createCell(5)
                        cell.setCellValue("RIPARATORE")
                        cell.setCellStyle(style)
                        cell = row.createCell(6)
                        cell.setCellValue("PARTITA IVA RIPARATORE")
                        cell.setCellStyle(style)
                        cell = row.createCell(7)
                        cell.setCellValue("DATA APERTURA")
                        cell.setCellStyle(style)
                        cell = row.createCell(8)
                        cell.setCellValue("DATA SINISTRO")
                        cell.setCellStyle(style)
                        cell = row.createCell(9)
                        cell.setCellValue("DATA DENUNCIA")
                        cell.setCellStyle(style)
                        cell = row.createCell(10)
                        cell.setCellValue("EVENTO")
                        cell.setCellStyle(style)
                        cell = row.createCell(11)
                        cell.setCellValue("DESCRIZIONE")
                        cell.setCellStyle(style)
                        cell = row.createCell(12)
                        cell.setCellValue("DESCRIZIONE2")
                        cell.setCellStyle(style)
                        cell = row.createCell(13)
                        cell.setCellValue("ITER")
                        cell.setCellStyle(style)
                        cell = row.createCell(14)
                        cell.setCellValue("DATA ITER")
                        cell.setCellStyle(style)
                        cell = row.createCell(15)
                        cell.setCellValue("STATO")
                        cell.setCellStyle(style)
                        cell = row.createCell(16)
                        cell.setCellValue("RICHIESTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(17)
                        cell.setCellValue("DATA RICHIESTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(18)
                        cell.setCellValue("OFFERTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(19)
                        cell.setCellValue("DATA OFFERTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(20)
                        cell.setCellValue("LIQUIDAZIONE")
                        cell.setCellStyle(style)
                        cell = row.createCell(21)
                        cell.setCellValue("DATA LIQUIDAZIONE")
                        cell.setCellStyle(style)
                        cell = row.createCell(22)
                        cell.setCellValue("SCOPERTO %")
                        cell.setCellStyle(style)
                        cell = row.createCell(23)
                        cell.setCellValue("FRANCHIGIA")
                        cell.setCellStyle(style)
                        cell = row.createCell(24)
                        cell.setCellValue("NOTA")
                        cell.setCellStyle(style)
                        cell = row.createCell(25)
                        cell.setCellValue("IPERTESTO")
                        cell.setCellStyle(style)
                        if(fileEstra){
                            fileEstra.eachWithIndex { datoconta, idx ->
                                row = my_sheet.createRow(idx+1)
                                cell = row.createCell(0)
                                cell.setCellValue(datoconta.nome.toString().toUpperCase())
                                cell = row.createCell(1)
                                cell.setCellValue(datoconta.codicefiscale.toString().toUpperCase())
                                cell = row.createCell(2)
                                cell.setCellValue(datoconta.targa.toString().toUpperCase())
                                cell = row.createCell(3)
                                cell.setCellValue(datoconta.nosinistro)
                                cell = row.createCell(4)
                                cell.setCellValue(datoconta.nopolizza)
                                cell = row.createCell(5)
                                cell.setCellValue(datoconta.riparatore ? datoconta.riparatore.toString().toUpperCase(): '')
                                cell = row.createCell(6)
                                cell.setCellValue(datoconta.partitaivariparatore ? datoconta.partitaivariparatore.toString().toUpperCase() : '')
                                cell = row.createCell(7)
                                cell.setCellValue(datoconta.dataapertura ? datoconta.dataapertura.format("dd/MM/yyyy"):"")
                                cell = row.createCell(8)
                                cell.setCellValue(datoconta.datasinistro ? datoconta.datasinistro.format("dd/MM/yyyy"):"")
                                cell = row.createCell(9)
                                cell.setCellValue(datoconta.datadenuncia ? datoconta.datadenuncia.format("dd/MM/yyyy"):"")
                                cell = row.createCell(10)
                                cell.setCellValue(datoconta.evento)
                                cell = row.createCell(11)
                                cell.setCellValue(datoconta.descrizione)
                                cell = row.createCell(12)
                                cell.setCellValue(datoconta.descrizione2)
                                cell = row.createCell(13)
                                cell.setCellValue(datoconta.iter)
                                cell = row.createCell(14)
                                cell.setCellValue(datoconta.dataiter ? datoconta.dataiter.format("dd/MM/yyyy"):"")
                                cell = row.createCell(15)
                                cell.setCellValue(datoconta.stato)
                                cell = row.createCell(16)
                                cell.setCellValue(datoconta.richiesta)
                                cell = row.createCell(17)
                                cell.setCellValue(datoconta.datarichiesta ? datoconta.datarichiesta.format("dd/MM/yyyy"):"")
                                cell = row.createCell(18)
                                cell.setCellValue(datoconta.offerta)
                                cell = row.createCell(19)
                                cell.setCellValue(datoconta.dataofferta ? datoconta.dataofferta.format("dd/MM/yyyy"):"")
                                cell = row.createCell(20)
                                cell.setCellValue(datoconta.liquidazione)
                                cell = row.createCell(21)
                                cell.setCellValue(datoconta.dataliquidazione ? datoconta.dataliquidazione.format("dd/MM/yyyy"):"")
                                cell = row.createCell(22)
                                cell.setCellValue(datoconta.scoperto)
                                cell = row.createCell(23)
                                cell.setCellValue(datoconta.franchigia)
                                cell = row.createCell(24)
                                cell.setCellValue(datoconta.nota)
                                cell = row.createCell(25)
                                cell.setCellValue(datoconta.ipertesto)
                                datoconta.discard()
                            }
                        }else{
                            row = my_sheet.createRow(1)
                            cell = row.createCell(0)
                            cell.setCellValue("non ci sono sinistri")
                        }

                        for (int i = 0; i < 26; i++) {
                            my_sheet.setColumnWidth(i, 30*256)
                        }
                        def stream = new ByteArrayOutputStream()
                        wb.write(stream)
                        stream.close()
                        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        response.addHeader "Content-disposition", "inline; filename=estrazionecontabile_${new Date().format("yyyyMMdd")}.xlsx"
                        response.outputStream << stream.toByteArray()*/
                    }
                    else{
                        flash.error= "non ci sono sinistri nella data e stati selezionati"
                        render view: 'estrazionisx', model: [datada:params.datada,dataal: params.dataal, stato:params.stato1]
                    }
                }catch (e){
                    //response.sendError(404)
                    render view: 'estrazionisx', model: [datada:params.datada,dataal: params.dataal, stato:params.stato1]
                }
            }else{

                flash.error ="selezionare un parametro"
                render view: 'estrazionisx', model: [datada:params.datada,dataal: params.dataal, stato:params.stato1]
            }

        }else{

            flash.error= "selezionare un parametro"
            render view: 'estrazionisx'
        }

    }

    def grails() { render view: '/grails' }
}
