package esmobility

import groovy.time.TimeCategory
import groovy.xml.XmlUtil
import sinistri.Log
import wslite.rest.RESTClientException

class IAssicurWebService {

    static transactional = false
    def iAssicurClient
    def grailsApplication

    def query(String sql) throws RESTClientException {
        def logg
        def path = grailsApplication.config.iAssicur.azioni.query.path
        def url = iAssicurClient.url
        iAssicurClient.url = grailsApplication.config.iAssicur.urlProduzione
        println "iAssicur Webservice: ${url}${path} -> ${sql}"
        logg =new Log(parametri: "iAssicur Webservice: ${url}${path} -> ${sql}", operazione: "query IASSCIUR", pagina: "CHIAMATA IASSICUR")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def response = null
        try {
            response = iAssicurClient.get(path: path, query: [SQL: sql])
        } finally {
            iAssicurClient.url = url
        }
        println "Response status: ${response.statusCode} (${response.statusMessage})"
        logg =new Log(parametri: "Response status: ${response.statusCode} (${response.statusMessage})", operazione: "query IASSCIUR", pagina: "CHIAMATA IASSICUR")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return response
    }
    def getDRE(def parametro) {
        def logg
        def select
        def  targaRExp = /^[a-zA-Z]{2}\d{3}[a-zA-Z]{2}$/
        def  polRExp =/^[a-zA-Z]{2}\d{8}[a-zA-Z]{1}$/
        def pIvaRExp=/^[A-Za-z]{6}[0-9]{2}[A-Za-z]{1}[0-9]{2}[A-Za-z]{1}[0-9]{3}[A-Za-z]{1}$/
        def codFisExp=/^[0-9]{11}$/
        def result = [polizze: [], error: null, iAssicurError: null]

        // select rischio from dre where rischio=FF928YY AND COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75)
        // select rischio from dre where numero=EF51600941R AND COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75)
        // select vari campi from sre where dre in (ACDSIT/001,ACDSIT/002,ACDSIT/003)
        // select vari campi from sre where codicerecord=117/1448 AND COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75)
            if(parametro.toString().trim().matches(targaRExp)){
                //select = "select RISCHIO FROM DRE WHERE COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75) AND RISCHIO= ${parametro}"
                /**vecchia query in prod*/
                select = "select RISCHIO FROM DRE WHERE COMPAGNIA IN (A32, A23, A44, A54,A76, A75, A69) AND RAMO IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45,X/43,X/33,X/46,X/32,X/47,X/31,X/34,X/44,D/89,D/59) " +
                        "AND RISCHIO= ${parametro.toString().trim()}"
                /**implementazione 22 maggio su richiesta*/
               /* select = "select RISCHIO FROM DRE WHERE COMPAGNIA IN (A32) AND RAMO IN (L/90,O/10,L/74,W/11,L/74,L/75) AND RISCHIO= ${parametro}"*/
            }else if (parametro.toString().trim().matches(pIvaRExp) || parametro.toString().trim().matches(codFisExp)){
                //select = "select RISCHIO FROM DRE WHERE COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75) AND CODICEFISCALE = ${parametro}"
                /**vecchia query in prod*/
                select = "select RISCHIO FROM DRE WHERE COMPAGNIA IN (A32, A23, A44, A54,A76, A75, A69) AND RAMO IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45,X/43,X/33,X/46,X/32,X/47,X/31,X/34,X/44,D/89,D/59) " +
                        "AND CODICEFISCALE = ${parametro.toString().trim()}"
                /**implementazione 22 maggio su richiesta*/
                /*select = "select RISCHIO FROM DRE WHERE COMPAGNIA IN (A32) AND RAMO IN (L/90,O/10,L/74,W/11,L/74,L/75) AND CODICEFISCALE = ${parametro}"*/
            }else if (parametro.toString().trim().matches(polRExp)){
                //select = "select RISCHIO FROM DRE WHERE COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75) AND NUMERO = ${parametro}"
                /**vecchia query in prod*/
                select = "select RISCHIO FROM DRE WHERE COMPAGNIA IN (A32, A23, A44, A54,A76, A75, A69) AND RAMO IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45,X/43,X/33,X/46,X/32,X/47,X/31,X/34,X/44,D/89,D/59) " +
                        "AND NUMERO = ${parametro.toString().trim()}"
                /**implementazione 22 maggio su richiesta*/
                /*select = "select RISCHIO FROM DRE WHERE COMPAGNIA IN (A32) AND RAMO IN (L/90,O/10,L/74,W/11,L/74,L/75) AND NUMERO = ${parametro}"*/

            }else{
                logg =new Log(parametri: "il parametro non corrisponde a nessuno dei valori permessi controllare  ${parametro.toString().trim()} ", operazione: "queryIASSICURPolizze", pagina: "PAGINA RICERCA SINISTRI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
        if(parametro.toString().trim().matches(targaRExp) || (parametro.toString().trim().matches(pIvaRExp) || parametro.toString().trim().matches(codFisExp)) || parametro.toString().trim().matches(polRExp)){
            println select
            logg =new Log(parametri: "query chiamata IAssicur per Polizze ${select} ", operazione: "queryIASSICURPolizze", pagina: "PAGINA RICERCA SINISTRI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            try {
                def res = query(select)
                if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuna polizza trovata"
                logg =new Log(parametri: "sono stati trovati ${res.xml.Query.Record.size()} polizze", operazione: "queryIASSICURPolizze", pagina: "PAGINA RICERCA SINISTRI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                //println "${res.xml.Query.Record.size()}"
                if (res.xml.Query.Record.Campo.size() > 0) {
                    res.xml.Query.Record.each { record ->
                        def polizza = [:]
                        record.Campo.eachWithIndex { campo, indice ->
                            switch (indice) {
                                case 0: polizza.codicepolizza = campo.text()
                                    break
                            }
                        }
                        result.polizze << polizza
                    }
                }
            } catch(RESTClientException e) {
                if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
                else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
                else result.iAssicurError = e.response.statusMessage
            }
        }

        return result
    }
    def getSRE(def parametro, boolean isSinistro) {
        println parametro
        def logg
        def select
        def  sxRExp = /^\d{3}\/\d{4}$/
        // select rischio from dre where rischio=FF928YY AND COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75)
        // select rischio from dre where numero=EF51600941R AND COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75)
        // select vari campi from sre where dre in (ACDSIT/001,ACDSIT/002,ACDSIT/003)
        // select vari campi from sre where codicerecord=117/1448 AND COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75)

        if( parametro.toString().trim().matches(sxRExp) && isSinistro){
            //select = "select NOME,RISCHIO,NUMERO,RUBRICARIPARA,DATA,DENUNCIA,EVENTO,DESCRIZIONE,DESCRIZIONE2,ITER,DATAITER,STATO,RICHIESTA,DATARICHIESTA,OFFERTA,DATAOFFERTA,LIQUIDAZIONE,DATALIQUIDAZIO,SCOPERTO%,FRANCHIGIA,NOTA,NUOVIDOCUMENTI?,DOCUMENTAZIONE1,DOCUMENTAZIONE2,DOCUMENTAZIONE3,DOCUMENTAZIONE4,DOCUMENTAZIONE5,DOCUMENTAZIONE6,DOCUMENTAZIONE7,DOCUMENTAZIONE8,DOCUMENTAZIONE9,IPERTESTO,DRE:STATO, DRE:SCADENZA, DRE:CODICEFISCALE FROM SRE WHERE COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75)  AND CODICERECORD = ${parametro} for output html"
            /** query in prod*/
            select = "select NOME,RISCHIO,NUMERO,RUBRICARIPARA,DATA,DENUNCIA,EVENTO,DESCRIZIONE,DESCRIZIONE2,ITER,DATAITER,STATO,RICHIESTA,DATARICHIESTA,OFFERTA,DATAOFFERTA," +
                    "LIQUIDAZIONE,DATALIQUIDAZIO,SCOPERTO%,FRANCHIGIA,NOTA,IPERTESTO,DRE:STATO, DRE:SCADENZA, DRE:CODICEFISCALE FROM SRE WHERE DRE:COMPAGNIA IN (A32, A23, A44, A54, A76, A75, A69) " +
                    "AND DRE:RAMO IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45,X/43,X/33,X/46,X/32,X/47,X/31,X/34,X/44,D/89,D/59)  AND CODICERECORD = ${parametro.toString().trim()} for output html"
            /**implementazione 22 maggio su richiesta*/
           /* select = "select NOME,RISCHIO,NUMERO,RUBRICARIPARA,DATA,DENUNCIA,EVENTO,DESCRIZIONE,DESCRIZIONE2,ITER,DATAITER,STATO,RICHIESTA,DATARICHIESTA,OFFERTA,DATAOFFERTA,LIQUIDAZIONE," +
                    "DATALIQUIDAZIO,SCOPERTO%,FRANCHIGIA,NOTA,IPERTESTO,DRE:STATO, DRE:SCADENZA, DRE:CODICEFISCALE FROM SRE WHERE DRE:COMPAGNIA IN (A32) " +
                    "AND DRE:RAMO IN (L/90,O/10,L/74,W/11,L/74,L/75)  AND CODICERECORD = ${parametro} for output html"*/
        }else {
            //select = "select NOME,RISCHIO,NUMERO,RUBRICARIPARA,DATA,DENUNCIA,EVENTO,DESCRIZIONE,DESCRIZIONE2,ITER,DATAITER,STATO,RICHIESTA,DATARICHIESTA,OFFERTA,DATAOFFERTA,LIQUIDAZIONE,DATALIQUIDAZIO,SCOPERTO%,FRANCHIGIA,NOTA,NUOVIDOCUMENTI?,DOCUMENTAZIONE1,DOCUMENTAZIONE2,DOCUMENTAZIONE3,DOCUMENTAZIONE4,DOCUMENTAZIONE5,DOCUMENTAZIONE6,DOCUMENTAZIONE7,DOCUMENTAZIONE8,DOCUMENTAZIONE9,IPERTESTO, DRE:STATO, DRE:SCADENZA, DRE:CODICEFISCALE FROM SRE WHERE COMPAGNIA=A32 AND RAMO IN (L/90,O/10,L/74,W/11,L/75) AND DRE IN (${parametro}) for output html"
            /** query in prod*/
            select = "select NOME,RISCHIO,NUMERO,RUBRICARIPARA,DATA,DENUNCIA,EVENTO,DESCRIZIONE,DESCRIZIONE2,ITER,DATAITER,STATO,RICHIESTA,DATARICHIESTA,OFFERTA,DATAOFFERTA,LIQUIDAZIONE," +
                    "DATALIQUIDAZIO,SCOPERTO%,FRANCHIGIA,NOTA,IPERTESTO, DRE:STATO, DRE:SCADENZA, DRE:CODICEFISCALE FROM SRE WHERE DRE:COMPAGNIA IN (A32, A23, A44, A54,A76, A75, A69) AND DRE:RAMO " +
                    "IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45,X/43,X/33,X/46,X/32,X/47,X/31,X/34,X/44,D/89,D/59) AND DRE IN (${parametro.toString().trim()}) for output html"
            /**implementazione 22 maggio su richiesta*/
           /* select = "select NOME,RISCHIO,NUMERO,RUBRICARIPARA,DATA,DENUNCIA,EVENTO,DESCRIZIONE,DESCRIZIONE2,ITER,DATAITER,STATO,RICHIESTA,DATARICHIESTA,OFFERTA,DATAOFFERTA,LIQUIDAZIONE," +
                    "DATALIQUIDAZIO,SCOPERTO%,FRANCHIGIA,NOTA,IPERTESTO, DRE:STATO, DRE:SCADENZA, DRE:CODICEFISCALE FROM SRE WHERE DRE:COMPAGNIA IN (A32) " +
                    "AND DRE:RAMO IN (L/90,O/10,L/74,W/11,L/74,L/75) AND DRE IN (${parametro}) for output html"*/

        }
        println select
        logg =new Log(parametri: "query chiamata IAssicur per Sinistri ${select} ", operazione: "queryIASSICURSinistri", pagina: "JOB CARICA SINISTRI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [sinistri: [], error: null, iAssicurError: null]
        try {
            def res = query(select)
            //println res
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuno sinistro trovato"
            logg =new Log(parametri: "sono stati trovati ${res.xml.Query.Record.size()} sinistri", operazione: "queryIASSICURSinistri", pagina: "JOB CARICA SINISTRI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            //println "${res.xml.Query.Record.size()}"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def sinistro = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: sinistro.numerosinistro = campo.text()
                                break
                            case 1: def nome= campo.text()
                                sinistro.nome = nome.toString().trim().replaceAll(","," ").replaceAll(":", " ")
                                break
                            case 2: sinistro.targa = campo.text()
                                break
                            case 3: sinistro.polizza = campo.text()
                                break
                            case 4: sinistro.rubricaripara = campo.text()
                                break
                            case 5: sinistro.datasinistro = campo.text()
                                break
                            case 6: sinistro.dataricevimentodenuncia = campo.text()
                                break
                            case 7: def evento = campo.text()
                                def index1 = evento.lastIndexOf("(")
                                //def index2 = evento.lastIndexOf(")")
                                evento = evento[0..<index1]
                                evento=evento.toString().trim().replaceAll("\\*","").replaceAll("\\+","").replaceAll(","," ").replaceAll(":", " ")
                                sinistro.evento = evento.toString().trim()
                                break
                            case 8: def descrizione=campo.text()
                                sinistro.descrizione = descrizione.toString().trim().replaceAll(","," ").replaceAll(":", " ")
                                break
                            case 9: def descrizione2= campo.text()
                                sinistro.descrizione2 = descrizione2.toString().trim().replaceAll(","," ").replaceAll(":", " ")
                                break
                            case 10: def iter = campo.text()
                                def index1 = iter.lastIndexOf("(")
                                //def index2 = iter.lastIndexOf(")")
                                iter = iter[0..<index1]
                                iter=iter.toString().trim().replaceAll("\\*","").replaceAll("\\+","")
                                sinistro.iter = iter.toString().trim()
                                break
                            case 11: sinistro.dataIter  = campo.text()
                                break
                            case 12: def stato = campo.text()
                                def index1 = stato.lastIndexOf("(")
                                //def index2 = stato.lastIndexOf(")")
                                stato = stato[0..<index1]
                                sinistro.stato = stato.toString().trim()
                                break
                            case 13: def preventivo = campo.text()
                                preventivo=preventivo.toString().trim().replaceAll("/.","")
                                sinistro.preventivo  = preventivo
                                break
                            case 14: sinistro.datapreventivo  = campo.text()
                                break
                            case 15: def concordato = campo.text()
                                concordato=concordato.toString().trim().replaceAll("/.","")
                                sinistro.concordato  = concordato
                                break
                            case 16: sinistro.dataconcordato  = campo.text()
                                break
                            case 17: def pagato= campo.text()
                                pagato=pagato.toString().trim().replaceAll("/.","")
                                sinistro.pagato  = pagato
                                break
                            case 18: sinistro.datapagamento  = campo.text()
                                break
                            case 19: def scoperto= campo.text()
                                scoperto=scoperto.toString().trim().replaceAll("/.","")
                                sinistro.scoperto  = scoperto
                                break
                            case 20: def franchigia= campo.text()
                                franchigia=franchigia.toString().trim().replaceAll("/.","")
                                sinistro.franchigia  = franchigia
                                break
                            case 21: def degrado= campo.text()
                                degrado=degrado.toString().trim().replaceAll("/.","")
                                sinistro.degrado  = degrado
                                break
                            case 22: def note= campo.text()
                                sinistro.note  = note.toString().replaceAll("<br>","").replaceAll("<BR>","").trim().replaceAll(",",";").replaceAll(":", "-->")
                                break
                            case 23: def stato = campo.text()
                                def index1 = stato.lastIndexOf("(")
                                stato = stato[0..<index1]
                                sinistro.statodre  = stato.toString().trim()
                                break
                            case 24: sinistro.scadenzadre  = campo.text()
                                break
                            case 25: sinistro.codFiscale  = campo.text()
                                break
                        }
                    }
                    result.sinistri << sinistro
                }
            }
        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }
        return result
    }
    def getSRESett() {
        def logg
        def select
        def dataOdierna=new Date().format("dd.MM.yyyy")
        def datasett= use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -7.day}.format("dd.MM.yyyy")
        def settimana=use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -7.day}.format("dd_MM_yyyy")
        /** query in prod*/
        select ="select NOME, RISCHIO as TARGA, CODICERECORD as NOSINISTRO, NUMERO as NO_POLIZZA, DRE:CODICEFISCALE as CODICE_FISCALE, RUBRICARIPARA as riparatore, " +
                "RUBRICARIPARA:PARTITAIVA as PARTITA_IVA_RIPARATORE, DATAAPERTURA as DATA_APERTURA,DATA as DATA_SINISTRO, DENUNCIA as DATA_DENUNCIA, EVENTO, DESCRIZIONE, " +
                "DESCRIZIONE2, ITER, DATAITER, STATO, RICHIESTA, DATARICHIESTA, OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA, IPERTESTO, " +
                "DRE:COMPAGNIA, DRE:RAMO FROM SRE WHERE DRE:COMPAGNIA IN (A32, A23, A44, A54, A76, A75, A69 ) AND DRE:RAMO IN  (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45,X/43,X/33,X/46,X/32,X/47,X/31,X/34,X/44,D/89,D/59) " +
                "AND ITER IN (198, 186,233,163,178,179,180,197,164,172,173,174,166,175,176,177,259,260,237,238) AND DENUNCIA <= ${datasett} AND STATO = 1  for output html"

        /*"select NOME, RISCHIO, NUMERO, DRE:CODICEFISCALE, RUBRICARIPARA, RUBRICARIPARA:PARTITAIVA, DATA, DENUNCIA, EVENTO, DESCRIZIONE, DESCRIZIONE2, ITER, DATAITER, " +
                "STATO, RICHIESTA, DATARICHIESTA, " +
                "OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA,  IPERTESTO FROM SRE WHERE DRE:COMPAGNIA IN (A32, A23, A44, A54) AND DRE:RAMO IN " +
                "(O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45) AND ITER IN (198, 186,233,163,178,179,180,197,164,172,173,174,166,175,176,177,259,260,237,238) " +
                "AND DENUNCIA <= ${datasett} AND STATO = 1  for output html"*/
        /**implementazione 22 maggio su richiesta*/
        /*select = "select NOME, RISCHIO, NUMERO, DRE:CODICEFISCALE, RUBRICARIPARA,  RUBRICARIPARA:PARTITAIVA, DATA, DENUNCIA, EVENTO, DESCRIZIONE, DESCRIZIONE2, ITER, DATAITER, STATO, RICHIESTA, DATARICHIESTA, " +
                "OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA, IPERTESTO FROM SRE WHERE DRE:COMPAGNIA IN (A32) " +
                "AND DRE:RAMO IN (L/90,O/10,L/74,W/11,L/74,L/75) AND ITER IN (198, 186,233,163,178,179,180,197,164,172,173,174,166,175,176,177,259,260,237,238) AND DENUNCIA <= ${datasett} " +
                "AND STATO = 1  for output html"*/
        //select = "select NOME, RISCHIO, NUMERO, DRE:CODICEFISCALE, RUBRICARIPARA, DATA, DENUNCIA, EVENTO, DESCRIZIONE, DESCRIZIONE2, ITER, DATAITER, STATO, RICHIESTA, DATARICHIESTA,
        // OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA, NUOVIDOCUMENTI?, DOCUMENTAZIONE1, DOCUMENTAZIONE2, DOCUMENTAZIONE3, DOCUMENTAZIONE4,
        // DOCUMENTAZIONE5, DOCUMENTAZIONE6, DOCUMENTAZIONE7, DOCUMENTAZIONE8, DOCUMENTAZIONE9, IPERTESTO FROM SRE WHERE DRE:COMPAGNIA=A32 and DRE:RAMO IN (L/90,O/10,L/74,W/11,L/74,L/75)
        // AND ITER IN (198, 186,233,163,178,179,180,197,164,172,173,174,166,175,176,177,259,260,237,238) AND DENUNCIA = 07.02.2017 AND STATO = 1  for output html"

        println select
        logg =new Log(parametri: "query chiamata IAssicur per Sinistri ${select} ", operazione: "queryIASSICURSinistri", pagina: "JOB CARICA SINISTRI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [sinistri: [], error: null, iAssicurError: null, settimana:null]
        try {
            def res = query(select)
            //println res
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuno sinistro trovato"
            logg =new Log(parametri: "sono stati trovati ${res.xml.Query.Record.size()} sinistri", operazione: "queryIASSICURSinistri", pagina: "JOB CARICA SINISTRI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            //println "numero di ${res.xml.Query.Record.size()}"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def sinistro = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: sinistro.noSinistro = campo.text()
                                break
                            case 1: sinistro.nome = campo.text()
                                break
                            case 2: sinistro.targa = campo.text()
                                break
                            case 4: sinistro.numero = campo.text()
                                break
                            case 5: sinistro.codFiscale  = campo.text()
                                break
                            case 6: sinistro.rubricaripara = campo.text()
                                break
                            case 7: sinistro.partitaivariparatore= campo.text()
                                break
                            case 8: sinistro.dataapertura = campo.text()
                                break
                            case 9: sinistro.data = campo.text()
                                break
                            case 10: sinistro.denuncia = campo.text()
                                break
                            case 11: def evento = campo.text()
                                if(evento.lastIndexOf("(")){
                                    def index1 = evento.lastIndexOf("(")
                                    //def index2 = evento.lastIndexOf(")")
                                    evento = evento[0..<index1]
                                    evento=evento.toString().trim().replaceAll("\\*","").replaceAll("\\+","")
                                    sinistro.evento = evento.toString().trim()
                                }else{
                                    sinistro.evento = evento
                                }
                                break
                            case 12: sinistro.descrizione = campo.text()
                                break
                            case 13: sinistro.descrizione2 = campo.text()
                                break
                            case 14: def iter = campo.text()
                                if(iter.lastIndexOf("(")){
                                    def index1 = iter.lastIndexOf("(")
                                    //def index2 = iter.lastIndexOf(")")
                                    iter = iter[0..<index1]
                                    iter=iter.toString().trim().replaceAll("\\*","").replaceAll("\\+","")
                                    sinistro.iter = iter.toString().trim()
                                }else{
                                    sinistro.iter = iter
                                }
                                break
                            case 15: sinistro.dataIter  = campo.text()
                                break
                            case 16: def stato = campo.text()
                                if(stato.lastIndexOf("(")){
                                    def index1 = stato.lastIndexOf("(")
                                    //def index2 = stato.lastIndexOf(")")
                                    stato = stato[0..<index1]
                                    sinistro.stato = stato.toString().trim()
                                }else{
                                    sinistro.stato = stato
                                }

                                break
                            case 17: sinistro.richiesta  = campo.text()
                                break
                            case 18: sinistro.dataRichiesta  = campo.text()
                                break
                            case 19: sinistro.offerta  = campo.text()
                                break
                            case 20: sinistro.dataOfferta  = campo.text()
                                break
                            case 21: def liquidazione = campo.text()
                                liquidazione=liquidazione.toString().trim().replaceAll("/.","")
                                sinistro.liquidazione  = liquidazione
                                break
                            case 22: sinistro.dataLiquidazione  = campo.text()
                                break
                            case 23: def scoperto= campo.text()
                                scoperto=scoperto.toString().trim().replaceAll("/.","")
                                sinistro.scoperto  = scoperto
                                break
                            case 24: def franchigia= campo.text()
                                franchigia=franchigia.toString().trim().replaceAll("/.","")
                                sinistro.franchigia  = franchigia
                                break
                            case 25: def degrado= campo.text()
                                degrado=degrado.toString().trim().replaceAll("/.","")
                                sinistro.degrado  = degrado
                                break
                            case 26: def note= campo.text()
                                sinistro.note  = note.toString().replaceAll("<br>","").replaceAll("<BR>","").trim()
                                break
                            case 27: def compagnia = campo.text()
                                //println "compagnia-->$compagnia"

                                def index1 = compagnia.lastIndexOf("(")
                                def index2 = compagnia.lastIndexOf(")")
                                compagnia = compagnia[index1+1..<index2]
                                sinistro.compagnia = compagnia
                                /*logg =new Log(parametri: "${sinistro.noSinistro} --> compagnia-->$compagnia", operazione: "query IASSICUR sinistri da salvare a DB", pagina: "JOB CARICA SINISIRI")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"*/
                                break
                            case 28: def ramo = campo.text()
                                def index1 = ramo.lastIndexOf("(")
                                def index2 = ramo.lastIndexOf(")")
                                ramo = ramo[index1+1..<index2]
                                sinistro.ramo = ramo
                                break
                        }
                    }
                    result.sinistri << sinistro
                }
            }
            result.settimana=settimana
        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }
        return result
    }
    def getSREAperti() {
        def logg
        def select
        def dataOdierna=new Date().format("dd.MM.yyyy")
        def datasett= use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -7.day}.format("dd.MM.yyyy")
        def settimana=use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -7.day}.format("dd_MM_yyyy")
        select = "select NOME, RISCHIO, NUMERO, DRE:CODICEFISCALE, RUBRICARIPARA, DATA, DENUNCIA, EVENTO, DESCRIZIONE, DESCRIZIONE2, ITER, DATAITER, STATO, RICHIESTA, DATARICHIESTA, " +
                "OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA,  IPERTESTO FROM SRE WHERE DRE:COMPAGNIA in (A32,A44,A54, A76, A75, A69) and DRE:RAMO " +
                "IN (O/95,K/35,L/74,W/11,O/30,K/45,X/43,X/33,X/46,X/32,X/47,X/31,X/34,X/44,D/89,D/59) AND evento IN (203,204,205,206,207) AND STATO = 1 for output html"
        //select = "select NOME, RISCHIO, NUMERO, DRE:CODICEFISCALE, RUBRICARIPARA, DATA, DENUNCIA, EVENTO, DESCRIZIONE, DESCRIZIONE2, ITER, DATAITER, STATO, RICHIESTA, DATARICHIESTA, OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA,  IPERTESTO FROM SRE WHERE DRE:COMPAGNIA=A32 and DRE:RAMO IN (L/90,O/10,L/74,W/11,L/74,L/75) AND ITER IN (198, 186,233,163,178,179,180,197,164,172,173,174,166,175,176,177,259,260,237,238) AND DENUNCIA <= ${datasett} AND STATO = 1  for output html"
        //select = "select NOME, RISCHIO, NUMERO, DRE:CODICEFISCALE, RUBRICARIPARA, DATA, DENUNCIA, EVENTO, DESCRIZIONE, DESCRIZIONE2, ITER, DATAITER, STATO, RICHIESTA, DATARICHIESTA, OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA, NUOVIDOCUMENTI?, DOCUMENTAZIONE1, DOCUMENTAZIONE2, DOCUMENTAZIONE3, DOCUMENTAZIONE4, DOCUMENTAZIONE5, DOCUMENTAZIONE6, DOCUMENTAZIONE7, DOCUMENTAZIONE8, DOCUMENTAZIONE9, IPERTESTO FROM SRE WHERE DRE:COMPAGNIA=A32 and DRE:RAMO IN (L/90,O/10,L/74,W/11,L/74,L/75) AND ITER IN (198, 186,233,163,178,179,180,197,164,172,173,174,166,175,176,177,259,260,237,238) AND DENUNCIA = 07.02.2017 AND STATO = 1  for output html"

        println select
        logg =new Log(parametri: "query chiamata IAssicur per Sinistri ${select} ", operazione: "queryIASSICURSinistri", pagina: "JOB CARICA SINISTRI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [sinistri: [], error: null, iAssicurError: null, settimana:null]
        try {
            def res = query(select)
            //println res
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuno sinistro trovato"
            logg =new Log(parametri: "sono stati trovati ${res.xml.Query.Record.size()} sinistri", operazione: "queryIASSICURSinistri", pagina: "JOB CARICA SINISTRI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            //println "numero di ${res.xml.Query.Record.size()}"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def sinistro = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: sinistro.noSinistro = campo.text()
                                break
                            case 1: sinistro.nome = campo.text()
                                break
                            case 2: sinistro.targa = campo.text()
                                break
                            case 3: sinistro.numero = campo.text()
                                break
                            case 4: sinistro.codFiscale  = campo.text()
                                break
                            case 5: sinistro.rubricaripara = campo.text()
                                break
                            case 6: sinistro.data = campo.text()
                                break
                            case 7: sinistro.denuncia = campo.text()
                                break
                            case 8: def evento = campo.text()
                                def index1 = evento.lastIndexOf("(")
                                //def index2 = evento.lastIndexOf(")")
                                evento = evento[0..<index1]
                                evento=evento.toString().trim().replaceAll("\\*","").replaceAll("\\+","")
                                sinistro.evento = evento.toString().trim()
                                break
                            case 9: sinistro.descrizione = campo.text()
                                break
                            case 10: sinistro.descrizione2 = campo.text()
                                break
                            case 11: def iter = campo.text()
                                def index1 = iter.lastIndexOf("(")
                                //def index2 = iter.lastIndexOf(")")
                                iter = iter[0..<index1]
                                iter=iter.toString().trim().replaceAll("\\*","").replaceAll("\\+","")
                                sinistro.iter = iter.toString().trim()
                                break
                            case 12: sinistro.dataIter  = campo.text()
                                break
                            case 13: def stato = campo.text()
                                def index1 = stato.lastIndexOf("(")
                                //def index2 = stato.lastIndexOf(")")
                                stato = stato[0..<index1]
                                sinistro.stato = stato.toString().trim()
                                break
                            case 14: sinistro.richiesta  = campo.text()
                                break
                            case 15: sinistro.dataRichiesta  = campo.text()
                                break
                            case 16: sinistro.offerta  = campo.text()
                                break
                            case 17: sinistro.dataOfferta  = campo.text()
                                break
                            case 18: def liquidazione = campo.text()
                                liquidazione=liquidazione.toString().trim().replaceAll("/.","")
                                sinistro.liquidazione  = liquidazione
                                break
                            case 19: sinistro.dataLiquidazione  = campo.text()
                                break
                            case 20: def scoperto= campo.text()
                                scoperto=scoperto.toString().trim().replaceAll("/.","")
                                sinistro.scoperto  = scoperto
                                break
                            case 21: def franchigia= campo.text()
                                franchigia=franchigia.toString().trim().replaceAll("/.","")
                                sinistro.franchigia  = franchigia
                                break
                            case 22: def degrado= campo.text()
                                degrado=degrado.toString().trim().replaceAll("/.","")
                                sinistro.degrado  = degrado
                                break
                            case 23: def note= campo.text()
                                sinistro.note  = note.toString().replaceAll("<br>","").replaceAll("<BR>","").trim()
                                break
                        }
                    }
                    result.sinistri << sinistro
                }
            }
            result.settimana=settimana
        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }
        return result
    }
    def getSRETot() {
        def logg
        def select
        select ="select NOME, RISCHIO as TARGA, CODICERECORD as NOSINISTRO, NUMERO as NO_POLIZZA, DRE:CODICEFISCALE as CODICE_FISCALE, RUBRICARIPARA as riparatore, " +
                "RUBRICARIPARA:PARTITAIVA as PARTITA_IVA_RIPARATORE, DATAAPERTURA as DATA_APERTURA,DATA as DATA_SINISTRO, DENUNCIA as DATA_DENUNCIA, EVENTO, DESCRIZIONE, DESCRIZIONE2, ITER, " +
                "DATAITER, STATO, RICHIESTA, DATARICHIESTA, OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA, IPERTESTO, DRE:COMPAGNIA, DRE:RAMO " +
                "FROM SRE WHERE DRE:COMPAGNIA IN (A32, A23, A44, A54, A76, A75, A69) and DRE:RAMO IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45,X/43,X/33,X/46,X/32,X/47,X/31,X/34,X/44,D/89,D/59)  for output html"

        /* "select NOME, RISCHIO, NUMERO, DRE:CODICEFISCALE, RUBRICARIPARA, DATA, DENUNCIA, EVENTO, DESCRIZIONE, DESCRIZIONE2, ITER, DATAITER, STATO, RICHIESTA, " +
                "DATARICHIESTA, OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA, IPERTESTO FROM SRE WHERE DRE:COMPAGNIA IN (A32, ​A23, A44, A54)" +
                " and DRE:RAMO IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45)  for output html"
        */

        println select
        logg =new Log(parametri: "query chiamata IAssicur per Sinistri ${select} ", operazione: "queryIASSICURSinistri", pagina: "JOB CARICA SINISTRI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [sinistri: [], error: null, iAssicurError: null]
        try {
            def res = query(select)
            //println res
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuno sinistro trovato"
            logg =new Log(parametri: "sono stati trovati ${res.xml.Query.Record.size()} sinistri", operazione: "queryIASSICURSinistri", pagina: "JOB CARICA SINISTRI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            //println "${res.xml.Query.Record.size()}"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def sinistro = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: sinistro.noSinistro = campo.text()
                                break
                            case 1: sinistro.nome = campo.text()
                                break
                            case 2: sinistro.targa = campo.text()
                                break
                            case 4: sinistro.numero = campo.text()
                                break
                            case 5: sinistro.codFiscale  = campo.text()
                                break
                            case 6: sinistro.rubricaripara = campo.text()
                                break
                            case 7: sinistro.partitaivariparatore = campo.text()
                                break
                            case 8: sinistro.dataapertura = campo.text()
                                break
                            case 9: sinistro.data = campo.text()
                                break
                            case 10: sinistro.denuncia = campo.text()
                                break
                            case 11: def evento = campo.text()
                                def index1 = evento.lastIndexOf("(")
                                //def index2 = evento.lastIndexOf(")")
                                evento = evento[0..<index1]
                                evento=evento.toString().trim().replaceAll("\\*","").replaceAll("\\+","")
                                sinistro.evento = evento.toString().trim()
                                break
                            case 12: sinistro.descrizione = campo.text()
                                break
                            case 13: sinistro.descrizione2 = campo.text()
                                break
                            case 14: def iter = campo.text()
                                def index1 = iter.lastIndexOf("(")
                                //def index2 = iter.lastIndexOf(")")
                                iter = iter[0..<index1]
                                iter=iter.toString().trim().replaceAll("\\*","").replaceAll("\\+","")
                                sinistro.iter = iter.toString().trim()
                                break
                            case 15: sinistro.dataIter  = campo.text()
                                break
                            case 16: def stato = campo.text()
                                def index1 = stato.lastIndexOf("(")
                                //def index2 = stato.lastIndexOf(")")
                                stato = stato[0..<index1]
                                sinistro.stato = stato.toString().trim()
                                break
                            case 17: sinistro.richiesta  = campo.text()
                                break
                            case 18: sinistro.dataRichiesta  = campo.text()
                                break
                            case 19: sinistro.offerta  = campo.text()
                                break
                            case 20: sinistro.dataOfferta  = campo.text()
                                break
                            case 21: def liquidazione = campo.text()
                                liquidazione=liquidazione.toString().trim().replaceAll("/.","")
                                sinistro.liquidazione  = liquidazione
                                break
                            case 22: sinistro.dataLiquidazione  = campo.text()
                                break
                            case 23: def scoperto= campo.text()
                                scoperto=scoperto.toString().trim().replaceAll("/.","")
                                sinistro.scoperto  = scoperto
                                break
                            case 24: def franchigia= campo.text()
                                franchigia=franchigia.toString().trim().replaceAll("/.","")
                                sinistro.franchigia  = franchigia
                                break
                            case 25: def degrado= campo.text()
                                degrado=degrado.toString().trim().replaceAll("/.","")
                                sinistro.degrado  = degrado
                                break
                            case 26: def note= campo.text()
                                sinistro.note  = note.toString().replaceAll("<br>","").replaceAll("<BR>","").trim()
                                break
                            case 27: def compagnia = campo.text()
                                //println "compagnia-->$compagnia"

                                def index1 = compagnia.lastIndexOf("(")
                                def index2 = compagnia.lastIndexOf(")")
                                compagnia = compagnia[index1+1..<index2]
                                sinistro.compagnia = compagnia
                                /*logg =new Log(parametri: "${sinistro.noSinistro} --> compagnia-->$compagnia", operazione: "query IASSICUR sinistri da salvare a DB", pagina: "JOB CARICA SINISIRI")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"*/
                                break
                            case 28: def ramo = campo.text()
                                def index1 = ramo.lastIndexOf("(")
                                def index2 = ramo.lastIndexOf(")")
                                ramo = ramo[index1+1..<index2]
                                sinistro.ramo = ramo
                                break
                        }
                    }
                    result.sinistri << sinistro
                }
            }
        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }
        return result
    }
    def getSRENott() {
        def logg
        def select
        def dataOdierna=new Date().format("dd.MM.yyyy")
        //vecchia query fino a 02/11/2017
        /*select = "select NOME,RISCHIO as TARGA,codicerecord as NO.SINISTRO, DRE:CODICEFISCALE as CODICE_FISCALE, RUBRICARIPARA:nome as RIPARATORE,DATA as DATA_SINISTRO,DENUNCIA as"+
                " DATA_DENUNCIA," +
                "EVENTO:nome as EVENTO,DESCRIZIONE,DESCRIZIONE2,STATO:nome as STATO FROM SRE WHERE DRE:COMPAGNIA IN (A32, A23, A44, A54) AND DRE:RAMO IN "+
                "(O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74," +
                "W/11,L/26,O/29,O/30,K/44,K/45) and denuncia = ${dataOdierna} for output html"*/

        select="select NOME as not used, RISCHIO as asset_ref, codicerecord as case_reference,RUBRICARIPARA:nome as repair_location, DATA as damage_date,DENUNCIA as declaration_date," +
                "EVENTO:nome as damage_type, DESCRIZIONE, DESCRIZIONE2 as damage_detail, STATO:nome as coverage,DRE:CODICEFISCALE as code_fiscal, ITER:nome as case_status_details," +
                " DATAITER as case_status_update_date,NUMERO as customer_id FROM SRE WHERE DRE:COMPAGNIA IN (A32) AND DRE:RAMO IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26, "+
                "O/29,O/30,K/44,K/45) " +
                "and dataiter=${dataOdierna} and dataapertura>=01/01/2017 for output html"
               // "and dataiter inrange 01.01.2017:15.11.2017 and dataapertura>=01/01/2017 for output html"
        /*select = "select NOME,RISCHIO as TARGA,codicerecord as NO.SINISTRO, DRE:CODICEFISCALE as CODICE_FISCALE, RUBRICARIPARA:nome as RIPARATORE,DATA as DATA_SINISTRO,DENUNCIA as DATA_DENUNCIA," +
                "EVENTO:nome as EVENTO,DESCRIZIONE,DESCRIZIONE2,STATO:nome as STATO FROM SRE WHERE DRE:COMPAGNIA IN (A32, A23, A44, A54) AND " +
                "DRE:RAMO IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45) and denuncia inrange 01.01.2017:21.09.2017 for output html"*/
        println select
        logg =new Log(parametri: "query chiamata IAssicur per Sinistri ${select} ", operazione: "queryIASSICURSinistri NOTTURNI", pagina: "JOB CARICA SINISTRI NOTTURNI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [sinistri: [], error: null, iAssicurError: null]
        try {
            def res = query(select)
            //println res
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuno sinistro trovato"
            logg =new Log(parametri: "sono stati trovati ${res.xml.Query.Record.size()} sinistri", operazione: "queryIASSICURSinistri NOTTURNI", pagina: "JOB CARICA SINISTRI NOTTURNI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            //println "${res.xml.Query.Record.size()}"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def sinistro = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: sinistro.noSinistro = campo.text()
                                break
                            case 1: sinistro.nome = campo.text()
                                break
                            case 2: sinistro.targa = campo.text()
                                break
                            /*case 4: sinistro.codFiscale  = campo.text()
                                break*/
                            case 4: sinistro.rubricaripara = campo.text()
                                break
                            case 5: sinistro.data = campo.text()
                                break
                            case 6: sinistro.denuncia = campo.text()
                                break
                            case 7: def evento = campo.text()
                                evento=evento.toString().trim().replaceAll("\\*","").replaceAll("\\+","")
                                sinistro.evento = evento.toString().trim()
                                break
                            case 8: sinistro.descrizione = campo.text()
                                break
                            case 9: sinistro.descrizione2 = campo.text()
                                break
                            case 10: def stato = campo.text()
                                sinistro.stato = stato.toString().trim()
                                break
                            case 11: sinistro.codFiscale  = campo.text()
                                break
                            case 12: sinistro.iter  = campo.text()
                                break
                            case 13: sinistro.dataiter = campo.text()
                                break
                            case 14: def nopolizza= campo.text()
                                if(nopolizza.length()>10){
                                    nopolizza= nopolizza.replace(nopolizza.substring(nopolizza.length()-1), "")
                                }
                                sinistro.numero = nopolizza
                                break
                        }
                    }
                    result.sinistri << sinistro
                }
            }
        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }
        return result
    }
    def getDatiCont() {
        def logg
        def select
        def dataOdierna=new Date().format("dd.MM.yyyy")
        /*select = "select dre:ramo, compagnia, codicerecord as No Sinistro, nome as intestatario polizza, liquidazione as importo liquidato, rubricaripara:nome as intestatario conto, " +
                "dataliquidazio as data pagamento from sre where stato = 2 and dataliquidazio =  ${dataOdierna} and compagnia in (a32,a44,a54) " +
                "and ramo in (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45) for output html"*/
        select = "select dre:ramo, compagnia, codicerecord as No Sinistro, nome as intestatario polizza, liquidazione as importo liquidato, rubricaripara:nome as intestatario conto, " +
                "dataliquidazio as data pagamento from sre where stato = 2 and dataliquidazio  =  ${dataOdierna}  and compagnia in (A32,A44,A54,A23,A76, A75, A69) " +
                "and ramo in (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45,X/43,X/33,X/46,X/32,X/47,X/31,X/34,X/44,D/89,D/59) for output html"
                            //O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45
        println select
        logg =new Log(parametri: "query chiamata IAssicur per DatiContabili ${select} ", operazione: "query IASSICUR DATI CONTABILI", pagina: "JOB CARICA DATICONTABILI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [sinistri: [], error: null, iAssicurError: null]
        try {
            def res = query(select)
            //println res
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuno sinistro trovato"
            logg =new Log(parametri: "sono stati trovati ${res.xml.Query.Record.size()} sinistri", operazione: "query IASSICUR DATI CONTABILI", pagina: "JOB CARICA DATICONTABILI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            //println "${res.xml.Query.Record.size()}"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def sinistro = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: sinistro.noSinistro = campo.text()
                                break
                            case 1: def ramo = campo.text()
                                def index1 = ramo.lastIndexOf("(")
                                def index2 = ramo.lastIndexOf(")")
                                ramo = ramo[index1+1..<index2]
                                sinistro.ramo = ramo
                                break
                            case 2: sinistro.compagnia = campo.text()
                                break
                            case 4: sinistro.intestatariopolizza = campo.text()
                                break
                            case 5: sinistro.importoliquidato = campo.text()
                                break
                            case 6: sinistro.intestatarioconto = campo.text()
                                break
                            case 7: sinistro.datapagamento = campo.text()
                                break
                        }
                    }
                    result.sinistri << sinistro
                }
            }
        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }
        return result
    }
    def getDatiSxIassicur() {
        def logg
        def select
        def dataOdierna=new Date().format("dd.MM.yyyy")
        //select = "select dre:ramo, compagnia, codicerecord as No Sinistro, nome as intestatario polizza, liquidazione as importo liquidato, rubricaripara:nome as intestatario conto, dataliquidazio as data pagamento from sre where stato = 2 and dataliquidazio = 05.07.2017 and compagnia in (a32,a44,a54) and ramo in (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45) for output html"
        /*"select NOME, RISCHIO as TARGA, CODICERECORD as NOSINISTRO, NUMERO as NO_POLIZZA, DRE:CODICEFISCALE as CODICE_FISCALE, RUBRICARIPARA as riparatore," +
                "RUBRICARIPARA:PARTITAIVA as PARTITA_IVA_RIPARATORE, DATAAPERTURA as DATA_APERTURA,DATA as DATA_SINISTRO, DENUNCIA as DATA_DENUNCIA, EVENTO, DESCRIZIONE, DESCRIZIONE2, " +
                "ITER, DATAITER, STATO, RICHIESTA, DATARICHIESTA, OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA, IPERTESTO, DRE:COMPAGNIA, DRE:RAMO FROM SRE" +
                " WHERE DRE:COMPAGNIA IN (a32,a44,a54,a23) and DRE:RAMO IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45) and STATO in (1,2,3) " +
                "and dataapertura = ${dataOdierna} for output html"
                //"and dataapertura inrange 01/01/2017:31/03/2017 for output html"
                */
        select ="select NOME, RISCHIO as TARGA, CODICERECORD as NOSINISTRO, NUMERO as NO_POLIZZA, DRE:CODICEFISCALE as CODICE_FISCALE, RUBRICARIPARA as riparatore, " +
                "RUBRICARIPARA:PARTITAIVA as PARTITA_IVA_RIPARATORE, DATAAPERTURA as DATA_APERTURA,DATA as DATA_SINISTRO, DENUNCIA as DATA_DENUNCIA, EVENTO, DESCRIZIONE, DESCRIZIONE2, " +
                "ITER, DATAITER, STATO, RICHIESTA, DATARICHIESTA, OFFERTA, DATAOFFERTA, LIQUIDAZIONE, DATALIQUIDAZIO, SCOPERTO%, FRANCHIGIA,NOTA, IPERTESTO, DRE:COMPAGNIA, DRE:RAMO " +
                "FROM SRE WHERE DRE:COMPAGNIA IN (a32,a44,a54,a23,A76, A75, A69) and DRE:RAMO IN (O/94,O/95,K/36,K/35,L/75,L/90,O/10,L/74,W/11,L/26,O/29,O/30,K/44,K/45,X/43,X/33,X/46,X/32,X/47,X/31,X/34,X/44,D/89,D/59) and STATO in (1,2,3)  " +
                "and dataapertura = ${dataOdierna} for output html"


        println select
        logg =new Log(parametri: "query chiamata IAssicur per Sinistri ${select} ", operazione: "query IASSICUR sinistri da salvare a DB", pagina: "JOB CARICA SINISIRI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [sinistri: [], error: null, iAssicurError: null]
        //try {
            def res = query(select)
            //println res
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuno sinistro trovato"
            logg =new Log(parametri: "sono stati trovati ${res.xml.Query.Record.size()} sinistri", operazione: "query IASSICUR sinistri da salvare a DB", pagina: "JOB CARICA SINISIRI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            //println "${res.xml.Query.Record.size()}"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def sinistro = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: sinistro.noSinistro = campo.text()
                                break
                            case 1: sinistro.nome = campo.text()
                                break
                            case 2: sinistro.targa = campo.text()
                                break
                            case 4: sinistro.nopolizza = campo.text()
                                break
                            case 5: sinistro.codicefiscale = campo.text()
                                break
                            case 6: sinistro.riparatore = campo.text()
                                break
                            case 7: sinistro.partitaivariparatore = campo.text()
                                break
                            case 8: sinistro.dataapertura = campo.text()
                                break
                            case 9: sinistro.datasinistro = campo.text()
                                break
                            case 10: sinistro.datadenuncia = campo.text()
                                break
                            case 11: sinistro.evento = campo.text()
                                break
                            case 12: sinistro.descrizione = campo.text()
                                break
                            case 13: sinistro.descrizione2 = campo.text()
                                break
                            case 14: sinistro.iter = campo.text()
                                break
                            case 15: sinistro.dataiter = campo.text()
                                break
                            case 16:  def stato = campo.text()
                                def index1 = stato.lastIndexOf("(")
                                //def index2 = stato.lastIndexOf(")")
                                stato = stato[0..<index1]
                                sinistro.stato = stato.toString().trim()
                                break
                            case 17: def richiesta= campo.text()
                                richiesta=richiesta.toString().trim().replaceAll("/.","")
                                sinistro.richiesta = richiesta
                                break
                            case 18: sinistro.datarichiesta = campo.text()
                                break
                            case 19: def offerta= campo.text()
                                offerta=offerta.toString().trim().replaceAll("/.","")
                                sinistro.offerta = offerta
                                break
                            case 20: sinistro.dataofferta = campo.text()
                                break
                            case 21: def liquidazione = campo.text()
                                liquidazione=liquidazione.toString().trim().replaceAll("/.","")
                                sinistro.liquidazione  = liquidazione
                                break
                            case 22: sinistro.dataliquidazione = campo.text()
                                break
                            case 23: def scoperto= campo.text()
                                scoperto=scoperto.toString().trim().replaceAll("/.","")
                                sinistro.scoperto  = scoperto
                                break
                            case 24: def franchigia= campo.text()
                                franchigia=franchigia.toString().trim().replaceAll("/.","")
                                sinistro.franchigia  = franchigia
                                break
                            case 25: def note= campo.text()
                                sinistro.note  = note.toString().replaceAll("<br>","").replaceAll("<BR>","").trim()
                            case 26: sinistro.ipertesto = campo.text()
                                break
                            case 27: def compagnia = campo.text()
                                //println "compagnia-->$compagnia"

                                def index1 = compagnia.lastIndexOf("(")
                                def index2 = compagnia.lastIndexOf(")")
                                compagnia = compagnia[index1+1..<index2]
                                sinistro.compagnia = compagnia
                                /*logg =new Log(parametri: "${sinistro.noSinistro} --> compagnia-->$compagnia", operazione: "query IASSICUR sinistri da salvare a DB", pagina: "JOB CARICA SINISIRI")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"*/
                                break
                            case 28: def ramo = campo.text()
                                def index1 = ramo.lastIndexOf("(")
                                def index2 = ramo.lastIndexOf(")")
                                ramo = ramo[index1+1..<index2]
                                sinistro.ramo = ramo
                                break
                        }
                    }
                    result.sinistri << sinistro
                }
            }
        /*} catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }*/
        return result
    }
    def downloadFile(String codiceRecord, String nomeFile, String archivio = "sre") throws RESTClientException {
        def path = grailsApplication.config.iAssicur.azioni.download.path
        def url = grailsApplication.config.iAssicur.urlProduzione
        println "iAssicur Webservice: ${url}${path} -> ${archivio}, ${codiceRecord}"

        try {
            def response = iAssicurClient.post(path: path) {
                urlenc Archivio: archivio, CodiceRecord: codiceRecord, NomeFile: nomeFile, Cartella: ''
            }

            //println "Response: ${response.contentAsString}"
            return response
        } catch(RESTClientException e) {
            println e.message
            println e.response?.contentAsString
            println e.request?.contentAsString
            throw e
        }
    }
    def getFile(numeroSinistro) {
        def select = "SELECT files FROM sre WHERE codicerecord = ${numeroSinistro} for output html"
        def error = null, file = []
        try {
            def res = query(select)
            res.xml.Query.Record.each { record ->
                record.Campo.eachWithIndex { campo, indice ->
                    switch (indice) {
                        case 1: file = []
                            def xml = XmlUtil.serialize(campo)
                            campo.File.each {
                                def f = [:]
                                def data = it.DataOraUltimaModifica.text()
                                f.data = data
                                f.nome = it.Nome.text()
                                def start = f.nome.lastIndexOf(".")
                                def end = f.nome.length()
                                f.estensione = (f.nome[start + 1..<end]).toLowerCase()
                                file << f
                            }
                            break
                    }
                }
            }
        } catch (RESTClientException e) {
            if (e.response == null) error = "Errore di connessione al portale iAssicur"
            else if (e.response.statusCode == 401) error = "Errore di autenticazione sul portale iAssicur"
            else error = e.response.statusMessage
        }
        return [error: error, file: file]
    }
}
