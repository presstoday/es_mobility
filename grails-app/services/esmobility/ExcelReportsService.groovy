package esmobility

import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import com.jcraft.jsch.SftpATTRS
import com.jcraft.jsch.SftpException
import excelbld.ExcelBuilder
import grails.util.Environment
import sinistri.Log
import au.com.bytecode.opencsv.CSVWriter

class ExcelReportsService {
    def IAssicurWebService
    def grailsApplication
    def estrazioneSett(def sinistriWS, def settimana) {
        def logg
        def wb = ExcelBuilder.createXls {
            style("header") {
                border style: "thin", color: "black"
                align vertical: "center", horizontal: "center"

            }
            sheet("Sinistri settimana ${settimana}") {
                row(style: "header") {
                    cell("NOME")
                    cell("TARGA")
                    cell("NO. SINISTRO")
                    cell("NO. POLIZZA")
                    cell("CODICE FISCALE")
                    cell("RIPARATORE")
                    cell("PARTITA IVA")
                    cell("DATA APERTURA")
                    cell("DATA SINISTRO")
                    cell("DATA DENUNCIA")
                    cell("EVENTO")
                    cell("DESCRIZIONE")
                    cell("DESCRIZIONE 2")
                    cell("ITER")
                    cell("DATA ITER")
                    cell("STATO")
                    cell("RICHIESTA")
                    cell("DATA RICHIESTA")
                    cell("OFFERTA")
                    cell("DATA OFFERTA")
                    cell("LIQUIDAZIONE")
                    cell("DATA LIQUIDAZIONE")
                    cell("SCOPERTO%")
                    cell("FRANCHIGIA")
                    cell("NOTA")
                    /*cell("DOCUMENTAZIONE COMPLETA")
                    cell("DOCUMENTAZIONE 1")
                    cell("DOCUMENTAZIONE 2")
                    cell("DOCUMENTAZIONE 3")
                    cell("DOCUMENTAZIONE 4")
                    cell("DOCUMENTAZIONE 5")
                    cell("DOCUMENTAZIONE 6")
                    cell("DOCUMENTAZIONE 7")
                    cell("DOCUMENTAZIONE 8")
                    cell("DOCUMENTAZIONE 9")*/
                    cell("IPERTESTO")
                    cell("COMPAGNIA")
                    cell("RAMO")
                }
                sinistriWS.each { sinistro ->
                    row(style: "content") {
                        cell(sinistro.nome)
                        cell(sinistro.targa)
                        cell(sinistro.noSinistro)
                        cell(sinistro.numero)
                        cell(sinistro.codFiscale)
                        cell(sinistro.rubricaripara)
                        cell(sinistro.partitaivariparatore)
                        cell(sinistro.dataapertura)
                        cell(sinistro.data)
                        cell(sinistro.denuncia)
                        cell(sinistro.evento)
                        cell(sinistro.descrizione)
                        cell(sinistro.descrizione2)
                        cell(sinistro.iter)
                        cell(sinistro.dataIter)
                        cell(sinistro.stato)
                        cell(sinistro.richiesta)
                        cell(sinistro.dataRichiesta)
                        cell(sinistro.offerta)
                        cell(sinistro.dataOfferta)
                        cell(sinistro.liquidazione)
                        cell(sinistro.dataLiquidazione)
                        cell(sinistro.scoperto)
                        cell(sinistro.franchigia)
                        cell(sinistro.degrado)
                        /*cell(sinistro.docucompleta)
                        cell(sinistro.docu1)
                        cell(sinistro.docu2)
                        cell(sinistro.docu3)
                        cell(sinistro.docu4)
                        cell(sinistro.docu5)
                        cell(sinistro.docu6)
                        cell(sinistro.docu7)
                        cell(sinistro.docu8)
                        cell(sinistro.docu9)*/
                        cell(sinistro.note)
                        cell(sinistro.compagnia)
                        cell(sinistro.ramo)
                    }
                    //sinistro.discard()
                }
            }
        }
        def stream = new ByteArrayOutputStream()
        wb.write(stream)
        stream.close()
        logg = new Log(parametri: "ho finito di creare il file excel", operazione: "generaREPORTSETT", pagina: "JOB ESTRAZIONE SETTIMANALE")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return stream.toByteArray()
    }
    def estrazioneTot(def sinistriWS) {
        def logg
        def wb = ExcelBuilder.createXls {
            style("header") {
                border style: "thin", color: "black"
                align vertical: "center", horizontal: "center"

            }
            sheet("Sinistri") {
                row(style: "header") {
                    cell("NOME")
                    cell("TARGA")
                    cell("NO. SINISTRO")
                    cell("NO. POLIZZA")
                    cell("CODICE FISCALE")
                    cell("RIPARATORE")
                    cell("PARTITA IVA")
                    cell("DATA APERTURA")
                    cell("DATA SINISTRO")
                    cell("DATA DENUNCIA")
                    cell("EVENTO")
                    cell("DESCRIZIONE")
                    cell("DESCRIZIONE 2")
                    cell("ITER")
                    cell("DATA ITER")
                    cell("STATO")
                    cell("RICHIESTA")
                    cell("DATA RICHIESTA")
                    cell("OFFERTA")
                    cell("DATA OFFERTA")
                    cell("LIQUIDAZIONE")
                    cell("DATA LIQUIDAZIONE")
                    cell("SCOPERTO%")
                    cell("FRANCHIGIA")
                    cell("NOTA")
                    /*cell("DOCUMENTAZIONE COMPLETA")
                    cell("DOCUMENTAZIONE 1")
                    cell("DOCUMENTAZIONE 2")
                    cell("DOCUMENTAZIONE 3")
                    cell("DOCUMENTAZIONE 4")
                    cell("DOCUMENTAZIONE 5")
                    cell("DOCUMENTAZIONE 6")
                    cell("DOCUMENTAZIONE 7")
                    cell("DOCUMENTAZIONE 8")
                    cell("DOCUMENTAZIONE 9")*/
                    cell("IPERTESTO")
                    cell("COMPAGNIA")
                    cell("RAMO")
                }
                sinistriWS.each { sinistro ->
                    row(style: "content") {
                        cell(sinistro.nome)
                        cell(sinistro.targa)
                        cell(sinistro.noSinistro)
                        cell(sinistro.numero)
                        cell(sinistro.codFiscale)
                        cell(sinistro.rubricaripara)
                        cell(sinistro.partitaivariparatore)
                        cell(sinistro.dataapertura)
                        cell(sinistro.data)
                        cell(sinistro.denuncia)
                        cell(sinistro.evento)
                        cell(sinistro.descrizione)
                        cell(sinistro.descrizione2)
                        cell(sinistro.iter)
                        cell(sinistro.dataIter)
                        cell(sinistro.stato)
                        cell(sinistro.richiesta)
                        cell(sinistro.dataRichiesta)
                        cell(sinistro.offerta)
                        cell(sinistro.dataOfferta)
                        cell(sinistro.liquidazione)
                        cell(sinistro.dataLiquidazione)
                        cell(sinistro.scoperto)
                        cell(sinistro.franchigia)
                        cell(sinistro.degrado)
                        /*cell(sinistro.docucompleta)
                        cell(sinistro.docu1)
                        cell(sinistro.docu2)
                        cell(sinistro.docu3)
                        cell(sinistro.docu4)
                        cell(sinistro.docu5)
                        cell(sinistro.docu6)
                        cell(sinistro.docu7)
                        cell(sinistro.docu8)
                        cell(sinistro.docu9)*/
                        cell(sinistro.note)
                        cell(sinistro.compagnia)
                        cell(sinistro.ramo)
                    }
                    //sinistro.discard()
                }
            }
        }
        def stream = new ByteArrayOutputStream()
        wb.write(stream)
        stream.close()
        logg = new Log(parametri: "ho finito di creare il file excel", operazione: "generaREPORTTOT", pagina: "JOB ESTRAZIONE TOTALE")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return stream.toByteArray()
    }
    def estrazioneNott(def sinistriWS) {
        def logg
        def streamCSV = new ByteArrayOutputStream()
        def writer = new StringWriter()
        char puntoe=';'
        def w = new CSVWriter(writer, puntoe)
        //w.writeNext((String[]) ['NOME', 'TARGA', 'NO.SINISTRO', 'CODICE_FISCALE', 'RIPARATORE', 'DATA_SINISTRO','DATA_DENUNCIA','EVENTO','DESCRIZIONE','DESCRIZIONE2','STATO'])
        w.writeNext((String[]) ['not used', 'asset_ref', 'case_reference', 'repair_location', 'damage_date', 'declaration_date','damage_type','damage_detail','coverage','code_fiscal','case_status_details','case_status_update_date','customer_id'])
        String[] line
        sinistriWS.each { sinistro ->
            //println "${sinistro} \n"
            line = ["${sinistro.nome}","${sinistro.targa}","${sinistro.noSinistro}","${sinistro.rubricaripara}","${sinistro.data}","${sinistro.denuncia}","${sinistro.evento}","${sinistro.descrizione} ${sinistro.descrizione2}","${sinistro.stato}","${sinistro.codFiscale}","${sinistro.iter}","${sinistro.dataiter}","${sinistro.numero}"]
            w.writeNext(line)
        }
        writer.close()
        streamCSV.write(writer.toString().getBytes())
        streamCSV.close()
        logg = new Log(parametri: "ho finito di creare il file excel", operazione: "generaREPORTTOT", pagina: "JOB ESTRAZIONE TOTALE")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return streamCSV.toByteArray()
    }
    def estrazioneRCAAperti(def sinistriWS) {
        def logg
        def wb = ExcelBuilder.createXls {
            style("header") {
                border style: "thin", color: "black"
                align vertical: "center", horizontal: "center"

            }
            sheet("Sinistri aperti") {
                row(style: "header") {
                    cell("NOME")
                    cell("TARGA")
                    cell("NO. SINISTRO")
                    cell("NO. POLIZZA")
                    cell("CODICE FISCALE")
                    cell("RIPARATORE")
                    cell("PARTITA IVA")
                    cell("DATA APERTURA")
                    cell("DATA SINISTRO")
                    cell("DATA DENUNCIA")
                    cell("EVENTO")
                    cell("DESCRIZIONE")
                    cell("DESCRIZIONE 2")
                    cell("ITER")
                    cell("DATA ITER")
                    cell("STATO")
                    cell("RICHIESTA")
                    cell("DATA RICHIESTA")
                    cell("OFFERTA")
                    cell("DATA OFFERTA")
                    cell("LIQUIDAZIONE")
                    cell("DATA LIQUIDAZIONE")
                    cell("SCOPERTO%")
                    cell("FRANCHIGIA")
                    cell("NOTA")
                    /*cell("DOCUMENTAZIONE COMPLETA")
                    cell("DOCUMENTAZIONE 1")
                    cell("DOCUMENTAZIONE 2")
                    cell("DOCUMENTAZIONE 3")
                    cell("DOCUMENTAZIONE 4")
                    cell("DOCUMENTAZIONE 5")
                    cell("DOCUMENTAZIONE 6")
                    cell("DOCUMENTAZIONE 7")
                    cell("DOCUMENTAZIONE 8")
                    cell("DOCUMENTAZIONE 9")*/
                    cell("IPERTESTO")
                    cell("COMPAGNIA")
                    cell("RAMO")
                }
                sinistriWS.each { sinistro ->
                    row(style: "content") {
                        cell(sinistro.nome)
                        cell(sinistro.targa)
                        cell(sinistro.noSinistro)
                        cell(sinistro.numero)
                        cell(sinistro.codFiscale)
                        cell(sinistro.rubricaripara)
                        cell(sinistro.partitaivariparatore)
                        cell(sinistro.dataapertura)
                        cell(sinistro.data)
                        cell(sinistro.denuncia)
                        cell(sinistro.evento)
                        cell(sinistro.descrizione)
                        cell(sinistro.descrizione2)
                        cell(sinistro.iter)
                        cell(sinistro.dataIter)
                        cell(sinistro.stato)
                        cell(sinistro.richiesta)
                        cell(sinistro.dataRichiesta)
                        cell(sinistro.offerta)
                        cell(sinistro.dataOfferta)
                        cell(sinistro.liquidazione)
                        cell(sinistro.dataLiquidazione)
                        cell(sinistro.scoperto)
                        cell(sinistro.franchigia)
                        cell(sinistro.degrado)
                        /*cell(sinistro.docucompleta)
                        cell(sinistro.docu1)
                        cell(sinistro.docu2)
                        cell(sinistro.docu3)
                        cell(sinistro.docu4)
                        cell(sinistro.docu5)
                        cell(sinistro.docu6)
                        cell(sinistro.docu7)
                        cell(sinistro.docu8)
                        cell(sinistro.docu9)*/
                        cell(sinistro.note)
                        cell(sinistro.compagnia)
                        cell(sinistro.ramo)
                    }
                    //sinistro.discard()
                }
            }
        }
        def stream = new ByteArrayOutputStream()
        wb.write(stream)
        stream.close()
        logg = new Log(parametri: "ho finito di creare il file excel", operazione: "generaREPORTAPERTI", pagina: "JOB ESTRAZIONE SINISTRI APERTI")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return stream.toByteArray()
    }
    def ftpsuploadFile(file, String fileName) {
        def logg
        def input
        try {
            if(file instanceof byte[]) input = new ByteArrayInputStream(file)
            else if(file instanceof InputStream) input = file
            else if(file instanceof String) input = new ByteArrayInputStream(file.bytes)
            def sftpConfig = grailsApplication.config.sftp
            def client = new JSch()
            def session = client.getSession(sftpConfig.username, sftpConfig.host, sftpConfig.port)
            session.setConfig("StrictHostKeyChecking", "no")
            session.password = sftpConfig.password
            session.connect()
            logg =new Log(parametri: "JSch Session connected to ${session.userName}@${session.host}:${session.port}", operazione: "collegamento server ftp", pagina: "JOB CARICA SINISTRI" )
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new Log(parametri: "JSch Channel connected", operazione: "collegamento server ftp", pagina: "JOB CARICA ESTRAZIONE")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            try {
                //channel.cd(sftpConfig.cartella)
                //logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "modifica cartella", pagina: "JOB CARICA ESTRAZIONE")
                //if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(Environment.current != Environment.PRODUCTION) {
                    channel.cd("/home/flussi-rci/Sinistri")
                }else{
                    channel.cd("/home/flussi-rci/Sinistri")
                }
                logg =new Log(parametri: "try uploading ${fileName}...", operazione: "caricamento notturni", pagina: "JOB CARICA SINISTRI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                channel.put(input, fileName)
                logg =new Log(parametri: "${fileName} uploaded!", operazione: "caricamento file", pagina: "JOB CARICA SINISTRI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            catch (e) {
                logg =new Log(parametri: "errore try catch ->${e.toString()}", operazione: "caricamento file", pagina: "JOB CARICA SINISTRI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            channel.disconnect()
            session.disconnect()
            return true
        } catch(e) {
            logg =new Log(parametri: "Error uploading file ${fileName} to sftp: ${e.toString()}", operazione: "FTP UPLOAD", pagina: "JOB CARICA ESTRAZIONE")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            return false
        }
    }
}
