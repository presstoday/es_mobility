package sinistri


enum OrigineDatocontabile { CARICATODAIASSICUR, SALVATODAPORTALE}
class Daticontabili {
    String tipoPolizza, intestatarioPolizza, intestatarioFattura, noFattura, intestatarioConto, iban,ramo
    Date dateCreated, dataPagamento, dataPagamentoIntegrato
    String noSinistro
    BigDecimal importoFattura,importoLiquidato,differenzaLiquidare

    OrigineDatocontabile origine = OrigineDatocontabile.CARICATODAIASSICUR
    static hasMany = [ allegati:Allegati]

    static constraints = {
        tipoPolizza nullable: true
        intestatarioPolizza nullable: true
        intestatarioFattura nullable: true
        noFattura nullable: true
        intestatarioConto nullable: true
        iban nullable: true
        ramo nullable: true
        dataPagamento nullable: true
        dataPagamentoIntegrato nullable: true
        differenzaLiquidare nullable: true, min: 0.0
        importoFattura nullable: true, min: 0.0
        importoLiquidato nullable: true, min: 0.0
        /*tipoPolizza nullable: true, validator: { tipoPolizza, daticontabili ->
            if(daticontabili.origine == OrigineDatocontabile.SALVATODAPORTALE && !tipoPolizza) return 'daticontabili.tipoPolizza.nullable'
        }*/
        /*intestatarioConto nullable: true, validator: { intestatarioConto, daticontabili ->
            if(daticontabili.origine == OrigineDatocontabile.SALVATODAPORTALE && !intestatarioConto) return 'daticontabili.intestatarioConto.nullable'
        }*/

    }

}
