package sinistri

class Sinistridb {
    String nome,targa,nosinistro,nopolizza,codicefiscale,riparatore,partitaivariparatore,evento,descrizione,descrizione2,iter,stato,nota,ipertesto, compagnia,ramo
    BigDecimal richiesta,offerta,liquidazione,scoperto,franchigia
    Date dateCreated, dataapertura,datasinistro,datadenuncia,dataiter,datarichiesta,dataofferta,dataliquidazione

    static constraints = {
        riparatore nullable: true
        partitaivariparatore nullable: true
        dataiter nullable: true
        descrizione2 nullable: true
        richiesta nullable: true
        datarichiesta nullable: true
        offerta nullable: true
        dataofferta nullable: true
        liquidazione nullable: true
        dataliquidazione nullable: true
        scoperto nullable: true
        franchigia nullable: true
        ipertesto nullable: true
    }
    static mapping = {
        ipertesto type: "text"
    }
}
