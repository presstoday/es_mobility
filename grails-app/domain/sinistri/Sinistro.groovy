package sinistri

import utenti.Utente

class Sinistro {
    String messaggio
    Date dataInserimento
    String noSinistro


    static hasMany = [utente: Utente]

    static constraints = {
        messaggio type: "text"
    }
}
