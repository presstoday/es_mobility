package sinistri

class Allegati {
    Date dateCreated, lastUpdated
    String fileName
    byte[] fileContent
    //static belongsTo = [sinistro: Daticontabili]
    static constraints = {
        fileContent nullable: false, size: 1..(1024 * 1024 * 5)
    }

}
