package sinistri

enum TipoEst {
    ELENCO_SETTIMANALE, ELENCO_TOTALE, ELENCO_APERTI, ELENCO_NOTTURNI
}
class Estrazioni {
    Date dateCreated, lastUpdated, settimanaEstra
    String fileName
    int totRecords
    TipoEst tipo=TipoEst.ELENCO_SETTIMANALE
    byte[] fileContent
    static constraints = {
        fileName: unique: true
        fileContent nullable: false, size: 0..(1024 * 1024 * 50)
    }
}
